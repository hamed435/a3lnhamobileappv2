import * as types from "../actions/types"
import * as colors from '../assets/colors' 
import AsyncStorage  from '@react-native-community/async-storage'

const initialState = {
    favList:[],
}

const FavouriteReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CURRENT_FAV_LIST:
            console.log('fav lsit =>'+action.payload)
            return { ...state, favList: action.payload };  
        case types.ADD_FAV_TO_LIST:
            console.log('add fav ti lsit =>'+action.payload)
            if(!state.favList.includes(action.payload)){
                console.log("ADD FAV IF ")
                const data = [...state.favList, action.payload]
                AsyncStorage.setItem('@fav',JSON.stringify(data))
                return { ...state, favList:data };    
            }else{
                console.log("ADD FAV ELSE ")
            }

        case types.REMOVE_FAV_FROM_LIST:
            console.log('remove fav from lsit =>'+action.payload)
            if(state.favList.includes(action.payload)){
                console.log("REMOVE FAV IF ")
                const data = state.favList.filter(item => {
                    return item !== action.payload
                })
                
                AsyncStorage.setItem('@fav',JSON.stringify(data))
                return { ...state, favList:data };    
            }else{
                console.log("REMOVE FAV ELSE ")
            }    
                      
            
        default:
            return state;
    }

}

export default FavouriteReducer;
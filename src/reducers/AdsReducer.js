import {
    FETCH_ADS_REFRESH,FETCH_ADS_REQUEST,FETCH_ADS_SUCCESS,FETCH_ADS_FAIL,DELETE_ADS
} from '../actions/types';

import {
    DataProvider,
} from 'recyclerlistview';


const initState = {
    loading:true,
    ads:[],
    page:1,
    refresh: false,
    pages:1,
    showDeleteDialog:false,
    id:null,
}

const AdsReducer = (state=initState, action) => {
    switch(action.type){
        case FETCH_ADS_REQUEST:
            return { ...state, refresh: false,loading:action.payload };
        case FETCH_ADS_SUCCESS:
            console.log()
            return {
                ...state,
                ads:state.refresh ? action.payload : [...state.ads, ...action.payload],
                loading: false,
                refresh: false,
                page:action.page,
                pages:action.pages,
            };
        case FETCH_ADS_FAIL:
            return { ...state, loading: false, refresh: false,};
        case FETCH_ADS_REFRESH:
            return { ...state, refresh: true, loading: false };
        case DELETE_ADS:
            console.log("nossss "+ action.payload)
            return { ...state,id:action.id,showDeleteDialog:action.show }; 
        default: 
            return state; 
    }
}

export default AdsReducer;
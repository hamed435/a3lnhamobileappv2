import * as types from "../actions/types"

const initialState = {
  iosRootNavigator:null,
};

const NavigatorReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.IOS_ROOT_NAVIGATOR:
      console.log("navigator  ",action.payload)
      return { ...state, iosRootNavigator: action.payload };
    default:
      return state;
  }
};

export default NavigatorReducer;

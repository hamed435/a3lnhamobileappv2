import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form';
import MenuReducer from "./MenuReducer";
import NavigatorReducer from './NavigatorReducer';
import AuthReducer from "./AuthReducer";
import LanguageReducer from './LanguageReducer';
import SignupReducer from './SignupReducer';
import ProductReducer from './ProductReducer';
import NotificationsReducer from './NotificationsReducer';
import OrderReducer from './OrderReducer';
import CategoryReducer from './CategoryReducer';
import ChatReducer from './ChatReducer'
import AdsReducer from './AdsReducer'
import FavouriteReducer from './FavouriteReducer'

export default combineReducers({
    form: formReducer,
    menu: MenuReducer,
    navigation: NavigatorReducer,
    auth: AuthReducer,
    lang:LanguageReducer,
    signup: SignupReducer,
    product: ProductReducer,
    noti: NotificationsReducer,
    order: OrderReducer,
    category: CategoryReducer,
    chat: ChatReducer,
    ads: AdsReducer,
    fav: FavouriteReducer,
});
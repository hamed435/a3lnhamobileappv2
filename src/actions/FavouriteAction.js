import {Alert} from 'react-native';
import {
    CURRENT_FAV_LIST,ADD_FAV_TO_LIST,REMOVE_FAV_FROM_LIST
} from './types';

export  function currentFavList(favList) {
    return  (dispatch) => {
        dispatch({ type: CURRENT_FAV_LIST,payload:favList});  
    }
}

export  function addFavLToist(adsId) {
   
    return  (dispatch) => {
        console.log("ADD   ",adsId)
        dispatch({ type: ADD_FAV_TO_LIST,payload:adsId});  
    }
}

export  function removeFavLToist(adsId) {
  
    return  (dispatch) => {
        console.log("REMOVE   ",adsId)
        dispatch({ type: REMOVE_FAV_FROM_LIST,payload:adsId});  
    }
}
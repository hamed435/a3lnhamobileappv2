
import { IOS_ROOT_NAVIGATOR } from "./types";

export  function IosRootNavigator(navigator) {
    return  (dispatch) => {
        dispatch({ type: IOS_ROOT_NAVIGATOR,payload:navigator});  
    }
}
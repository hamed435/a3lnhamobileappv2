import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {CATEGORY_LOAD,CATEGORY_SUCCESS,CATEGORY_FAILD} from './types';

export function getCategories(){
   return dispatch => {
        dispatch({type:CATEGORY_LOAD})
        axios.get(`${BASE_END_POINT}/categories`)
        .then(response=>{
            console.log('get catergory Done')
            dispatch({type:CATEGORY_SUCCESS,payload:response.data.data})
        })
        .catch(error=>{
            dispatch({type:CATEGORY_FAILD,payload:Strigs.noConnection})
            if(!error.response){
                dispatch({type:CATEGORY_FAILD,payload:Strigs.noConnection})
            }
      
        })
   }
}
import { LOGOUT,LOGOUT_LOADING,LOGOUT_LOADING_END } from "./types";
import { BASE_END_POINT } from '../AppConfig';
import AsyncStorage  from '@react-native-community/async-storage'
//import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';


export default function logout(FB_token, BE_token, navigator) {
    console.log('dddddddddddddqqqq')
    return  dispatch => {
        
        axios.post(`${BASE_END_POINT}logout`, JSON.stringify({
            token: FB_token 
          }), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${BE_token}`
          },
        }).then(response => {
            dispatch({ type: LOGOUT})
            console.log('log out')
            console.log(response);     
             AsyncStorage.removeItem("@QsathaUser")
             .then(res=>{
                 console.log('remove user done')
                 //AsyncStorage.removeItem('@COUNTRY')
                 navigator.resetTo({
                    screen: 'SplashScreen',
                    animated: true,
                    animationType:'slide-horizontal'
                })
             }).catch(err=>{
                console.log('remove user error  ',err)
             })
            
             dispatch({type:LOGOUT_LOADING})
          
        }).catch(error => {
            console.log('log out error')
            console.log(error)
            console.log(error.response)
            dispatch({type:LOGOUT_LOADING_END})
        });
        
    }
}
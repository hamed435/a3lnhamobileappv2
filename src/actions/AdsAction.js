import {Alert} from 'react-native';
import {
    FETCH_ADS_REFRESH,FETCH_ADS_REQUEST,FETCH_ADS_SUCCESS,FETCH_ADS_FAIL,DELETE_ADS
} from './types';
import {BASE_END_POINT} from '../AppConfig'
import axios from 'axios';


export function getAds(page,refresh,id) {
    console.log('page  ',page)
    return dispatch => {
        let uri=`${BASE_END_POINT}ads?owner=${id}&page=${page}&limit=20`
        if(refresh){
            dispatch({type:FETCH_ADS_REFRESH});
        }else{
            dispatch({type:FETCH_ADS_REQUEST,payload:page==1?true:false});
        }
        axios.get(uri)
        .then(response=>{
            console.log('ads  ads ')
            console.log(response.data);
            dispatch({type:FETCH_ADS_SUCCESS,payload:response.data.data,page:response.data.page,pages:response.data.pageCount})
        }).catch(error=>{
            console.log(error.response);
            dispatch({type:FETCH_ADS_FAIL})
            if (!error.response) {
                dispatch({type:FETCH_ADS_FAIL})
              }
        })
    }
}

export function showDeleteAdsDialog(id,show) {
    console.log('44444')
    return dispatch => {
        dispatch({type:DELETE_ADS,id:id,show:show})
    }
}
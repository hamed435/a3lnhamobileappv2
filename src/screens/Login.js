import React, { Component } from 'react';
import {
  View,TouchableOpacity,Keyboard,Image,Text,ScrollView,TextInput,Alert,Platform
} from 'react-native';
import { connect } from 'react-redux';
import {Button,Icon} from 'native-base';
import {login,socialLogin} from '../actions/AuthActions';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { Field, reduxForm } from "redux-form"
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {removeItem} from '../actions/MenuActions';

import FlatAppHeader from '../common/FlatAppHeader'
const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { LoginManager,GraphRequest, GraphRequestManager,LoginButton, AccessToken,ShareDialog   } from 'react-native-fbsdk';
import axios from 'axios';

const shareLinkContent = {
    contentType: 'link',
    contentUrl: "https://facebook.com",
    contentDescription: 'Wow, check out this great site!',
  };

class Login extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: 'white',
    };

    state={
        email: ' ',
        password: ' ',
        hidePassword: true,
    }
    componentDidUpdate(){
        console.log('Current country',this.props.currentCountry)
        /*this.props.navigator.setStyle({
            statusBarColor: '#4c96db',
            
          });*/ 
      }
    constructor(props) {
        super(props);
        /*rootNavigator = this.props.navigator;
        this.props.navigator.setStyle({
            statusBarColor: '#4c96db',
            
          }); */
    }
    
    componentDidMount(){  
       // Alert.alert("hi")
        //44:57:C8:C6:5E:DD:65:C1:CA:7B:F9:F8:A2:D4:F5:E1:07:2D:9D:73
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
            webClientId: '978344310083-2lq9jqv0bc6sv0sd22oebdhuvnbcsk52.apps.googleusercontent.com' // client ID of type WEB for your server (needed to verify user ID and offline access)
            //offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            //hostedDomain: '', // specifies a hosted domain restriction
            //loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
            //forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
            //accountName: '', // [Android] specifies an account name on the device that should be used
            //iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
          });
        this.disableDrawer(); 
        console.log('anwer  '+this.props.isRTL) 
        console.log('Login page user token  '+this.props.userToken)     
    }

    /*componentWillUnmount(){
        this.props.removeItem()
      }*/

      signIn =  () => {
        console.log("HI ")
        try {
           GoogleSignin.hasPlayServices().
           then(response=>{
            console.log("has play services",response)
           });
            GoogleSignin.signIn()
            .then(reponse=>{
                const user={
                    signUpFrom:'gmail',
                    socialId:reponse.user.id,
                    username:reponse.user.name,
                    img:reponse.user.photo,
                    email:reponse.user.email,
                    country: this.props.currentCountry.countryName,
                    countryID:this.props.currentCountry.id,
                    token:this.props.userToken,
                }
                  //Alert.alert('name  ',reponse.user.name)
               // Alert.alert('name  ',reponse.user.email)
               // Alert.alert('name  ',reponse.user.photo)
                this.props.socialLogin(user,this.props.userToken,this.props.navigator)
                console.log("GMAIL User Info",reponse)
          })
          .catch(error=>{
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                console.log("SIGN_IN_CANCELLED   ",error)
              } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (f.e. sign in) is in progress already
                console.log("IN_PROGRESS   ",error)
              } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                console.log("PLAY_SERVICES_NOT_AVAILABLE   ",error)
              } else {
                console.log("else   ",error)
                //Alert.alert('error  ',error.code)
              }
          })
         
        } catch (error) {
            //Alert.alert('error  ',error)
        }
      };

      facebookLogin = () => {
        if (Platform.OS === "android") {
            LoginManager.setLoginBehavior("web_only")
        }   
        console.log(LoginManager);
        LoginManager.logInWithPermissions(["public_profile","email"]).then(
            (result) => {                              
              if (result.isCancelled) {                             
               // Alert.alert("Login cancelled");
              } else {
                AccessToken.getCurrentAccessToken()
                .then((data) => {
                      //Alert.alert(data.accessToken.toString())
                      const infoRequest = new GraphRequest(
                        '/me',
                        {
                            accessToken: data.accessToken,
                            parameters: {
                                fields: {
                                    string: 'email,first_name,last_name,picture.type(large)'
                                }
                            }
                        },
                        (e,r)=>{
                            if (!e) {
                                console.log('FACE BOOK DATA FROM SIGNUP ',r)
                                let user = {
                                    username: r.first_name+"  "+r.last_name,
                                    email: r.email,
                                    img: r.picture.data.url,
                                    signUpFrom: 'FACEBOOK',
                                    //type: 'CLIENT',
                                    country: this.props.currentCountry.countryName,
                                    countryID:this.props.currentCountry.id,
                                    socialId:r.id,
                                    token:this.props.userToken,
                                }
                                this.props.socialLogin(user,this.props.userToken,this.props.navigator)
                              } else {
                                console.log('e  ',e)
                               // Alert.alert(""+e)
                              }
                        },
                      );
                      // Start the graph request.
                      new GraphRequestManager().addRequest(infoRequest).start();
                      
                    }
                ).catch(er=>{
                    //Alert.alert(er.toString())
                })
              }
            },
            (error) => {
               
               // Alert.alert("Login fail with error: " + error);
            }
          )
          
        /*LoginManager.logInWithReadPermissions(['public_profile', 'email', 'user_friends'])
        .then(
            (result) => {
                console.log("result::::::", result)
                if (result.isCancelled) {
                    console.log("result error::::::", result)
                } else {
                    AccessToken.getCurrentAccessToken().then((data) => {
                        const requestCallback = (error, result) => {
                            if (error) {
                                console.log("ERROR::::::", error)
                               
                            } else {
                                let newData = {
                                    fullName: result.first_name,
                                    email: result.email,
                                    profile: result.picture.data.url,
                                    signupType: 'FACEBOOK',
                                    type: 'CLIENT'
                                }
                                console.log("DATA::::::", newData)
                                
                            }
                        }
                        const request = new GraphRequest(
                            '/me',
                            {
                                accessToken: data.accessToken,
                                parameters: {
                                    fields: {
                                        string: 'email,first_name,last_name,picture'
                                    }
                                }
                            },
                            requestCallback,
                        );
                        new GraphRequestManager().addRequest(request).start()
                    });
                }
            },
            (error) => {
                console.log("ERROR::::::", error)
               
            }
        ).catch(error=>{
            console.log("ERROR::::::", error)
        })
        ;
        */
      }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }


    

    render(){
        const  { isRTL,navigator,userToken } = this.props;
        const  { email,password,hidePassword} = this.state
        return(
            <ScrollView style={{backgroundColor:'white', flex:1 }} >
               <Text style={{fontFamily:'Roboto-Regular',marginTop:moderateScale(15),color:'#378BA8',alignSelf:'center',fontSize:responsiveFontSize(8)}} >{Strings.welcomeTo}</Text>
               <Image resizeMode='center' style={{alignSelf:'center', marginTop:moderateScale(0),width:responsiveWidth(65),height:responsiveHeight(20)}} source={require('../assets/imgs/appLogo.png')} />

                <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                    <View style={{justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2,shadowOffset: { height: 1,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput 
                        keyboardType='email-address'
                        onChangeText={(val)=>{this.setState({email:val})}}
                        placeholder={Strings.enterYourEmail} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(0),width:responsiveWidth(86), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                    </View>
                    {email.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

                <View style={{marginVertical:moderateScale(4), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.password}</Text>
                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2,shadowOffset: { height: 1,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                
                        secureTextEntry={hidePassword}
                        onChangeText={(val)=>{this.setState({password:val})}}
                        placeholder={Strings.enterYourPassword} underlineColorAndroid='transparent' style={{width:responsiveWidth(81), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                        <TouchableOpacity 
                        onPress={()=>{this.setState({hidePassword:!hidePassword})}}
                        >
                            <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={hidePassword?'eye-off':'eye'} type='Feather' />
                        </TouchableOpacity>
                    </View>
                    {password.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

                <TouchableOpacity
                 onPress={()=>{
                     navigator.push({
                         screen:'ForgetPassword',
                         animated:true,
                         animationType:'slide-horizontal'
                     })
                 }}
                 style={{alignSelf:isRTL?'flex-start':'flex-end',marginTop:moderateScale(2),marginHorizontal:moderateScale(13)}}>
                    <Text style={{color:'#A9A7A7',fontSize:responsiveFontSize(6)}}>{Strings.forgetPassword}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(3)}}
                    onPress={()=>{
                    
                    if(!email.replace(/\s/g, '').length){
                        this.setState({email:''})
                    }
                    if(!password.replace(/\s/g, '').length){
                        this.setState({password:''})
                    }

                    if(email.replace(/\s/g, '').length&&password.replace(/\s/g, '').length){
                        this.props.login(
                            email.toLowerCase(),
                            password,
                            userToken,
                            navigator,
                        );
                    }
                    
                    }}
                    >
                    <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.login}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>{
                    this.props.navigator.push({
                        screen: 'Signup',
                        animated: true,
                        animationType:'slide-horizontal'
                    });
                }}
                 style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'center', alignSelf:'center',marginTop:moderateScale(10)}} >
                    <Text style={{color:'#a9a7a7',fontSize:responsiveFontSize(6)}}>{Strings.dontHaveAccount}</Text>
                    <Text style={{marginHorizontal:moderateScale(1.5), color:'#a9a7a7',fontSize:responsiveFontSize(6)}}>{Strings.registerNow}</Text>
                </TouchableOpacity>
               
                <Text style={{alignSelf:'center', marginTop:moderateScale(5),color:'#a9a7a7',fontSize:responsiveFontSize(6)}}>{Strings.signupWith}</Text>
                
                <View style={{alignSelf:'center',justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',marginVertical:moderateScale(10)}}>

                    <TouchableOpacity
                    onPress={()=>{
                        this.facebookLogin()
                    }}
                     style={{width:50,height:50,borderRadius:25,justifyContent:'center',alignItems:'center',backgroundColor:'#C7BE75'}}>
                        <Icon name='sc-facebook' type='EvilIcons' style={{color:'white'}} />
                    </TouchableOpacity>
                   
                    <TouchableOpacity style={{marginHorizontal:moderateScale(7), width:50,height:50,borderRadius:25,justifyContent:'center',alignItems:'center',backgroundColor:'#C7BE75'}}>
                        <Icon 
                        onPress={()=>{
                            this.signIn()
                        }}
                         name='gmail' type='MaterialCommunityIcons' style={{color:'white'}} />
                    </TouchableOpacity>
                </View>
                


               
            
                {this.props.loading&&<LoadingDialogOverlay title={Strings.checkLogin} />}
               
            </ScrollView>
        );
    }
}

const mapDispatchToProps = {
    login,
    removeItem,
    socialLogin,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    userToken: state.auth.userToken,
    barColor: state.lang.color ,
    currentCountry:state.auth.currentCountry,
})


export default connect(mapToStateProps,mapDispatchToProps)(Login);


import React,{Component} from 'react';
import {View,RefreshControl,NetInfo,Text,TextInput,TouchableOpacity,Modal} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Picker,Thumbnail} from 'native-base';
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';

import ChatPeopleCard from '../components/ChatPeopleCard';
import strings from '../assets/strings';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image';
import FlatAppHeader from '../common/FlatAppHeader';


class ChatPeople extends Component {

    page=1;
    list=[];
    state= {
        errorText:null,
        friendMessages: new DataProvider(),
        refresh:false,
        loading:true,
        pages:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };
 
    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(12);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getClientFriends(1,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getClientFriends(1,true);
                }
            }
          );      
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }    

    getClientFriends(page, refresh) {
        //this.setState({loading:true})
        let uri = `${BASE_END_POINT}messages/lastContacts?id=${this.props.currentUser.user.id}&page=${page}&limit=20`
        if (refresh) {
            this.setState({loading: false, refresh: true})
        } else {
            this.setState({refresh:false})
        }
        axios.get(uri)
            .then(response => {
                this.setState({
                    loading:false,
                    refresh:false,
                    pages:response.data.pageCount,
                    errorText:null,
                    friendMessages: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.friendMessages.getAllData(), ...response.data.data]),
                })
                console.log('friendMessages   ',response.data)
                
            }).catch(error => {
                this.setState({loading:false,refresh:false})
                console.log("error   ",error);
                console.log("error   ",error.response);
               
            })
    
}



    renderRow = (type, data, row) => {
     return (
        <ChatPeopleCard 
        onPress={()=>{
            this.props.navigator.push({
                screen:'Chat',
                animationType:'slide-horizontal',
                animated:true,
                passProps: {
                    other:data.to.id==this.props.currentUser.user.id?
                    {
                    id:data.from.id,
                    username:data.from.username
                    }
                    :
                    {
                    id:data.to.id,
                    username:data.to.username
                    }
                }
            })
        }}
       data={data}
       navigator={this.props.navigator}
        />
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,categoryName,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                 <FlatAppHeader menu navigator={navigator} title={Strings.friends} />
                {this.state.loading?
                <View style={{ flex:1}}> 
                <LottieView
                style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                source={require('../assets/animations/smartGarbageLoading.json')}
                autoPlay
                loop
                />
                 <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.state.friendMessages._data.length>0?
                    <RecyclerListView            
                    layoutProvider={this.renderLayoutProvider}
                    rowRenderer={this.renderRow}
                    dataProvider={this.state.friendMessages}                                   
                    renderFooter={this.renderFooter}
                    onEndReached={() => {
                       
                        if(this.page <= this.props.pages){
                            this.page++;
                            this.getClientFriends(this.page, false);
                        }
                        
                      }}
                      refreshControl={<RefreshControl colors={["#B7ED03"]}
                        refreshing={this.state.refresh}
                        onRefresh={() => {
                          this.page = 1
                          this.getClientFriends(1, true);
                        }
                        }
                        />
                        }
                      onEndReachedThreshold={.5}
                    />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Text style={{color:'black'}}>{Strings.noDataAtRecent} </Text>
                </View>    
   
                }

            </View>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps)(ChatPeople);

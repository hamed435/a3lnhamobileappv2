import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,StyleSheet,TextInput,Platform, Modal,ScrollView,TouchableOpacity,Text,ImageBackground,Image,FlatList,Share
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import LottieView from 'lottie-react-native';
import Stars from 'react-native-stars';
import Communications from 'react-native-communications';
import {addFavLToist,removeFavLToist} from '../actions/FavouriteAction'
import Swiper from 'react-native-swiper';
import HomeAdsCard from '../components/HomeAdsCard' 
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import strings from '../assets/strings';
import MapView, {Marker} from 'react-native-maps';

class AdsDescription extends Component {

    state = {
        myAds:[],
        adsLoading:true,
        index:0,
        favDone:false,
        isFollow:false,
        rate:3,
        disableFollowButton:false,
        gmailText:'',
        invitationCount:0,
        gmailDialog:false,
        typeDialog:false,
        inAppDialog:false,

        participants:[],
        participantsName:[],
        users:[],
        val:'',
        id:null,

        imgIndex:null,
        showImgDialog:false,
 
    }

    
      componentDidMount(){
        this.disableDrawer();
        this.getAds()
        if(this.props.data.ratePrecent==0){
          this.setState({rate:0.5})
        }else if(this.props.data.ratePrecent>0&&this.props.data.ratePrecent<=20){
          this.setState({rate:1})
        }else if(this.props.data.ratePrecent>20&&this.props.data.ratePrecent<=40){
          this.setState({rate:2})
        }else if(this.props.data.ratePrecent>40&&this.props.data.ratePrecent<=60){
          this.setState({rate:3})
        }else if(this.props.data.ratePrecent>60&&this.props.data.ratePrecent<=80){
          this.setState({rate:4})
        }else if(this.props.data.ratePrecent>80){
          this.setState({rate:5})
        }

        /*setInterval(()=>{
            if(this.state.index==this.props.data.img.length-1){
                this.setState({index:0})
            }else{
                this.setState({index:this.state.index+1})
            }
        },3000)*/
      }
    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };


        
   
    getUsers(user) {   
        axios.put(`${BASE_END_POINT}searchUser`,JSON.stringify({search:user}),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
            .then(response => {
                this.setState({users:response.data})
                
                console.log('USer   ',response.data)
               
            }).catch(error => {
                console.log("user   ",error);
                console.log("users   ",error.response);         
            })   
    }



    getAds = () => {
        let uri = `${BASE_END_POINT}ads?category=${this.props.data.category.id}&onlyMe=false&active=true&country=${this.props.currentCountry.id}&limit=20`
        axios.get(uri)
        .then(response => {
            console.log('ADS   ',response.data)
            this.setState({adsLoading:false,myAds:response.data.data})
            
        }).catch(error => {
            this.setState({adsLoading:false})
            console.log("error   ",error);
            console.log("error   ",error.response);
        })

    }

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.disableDrawer()
    }

   
    follow = () => {
        this.setState({disableFollowButton:true})
        axios.post(`${BASE_END_POINT}follow/${this.props.data.owner.id}/follow`, {}, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
           console.log('Follow Done');
           this.setState({isFollow:true,disableFollowButton:false})
          RNToasty.Success({title:Strings.youFollowHimNow})
          const user = {...this.props.currentUser,user:{...response.data.myUser}}
          console.log('current user')
          console.log(user)
          this.props.getUser(user)
          AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
        }).catch(error => {
            this.setState({disableFollowButton:false})
            if(error.status==403){
                //RNToasty.Info({title:Strings.youFollowHimNow})
            }
            console.log('Follow error  ',error.status);
            console.log('Follow error  ',error.respone);
            console.log('Follow error  ',error);
            
          });
    }

    unFollow = () => {
        this.setState({disableFollowButton:true})
        axios.put(`${BASE_END_POINT}follow/${this.props.data.owner.id}/unfollow`, {}, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
           console.log('un Follow Done');
           this.setState({isFollow:false,disableFollowButton:false})
           const user = {...this.props.currentUser,user:{...response.data.myuser}}
           console.log('current user')
           console.log(user)
           this.props.getUser(user)
           AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
          //RNToasty.Success({title:Strings.you})
        }).catch(error => {
            if(error.status==403){
                //RNToasty.Info({title:Strings.youFollowHimNow})
            }
            this.setState({disableFollowButton:false})
            console.log('unFollow error  ',error.status);
            console.log('unFollow error  ',error.respone);
            console.log('unFollow error  ',error);
            
          });
    }

    fav = (adsId) => {
        
        axios.post(`${BASE_END_POINT}favourite/${this.props.data.id}/ads`, {}, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
           console.log('Fav Done');
           this.setState({favDone:true })
           //this.props.addFavLToist(adsId)
           const user = {...this.props.currentUser,user:{...response.data.user}}
           console.log('current user')
           console.log(user)
           this.props.getUser(user)
           AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
          //RNToasty.Success({title:Strings.youFavouriteThisAds})
        }).catch(error => {
            if(error.status==403){
                //RNToasty.Info({title:Strings.youFollowHimNow})
            }
            console.log('Fav error  ',error.status);
            console.log('Fav error  ',error.respone);
            console.log('Fav error  ',error);
            
          });
    }

    removeFav = (adsId) => {
        
        axios.delete(`${BASE_END_POINT}favourite/${this.props.data.id}`, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
           console.log('un Fav Done');
           this.setState({favDone:false })
           //this.props.removeFavLToist(adsId)
           const user = {...this.props.currentUser,user:{...response.data.user}}
           this.props.getUser(user)
           AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
          //RNToasty.Success({title:Strings.youFavouriteThisAds})
        }).catch(error => {
            if(error.status==403){
                //RNToasty.Info({title:Strings.youFollowHimNow})
            }
            console.log('Fav error  ',error.status);
            console.log('Fav error  ',error.respone);
            console.log('Fav error  ',error);
            
          });
    }

    imageDialog = () =>{
        const {data} = this.props
        const {imgIndex,showImgDialog} = this.state
        return(
            <Modal
            visible={showImgDialog}
            animationType='slide'
            onRequestClose={()=>this.setState({showImgDialog:false})}
            >
                <TouchableOpacity
                style={{margin:moderateScale(6),alignSelf:'flex-end'}}
                onPress={()=>this.setState({showImgDialog:false})}
                >
                    <Icon name='close' type='AntDesign' style={{color:'black',fontWeight:'bold', fontSize:responsiveFontSize(10)}} />
                 </TouchableOpacity>

                 <View style={{width:responsiveWidth(100),height:responsiveHeight(80)}}>
                    <Swiper index={imgIndex} style={{width:responsiveWidth(100),height:responsiveHeight(80)}} autoplay={true}  autoplayTimeout={4}  showsPagination showsButtons={false} activeDot={<View style={{backgroundColor: '#679C8A', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />} activeDotColor='#679C8A' >
                        {data.img.map(i=>(
                        <FastImage
                        resizeMode='contain'
                        source={{uri:i}}
                        style={{width:responsiveWidth(100),height:responsiveHeight(80),justifyContent:'center',alignItems:'center'}}
                        />
                        ))}
                    </Swiper>
                </View>

            </Modal>
        )
    }

    render(){
        const {data,currentUser,isRTL,navigator,favList} = this.props; 
        const {gmailDialog,gmailText,invitationCount} = this.state
        console.log('Data   ',data)  
        return(
            <View style={{ flex:1,backgroundColor:'white' }}>
                <FlatAppHeader 
                update={currentUser?currentUser.user.id==data.owner.id?true:false:false}   
                navigator={navigator} 
                title={Strings.ads}  
                updateAction={()=>{
                    if(data.category.type=='MOTOR'){
                      navigator.push({
                        screen:'UpdateCarAds',
                        animated:true,
                        animationType:'slide-horizontal',
                        passProps:{data:data}
                    })
                    }else if(data.category.type=='JOPS'){
                        navigator.push({
                            screen:'UpdateJobAds',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data}
                        })
                    }else if(data.category.type=='REAL-STATE'){
                      navigator.push({
                          screen:'UpdateBuildingAds',
                          animated:true,
                          animationType:'slide-horizontal',
                          passProps:{data:data}
                      })
                  }
                    else {
                        navigator.push({
                            screen:'UpdateAds',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data}
                        })
                    }

                 
                }}
                /> 
                <ScrollView>
                    <View style={{width:responsiveWidth(100),height:responsiveHeight(40)}}>
                    <Swiper style={{width:responsiveWidth(100),height:responsiveHeight(40)}} autoplay={true}  autoplayTimeout={4}  showsPagination showsButtons={false} activeDot={<View style={{backgroundColor: '#679C8A', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />} activeDotColor='#679C8A' >
                        {data.img.map((i,index)=>(
                            <TouchableOpacity onPress={()=>this.setState({showImgDialog:true, imgIndex:index})}>
                                <ImageBackground
                                source={{uri:i}}
                                style={{width:responsiveWidth(100),height:responsiveHeight(40),justifyContent:'center',alignItems:'center'}}
                                />
                            </TouchableOpacity>
                        ))}
                    </Swiper>
                    </View>
                    {/*
                    <ImageBackground
                    source={{uri:data.img[this.state.index]}}
                     style={{width:responsiveWidth(100),height:responsiveHeight(40),justifyContent:'center',alignItems:'center'}}>
                        <View style={{width:responsiveWidth(100),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>
                            <TouchableOpacity
                            onPress={()=>{
                                if(this.state.index>0){
                                    this.setState({index:this.state.index-1})
                                }
                            }}
                             style={{marginHorizontal:moderateScale(5), backgroundColor:'#5B8A7B', height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'}} >
                                <Icon name={isRTL?'chevron-right':'chevron-left'} type='Entypo' style={{fontSize:responsiveFontSize(7),color:'white'}} />
                            </TouchableOpacity>
                            <TouchableOpacity
                            onPress={()=>{
                                if(this.state.index==data.img.length-1){
                                    this.setState({index:0})
                                }else{
                                    this.setState({index:this.state.index+1})
                                }
                            }}
                             style={{marginHorizontal:moderateScale(5), backgroundColor:'#5B8A7B', height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'}} >
                                <Icon name={!isRTL?'chevron-right':'chevron-left'} type='Entypo' style={{fontSize:responsiveFontSize(7),color:'white'}} />
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                    */}
                    <View style={{marginTop:moderateScale(-9),flexDirection:isRTL?'row-reverse':'row',  alignSelf:isRTL?'flex-start':'flex-end',justifyContent:'center',alignItems:'center',marginHorizontal:moderateScale(5)}}>
                            
                            {currentUser&&
                            <TouchableOpacity
                             onPress={()=>{
                                 if(this.props.currentUser){
                                    navigator.push({
                                        screen: 'Complaints',
                                        animated: true,
                                        animationType:'slide-horizontal',
                                        passProps:{
                                            data:data
                                        }
                                    })
                                }else{
                                    navigator.push({
                                        screen: 'Login',
                                        animated: true,
                                    })
                                }
                             }}
                             style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1,elevation:2,marginHorizontal:moderateScale(0), backgroundColor:'white', height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'}} >
                                <Icon name='shield-off' type='Feather' style={{fontSize:responsiveFontSize(10), color:'#5B8A7B'}} />
                            </TouchableOpacity>
                           
                            }
                            
                            {currentUser?
                            !currentUser.user.blockedFrom.includes(data.owner.id)&&
                            <TouchableOpacity
                             onPress={()=>{
                                if(currentUser){
                                    if(currentUser.user.favourite.includes(data.id)){
                                        this.removeFav(data.id)
                                    }else{
                                        this.fav(data.id)
                                    }
                                    

                                }else{
                                    navigator.push({
                                        screen: 'Login',
                                        animated: true,
                                        animationType:'slide-horizontal'
                                    })
                                }
                             }}
                             style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1,elevation:2,marginHorizontal:moderateScale(5), backgroundColor:'white', height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'}} >
                                <Icon name='heart' type='AntDesign' style={{fontSize:responsiveFontSize(7), color:!currentUser?'gray': currentUser.user.favourite.includes(data.id)?'red':this.state.favDone?'red':'gray'}} />
                            </TouchableOpacity>
                            :null
                            }
                            <TouchableOpacity
                            onPress={()=>{
                               
                                const url =`https://www.google.com/maps/search/?api=1&query=${data.location[0]},${data.location[1]}`
                                //"https://www.google.de/maps/@" +data.location[0] +"," +data.location[1] +"?q=test";
                                
                               
                                Share.share({
                                    message:` "هذا الاعلان تم مشاركتة من خلال تطبيق اعلنها" \n \n "عنوان الاعلان" \n \n ${data.title} \n \n "تفاصيل الاعلان" \n \n ${data.description} \n \n "رابط صورة الاعلان" \n \n ${data.img[0]} \n \n "موقع الاعلان على الخريطة" \n \n ${url} \n \n "رابط تحميل التطبيق" \n \n "https://play.google.com/store/apps/details?id=com.a3lnhamobileapp" ` ,
                                    url: 'http://bam.tech',
                                    title: data.title,
                                  }, {
                                    // Android only:
                                    dialogTitle: 'Share',
                                    // iOS only:
                                    excludedActivityTypes: [
                                      'com.apple.UIKit.activity.PostToTwitter'
                                    ]
                                  })
                            }}
                             style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1,elevation:2, backgroundColor:'white', height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'}} >
                                <Icon name='share' type='Entypo' style={{fontSize:responsiveFontSize(7), color:'#5B8A7B'}} />
                            </TouchableOpacity>
                    </View>

                    <Text style={{fontFamily:'Roboto-Regular',direction:isRTL?'rtl':'ltr', textAlign:isRTL?'right':'left', fontSize:responsiveFontSize(8),color:'#8BB4A6', width:responsiveWidth(90),alignSelf:'center',marginTop:moderateScale(4)}}>
                       {data.title}
                    </Text>

                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),alignSelf:'center',marginTop:moderateScale(5)}}>
                        <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black'}}>{data.country.currency} {data.price}</Text>
                        <Text style={{fontFamily:'Roboto-Regular',color:'#CFCFCF',fontSize:responsiveFontSize(7)}} ></Text>
                    </View>

                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),alignSelf:'center',marginTop:moderateScale(5)}}>
                        <TouchableOpacity
                         onPress={()=>{
                             if(currentUser){
                             if(!currentUser.user.blockedFrom.includes(data.owner.id))
                              if(data.owner.id == currentUser.user.id){
                                navigator.push({
                                    screen:'Profile',
                                    animated:true,
                                    animationType:'slide-horizontal'
                                    //passProps:{data:data.owner}
                                })
                              }else{
                                navigator.push({
                                    screen:'FriendProfile',
                                    animated:true,
                                    animationType:'slide-horizontal',
                                    passProps:{data:data.owner}
                                })
                              }
                            }else{
                                navigator.push({
                                    screen:'Login',
                                    animated:true,
                                    animationType:'slide-horizontal',
                                    passProps:{data:data.owner}
                                })
                            }
                               
                        }}
                         style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center',}}>
                            
                            <FastImage source={ 'img' in data.owner ? {uri:data.owner.img} : require('../assets/imgs/profileicon.jpg')}  style={{width:60,height:60,borderRadius:30}} />
                           

                            <Text style={{fontFamily:'Roboto-Regular',color:'black',fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(3)}}>{data.owner.username}</Text>
                        </TouchableOpacity>
                        {this.props.currentUser&&data.owner.id!=this.props.currentUser.user.id&&!currentUser.user.blockedFrom.includes(data.owner.id)&&
                        <TouchableOpacity
                        disabled={this.state.disableFollowButton}
                        onPress={()=>{
                           if(currentUser){
                               if(currentUser.user.following.includes(data.owner.id)){
                                console.log('exist follow')
                                this.unFollow()
                               }else{
                                console.log('not exist follow')
                                this.follow()
                               }
                            
                           }else{
                            navigator.push({
                                screen: 'Login',
                                animated: true,
                                animationType:'slide-horizontal',
                            })
                           }
                        }}
                         style={{backgroundColor:'#679C8A', height:responsiveHeight(7),width:responsiveWidth(23),borderRadius:moderateScale(4),justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontFamily:'Roboto-Regular',alignSelf:'center',textAlign:'center', color:'white',fontSize:responsiveFontSize(6)}}>{!currentUser?Strings.follow: currentUser.user.following.includes(data.owner.id)?Strings.unFollow:this.state.isFollow? Strings.unFollow:Strings.follow}</Text>
                        </TouchableOpacity>
                        }
                    </View>

                    <View style={{width:responsiveWidth(90),alignSelf:'center',marginTop:moderateScale(5)}}>
                    <View style={{alignSelf:isRTL?'flex-end':'flex-start', flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between',width:responsiveWidth(50)}} >
                    <Stars   
                    disabled                   
                       default={this.state.rate}
                       count={5}
                       half={true}
                       fullStar={<Icon name='star' type='MaterialCommunityIcons' style={[styles.myStarStyle]}/>}
                       emptyStar={<Icon name={'star-outline'} type='MaterialCommunityIcons' style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
                       halfStar={<Icon name={'star-half'} type='MaterialIcons' style={[styles.myStarStyle]}/>}
                   />
                        <Text style={{fontFamily:'Roboto-Regular',color:'#CFCFCF',fontSize:responsiveFontSize(6)}} >{data.rate +' ('+data.rateNumbers+')'}</Text>
                    </View>
                    </View>
                    
                    
                    <View style={{alignSelf:'center', flexDirection:isRTL?'row-reverse':'row',alignItems:'center', width:responsiveWidth(90),marginTop:moderateScale(4)}}>
                       <Icon name='location-pin' type='Entypo' style={{fontSize:responsiveFontSize(9), padding:0,margin:0, color:'#CFCFCF'}} />
                    <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(7),color:'#CFCFCF', }}>
                           {isRTL?data.country.arabicName:data.country.countryName}
                       </Text>
                    </View>
                    
                    {data.category.type=='REAL-STATE'&&
                    <View style={{elevation:1,backgroundColor:'white', borderWidth:0.5,borderColor:'white', width:responsiveWidth(90),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',marginTop:moderateScale(8)}} >
                        <View style={{marginVertical:moderateScale(7), flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center',flex:1}} >
                            <Icon name='bed' type='FontAwesome5' style={{fontSize:responsiveFontSize(10),color:'#ECE9CF'}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(4), colorL:'#DFDFDF'}} >{data.beds} {Strings.bed}</Text>
                        </View>
                        <View style={{marginVertical:moderateScale(7), flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center',flex:1}} >
                            <Icon name='bath' type='FontAwesome5' style={{fontSize:responsiveFontSize(10),color:'#ECE9CF'}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(4), colorL:'#DFDFDF'}} >{data.baths} {Strings.baths}</Text>
                        </View>
                        <View style={{marginVertical:moderateScale(7), flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center',flex:1}} >
                            <Icon name='subdirectory-arrow-right' type='MaterialCommunityIcons' style={{fontSize:responsiveFontSize(10),color:'#ECE9CF'}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(4), colorL:'#DFDFDF'}} >{data.space} {Strings.space}</Text>
                        </View>
                    </View>
                    }

                {data.category.type=='MOTOR'&&
                <View style={{borderBottomWidth:1,borderBottomColor:'#d7dade', flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center', alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(8)}} >
                    <View style={{marginVertical:moderateScale(3), flex:1, flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                        <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {Strings.brand+' : '}
                        </Text>
                        <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {data.brand}
                        </Text>
                    </View>

                    <View style={{marginVertical:moderateScale(3),flex:1, flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                        <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {Strings.modal+' : '}
                        </Text>
                        <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {data.modal}
                        </Text>
                    </View>

                </View>
                }

            {data.category.type=='MOTOR'&&
                <View style={{borderBottomWidth:1,borderBottomColor:'#d7dade', flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center', alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(8)}} >
                    <View style={{marginVertical:moderateScale(3), flex:1, flexDirection:isRTL?'row-reverse':'row', alignItems:'center'}}>
                        <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {Strings.year+' : '}
                        </Text>
                        <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {data.year}
                        </Text>
                    </View>

                    <View style={{ marginVertical:moderateScale(3),flex:1, flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                        <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {Strings.color+' : '}
                        </Text>
                        <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {isRTL?data.color.arabicColorName:data.color.colorName}
                        </Text>
                    </View>

                </View>
                }

                {/*data.category.type=='MOTOR'&&
                <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center', alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(8)}} >
                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {Strings.usage+' : '}
                        </Text>
                        <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {data.usage}
                        </Text>
                    </View>

                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {Strings.color+' : '}
                        </Text>
                        <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {data.color}
                        </Text>
                    </View>

                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {Strings.mileage+' : '}
                        </Text>
                        <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                        {data.mileage}
                        </Text>
                    </View>

                </View>
                */}


                
              
                

                

                {data.category.type=='MOTOR'&&
                <View style={{alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(8)}}>
                <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                     {Strings.mileage}
                </Text>
                <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
               {data.mileage}
                </Text>
                </View>
                }

             
                    <View style={{alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(8)}}>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                            {Strings.category}
                       </Text>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                       {isRTL?data.category.arabicName:data.category.name}
                       </Text>
                    </View>

                    <View style={{alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(8)}}>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                            {Strings.subCategory}
                       </Text>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                       {isRTL?data.subCategory.arabicName:data.subCategory.name}
                       </Text>
                    </View>

                    <View style={{alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(8)}}>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                            {Strings.description}
                       </Text>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(6),color:'#CFCFCF', alignSelf:isRTL?'flex-end':'flex-start'}}>
                      {data.description}
                       </Text>
                    </View>


                    <View style={{marginVertical:moderateScale(7),alignSelf:'center',height:responsiveHeight(40),width:responsiveWidth(90),marginTop:moderateScale(5)}}>
                        <MapView
                        style={{height:responsiveHeight(40),width:responsiveWidth(90),}}
                        region={{
                            latitude: data.location[0],
                            longitude: data.location[1],
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                        showsCompass
                        showsIndoors
                        showsUserLocation
                        showsTraffic
                        zoomControlEnabled
                        >
                            <Marker
                            coordinate={{
                            latitude: data.location[0],
                            longitude: data.location[1],
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                            }}
                            />
                        </MapView>
                    </View>


                    <View style={{alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(5)}}>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                            {Strings.participants}
                       </Text>
                       <View style={{marginTop:moderateScale(5), width:responsiveWidth(90),justifyContent:'space-between',alignItems:'center', flexDirection:isRTL?'row-reverse':'row',alignItems:'center',alignSelf:isRTL?'flex-end':'flex-start'}}>
                           <View  style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row',}}>
                                <FastImage source={require('../assets/imgs/profileicon.jpg')} style={{width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(0.5)}} />
                                <FastImage source={require('../assets/imgs/profileicon.jpg')} style={{width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(0.5)}} />
                                <FastImage source={require('../assets/imgs/profileicon.jpg')} style={{width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(0.5)}} />
                                <View style={{backgroundColor:'#679C8A', justifyContent:'center',alignItems:'center', width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(0.5)}}>
                                    <Text style={{fontFamily:'Roboto-Regular',color:'white'}} >+{data.participate.length+data.invitationCount+invitationCount}</Text>
                                </View>
                           </View>

                           <TouchableOpacity
                            onPress={()=>{
                                if(this.props.currentUser){
                                    this.setState({typeDialog:true})
                                    
                                }else{
                                    navigator.push({
                                        screen:'Login',
                                        animated:true,
                                        animationType:'slide-horizontal'
                                    })
                                }
                            }}
                            style={{backgroundColor:'#679C8A', justifyContent:'center',alignItems:'center', width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(0.5)}}>
                               <Icon name='user-plus' type='FontAwesome5' style={{fontSize:responsiveFontSize(6), color:'white'}} />
                           </TouchableOpacity>
                           
                       
                    </View>
                    </View>
                    
                    {this.props.currentUser?
                    data.owner.id!=this.props.currentUser.user.id&&!currentUser.user.blockedFrom.includes(data.owner.id)&&
                    <View style={{alignSelf:'center', width:responsiveWidth(90),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',marginTop:moderateScale(7)}}> 
                        <TouchableOpacity
                        onPress={()=>{
                            navigator.push({
                                screen: 'DirectChat',
                                animated:true,
                                animationType:'slide-horizontal',
                                passProps:{data:data.owner}
                            })
                           // Communications.text(data.owner.phone[0])
                        }}
                         style={{height:responsiveHeight(7), borderRadius:moderateScale(4), backgroundColor:'#679C8A', width:responsiveWidth(28),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                            <Icon name='email' type='MaterialCommunityIcons' style={{color:'white',fontSize:responsiveFontSize(7)}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), color:'white',fontSize:responsiveFontSize(7)}}>{Strings.chat}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        onPress={()=>{
                            if(data.owner.phone[0]){
                            Communications.phonecall(data.owner.phone[0], true)
                            }else{
                                RNToasty.Warn({title:strings.noPhone})
                            }

                        }}
                         style={{height:responsiveHeight(7),borderRadius:moderateScale(4), backgroundColor:'#679C8A', width:responsiveWidth(25),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                            <Icon name='md-call' type='Ionicons' style={{color:'white',fontSize:responsiveFontSize(7)}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), color:'white',fontSize:responsiveFontSize(7)}}>{Strings.call}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        onPress={()=>{
                            Communications.email([data.owner.email],null,null,'','')
                        }}
                         style={{height:responsiveHeight(7), borderRadius:moderateScale(4), backgroundColor:'#679C8A', width:responsiveWidth(25),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                            <Icon name='email' type='MaterialCommunityIcons' style={{color:'white',fontSize:responsiveFontSize(7)}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), color:'white',fontSize:responsiveFontSize(7)}}>{Strings.email}</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{alignSelf:'center', width:responsiveWidth(60),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',marginTop:moderateScale(7)}}> 
                        <TouchableOpacity
                        onPress={()=>{
                            if(data.owner.phone[0]){
                            Communications.phonecall(data.owner.phone[0], true)
                            }else{
                                RNToasty.Warn({title:strings.noPhone})
                            }

                        }}
                        style={{height:responsiveHeight(7),borderRadius:moderateScale(4), backgroundColor:'#679C8A', width:responsiveWidth(25),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                            <Icon name='md-call' type='Ionicons' style={{color:'white',fontSize:responsiveFontSize(7)}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), color:'white',fontSize:responsiveFontSize(7)}}>{Strings.call}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        onPress={()=>{
                            Communications.email([data.owner.email],null,null,'','')
                        }}
                        style={{height:responsiveHeight(7), borderRadius:moderateScale(4), backgroundColor:'#679C8A', width:responsiveWidth(25),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                            <Icon name='email' type='MaterialCommunityIcons' style={{color:'white',fontSize:responsiveFontSize(7)}} />
                            <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(3), color:'white',fontSize:responsiveFontSize(7)}}>{Strings.email}</Text>
                        </TouchableOpacity>
                </View>   
                    }
                 {currentUser?
                 !currentUser.user.blockedFrom.includes(data.owner.id)&&
                    <TouchableOpacity
                    onPress={()=>{
                        this.props.navigator.push({
                            screen:'AdsComments',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{adsId:data.id}
                        })
                    }}
                     style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'center',alignSelf:'center',marginTop:moderateScale(10)}} >
                        <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(2), color:'black',fontSize:responsiveFontSize(7)}} >{Strings.comments}</Text>
                        <Icon name={isRTL?'arrowleft':'arrowright'} type='AntDesign' style={{fontSize:responsiveFontSize(7)}} />
                    </TouchableOpacity>
                    :null
                 }    

                    <View style={{alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(4)}}>
                       <Text style={{fontFamily:'Roboto-Regular',fontSize:responsiveFontSize(7),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                            {Strings.otherAds}
                       </Text>
                       {this.state.adsLoading?                      
                       <LottieView
                       style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                       source={require('../assets/animations/smartGarbageLoading.json')}
                       autoPlay
                       loop
                       />
                        :
                       <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        data={this.state.myAds}
                        renderItem={({item})=>(
                            <HomeAdsCard onPress={()=>{
                                this.props.navigator.push({
                                 screen: item.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                                 animated:true,
                                 animationType:'slide-horizontal',
                                 passProps:{data:item}
                               })
                           }} navigator={this.props.navigator} data={item} />
                        )}
                        />
                        }
                    </View>

                </ScrollView>

                <Dialog
            width={responsiveWidth(90)}
            onHardwareBackPress={()=>{this.setState({ gmailDialog: false })}}
            visible={this.state.gmailDialog}
            onTouchOutside={() => {
            this.setState({ gmailDialog: false });
            }}
            >    
            <View style={{width:responsiveWidth(90)}}>
               <Text style={{alignSelf:'center', marginTop:moderateScale(15),fontSize:responsiveFontSize(7.5),color:'black'}} >{Strings.shareToGmail}</Text>
               <View style={{marginTop:moderateScale(15), borderRadius:moderateScale(4), width:responsiveWidth(80),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',borderWidth:1,borderColor:'#C6C6C6', height:responsiveHeight(8)}}>
                <TextInput 
                onChangeText={(val)=>{
                    this.setState({gmailText:val})
                }}
                value={this.state.gmailText}
                 style={{width:responsiveWidth(70)}} />
                <Icon name='search' type='EvilIcons' style={{color:'#C6C6C6',fontSize:responsiveFontSize(10)}} />
               </View>

    

                <TouchableOpacity
                onPress={()=>{
                   if(this.state.gmailText.replace(/\s/g, '').length)
                   {
                       console.log("Email   ",this.state.gmailText," ID : ",data.id)
                       const data2 = {
                        email:this.state.gmailText,
                        message:"https//www.google.com"
                       }
                       axios.post(`${BASE_END_POINT}ads/share/${data.id}`,JSON.stringify(data2),{
                        headers: {
                          'Content-Type': 'application/json',
                          'Authorization': `Bearer ${this.props.currentUser.token}`
                        },
                      })
                       .then(response=>{
                            RNToasty.Success({title:Strings.gmaiShare})
                            this.setState({invitationCount:invitationCount+1,gmailText:'',gmailDialog:false})
                       })
                       .catch(error=>{
                           console.log('error  ',error.response)
                       })
                   }else{
                    RNToasty.Error({title:Strings.enterCorrectEmail})
                   }
                }}
                 style={{alignSelf:'center', marginHorizontal:moderateScale(10),marginBottom:moderateScale(20),  marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',width:responsiveWidth(30),height:responsiveHeight(8),borderRadius:moderateScale(4),backgroundColor:'#679C8A'}}>
                    <Text style={{color:'white',fontSize:responsiveFontSize(7)}}>{Strings.done}</Text>
                </TouchableOpacity>
            </View>        
            </Dialog>

                <Dialog
                width={responsiveWidth(90)}
                onHardwareBackPress={()=>{this.setState({ typeDialog: false })}}          
                visible={this.state.typeDialog}
                onTouchOutside={() => {
                this.setState({ typeDialog: false });
                }}
                >   
                <View style={{width:responsiveWidth(90),marginVertical:moderateScale(10)}}>
                
                <TouchableOpacity
                 onPress={()=>{
                     this.setState({typeDialog:false,inAppDialog:true})
                    
                }}
                 style={{width:responsiveWidth(90),borderBottomColor:'#679C8A',borderBottomWidth:1 ,height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(5)}}>
                    <FastImage resizeMode='center' style={{width:responsiveWidth(7),height:responsiveHeight(6)}} source={require('../assets/imgs/appLogo.png')}/>
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}}>{Strings.inApp}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>{
                    this.setState({typeDialog:false,gmailDialog:true})
                }}
                 style={{width:responsiveWidth(90),height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(7)}}>
                    <Icon type='MaterialCommunityIcons' name='gmail' style={{fontSize:responsiveFontSize(8),color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}} >{Strings.gmail}</Text>
                </TouchableOpacity>

                
                </View>        
                </Dialog>

                <Dialog
            width={responsiveWidth(90)}
            onHardwareBackPress={()=>{this.setState({ inAppDialog: false })}}
            visible={this.state.inAppDialog}
            onTouchOutside={() => {
            this.setState({ inAppDialog: false });
            }}
            >    
            <View style={{width:responsiveWidth(90)}}>
               <Text style={{alignSelf:'center', marginTop:moderateScale(15),fontSize:responsiveFontSize(7.5),color:'black'}} >{Strings.selectParticipants}</Text>
               
               <View style={{marginTop:moderateScale(15), borderRadius:moderateScale(2), width:responsiveWidth(80),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',flexWrap:'wrap'}}>
                {this.state.participantsName.map((name,index)=>(
                    <TouchableOpacity 
                     onLongPress={()=>{
                          this.state.participantsName.splice(index,1);
                          this.state.participants.splice(index,1);
                         this.setState({ff:true,})

                     }}
                    style={{flexDirection:'row',alignItems:'center', justifyContent:'space-between', marginTop:moderateScale(3), padding:moderateScale(2), borderRadius:3, marginHorizontal:moderateScale(1),color:'black',backgroundColor:'#ebebeb', }}> 
                    <Text style={{color:'black',marginHorizontal:moderateScale(3)}}>{name}</Text>
                    <Icon name='close' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(5)}} />
                    </TouchableOpacity>
                ))}
               </View>
               
               <View style={{marginTop:moderateScale(15), borderRadius:moderateScale(4), width:responsiveWidth(80),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',borderWidth:1,borderColor:'#C6C6C6', height:responsiveHeight(8)}}>
                <TextInput 
                onChangeText={(val)=>{
                    console.log("ooo   ",val)
                    this.setState({val:val})
                    this.getUsers(val)
                }}
                value={this.state.val}
                 style={{width:responsiveWidth(70)}} />
                <Icon name='search' type='EvilIcons' style={{color:'#C6C6C6',fontSize:responsiveFontSize(10)}} />
               </View>

               <ScrollView style={{maxHeight:responsiveHeight(30), borderRadius:moderateScale(4), marginTop:moderateScale(0.5),backgroundColor:'white',elevation:2,width:responsiveWidth(80),alignSelf:'center'}} >
                {this.state.users.map(val=>(
                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({invitationCount:this.state.invitationCount+1, participantsName:[...this.state.participantsName,val.email], participants:[...this.state.participants,val.id], val:'',users:[]})
                    }}
                     style={{ borderBottomColor:'gray',borderBottomWidth:0.3, width:responsiveWidth(80),height:responsiveHeight(8),justifyContent:'center'}} >
                        <Text style={{marginHorizontal:moderateScale(5)}}>{val.email}</Text>
                    </TouchableOpacity>
                ))}
               </ScrollView>

                <TouchableOpacity
                onPress={()=>{
                    console.log("len   ",this.state.participantsName.length>0)
                   if(this.state.participantsName.length>0){
                    axios.post(`${BASE_END_POINT}/ads/shareInside/${data.id}`,JSON.stringify({
                        participate:this.state.participants
                    }) , {
                        headers: {
                          'Content-Type': 'application/json',
                          'Authorization': `Bearer ${this.props.currentUser.token}`
                        },
                      }).then(response=>{
                        console.log('done  ',response)
                        RNToasty.Success({title:Strings.done})
                        this.setState({participants:[],participantsName:[], inAppDialog:false,id:null,val:'',users:[]})
                      }).catch(error=>{
                        console.log('error  ',error)
                        console.log('error  ',error.response)
                      })
                    
                }
                }}
                 style={{marginHorizontal:moderateScale(10),marginBottom:moderateScale(20), alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',width:responsiveWidth(30),height:responsiveHeight(8),borderRadius:moderateScale(4),backgroundColor:'#679C8A'}}>
                    <Text style={{color:'white',fontSize:responsiveFontSize(7)}}>{Strings.done}</Text>
                </TouchableOpacity>
            </View>        
            </Dialog>
     
                {this.imageDialog()}

            </View>
            
        );
    }
}

const styles = StyleSheet.create({
    myStarStyle: {
      color: '#FFC416',
      backgroundColor: 'transparent',
      //textShadowColor: 'black',
      //textShadowOffset: {width: 1, height: 1},
      //textShadowRadius: 2,
      fontSize:responsiveFontSize(10),
      margin:0,
      padding:0,
    },
    myEmptyStarStyle: {
      color: 'gray',
      fontSize:responsiveFontSize(10),
      margin:0,
      padding:0,
    }
  });

const mapDispatchToProps = {
    getUser,
    removeItem,
    addFavLToist,
    removeFavLToist
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color ,
    favList:state.fav.favList,
    currentCountry:state.auth.currentCountry,
})

export default connect(mapToStateProps,mapDispatchToProps)(AdsDescription);


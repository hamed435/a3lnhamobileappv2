import React,{Component} from 'react';
import {Alert, ScrollView, TextInput,ImageBackground,ActivityIndicator, Modal, View,RefreshControl,StyleSheet,NetInfo,Text,TouchableOpacity,FlatList} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import {Icon,Picker} from 'native-base'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import FavoritueCard from '../components/FavoritueCard'
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import FastImage from 'react-native-fast-image'
import ListFooter from '../components/ListFooter';
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import HomeAdsCard from '../components/HomeAdsCard'
//import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import RangeSlider from 'rn-range-slider';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import Checkbox from 'react-native-custom-checkbox';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    MenuProvider 
  } from 'react-native-popup-menu';
  

var postion=-1;


class CategoryDetails extends Component {

    page=1;
    state= {
        errorText:null,
       // ads: new DataProvider(),
       ads: [],
        refresh:false,
        loading:true,
        pages:null,
       
        showModal:false,
        filterLoading:false,
        selectedAddress: ' ',
        priceTo:5000,
        priceFrom:1000,
        result:[],
        rent:'month',
        color:1,
        beds:3,
        baths:1,
        space:85,
        selectedJobType:'Full Time',
        jopRequire:' ',

        dafault:true,
        categoryId:this.props.data.id,
        subCategoryId:-1,
        subCategory:false,
        sort:false,

        color2:'red',
        year:' ',
        usage:'New',

        selectedBrand:0,
        brandLoad:true,
        brands:[],
        brandText:[],
        brandImage:'',
        showBrandModal:false,

        i:-1,
        x:0,
        y:0,
        soretDialog:false,
        open:false,

        color2:'red',
        colorId:0,     
        colores:[],
        

        cities:[],
        selectedCity:null,

        selectedSubcategory2:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };

    componentWillUnmount(){
        this.props.removeItem()
    }

    constructor(props) {
        super(props);     
        this.renderLayoutProvider = new LayoutProvider(
            () => 2,
            (type, dim) => {
              dim.width = responsiveWidth(50);
              dim.height = responsiveHeight(35);
            },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getAds(this.page,false,false,-1);
                this.getBrands()
                this.getColors()
                this.getCities()
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getAds(this.page,true,false,-1);
                    this.getBrands()
                    this.getColors()
                    this.getCities()
                }
            }
          );      
    }

    getCities() {   
        axios.get(`${BASE_END_POINT}countries/${this.props.currentCountry.id}/cities`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
            },
          })
            .then(response => {
                console.log('Citirs   ',response.data.data)
                this.setState({cities:response.data.data,selectedCity:response.data.data[0].id})
                
            
            }).catch(error => {
                console.log("Citirs error   ",error);
                console.log("error   ",error.response);         
            })   
    }

    getColors() {   
        axios.get(`${BASE_END_POINT}color`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
            },
          })
            .then(response => {
                console.log('Citirs   ',response.data.data)
                this.setState({colores:response.data.data,color2:response.data.data[0].colorName,colorId:response.data.data[0].id})
                
            
            }).catch(error => {
                console.log("Citirs error   ",error);
                console.log("error   ",error.response);         
            })   
    }
    

    getBrands() {
        this.setState({brandLoad:true})   
        axios.get(`${BASE_END_POINT}brand`)
            .then(response => {
                this.setState({brandText:response.data.data[0].brandname,brandImage:response.data.data[0].img, brands:response.data.data,selectedBrand:response.data.data[0].id,brandLoad:false})
                console.log('brand   ',response.data)
            }).catch(error => {
                this.setState({brandLoad:false})
                console.log("error   ",error);
                console.log("error   ",error.response);         
            })   
    }

    _menu = null;
 
    setMenuRef = ref => {
      this._menu = ref;
    };
   
    hideMenu = () => {
      this._menu.hide();
    };
   
    showMenu = () => {
      this._menu.show();
    };
    

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

    getAds(page, refresh,sort,subCatId) {
            //this.setState({sort:sort})
            console.log("CAT ID   ",this.props.data.id)
            //let uri = `${BASE_END_POINT}ads?category=${this.props.data.id}&sortByPrice=${sort}&page=${page}&limit=20`
            let uri = `${BASE_END_POINT}ads?category=${this.props.data.id}&active=true&onlyMe=false&country=${this.props.currentCountry.id}&page=${page}&limit=20`
            if(subCatId>-1){
                uri=`${uri}&subCategory=${subCatId}`
                console.log("Selected sub cat")
            }
            if(sort){
                uri=`${uri}&sortByPrice=true`
                console.log("Selected sort")
            }


            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        ads:refresh?response.data.data:[...this.state.ads,...response.data.data],
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                       // ads: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.ads.getAllData(), ...response.data.data]),
                    })

                    console.log("MYFAV   ",response.data.data)
                
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
            <HomeAdsCard onPress={()=>{
               this.props.navigator.push({
                screen:data.category.name=='Jops'?'JobAdsDescription':'AdsDescription',
                animated:true,
                animationType:'slide-horizontal',
                passProps:{data:data}
              })
            }} navigator={this.props.navigator} data={data}
             />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

      getSubCategoriesAds(page, refresh,catId,sort) {
        //this.setState({loading:true})
        
        console.log("CAT ID   ",this.props.data.id)
        console.log("SUB CAT ID   ",this.state.subCategoryId)
        this.setState({subCategoryId:catId,sort:sort, dafault:false ,loading:false, ads: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows([]),})
        let uri = `${BASE_END_POINT}ads?category=${this.props.data.id}&onlyMe=false&active=true&country=${this.props.currentCountry.id}&subCategory=${catId}&sortByPrice=${sort}&page=${page}&limit=20`

        if (refresh) {
            this.setState({loading: false, refresh: true})
        } else {
            this.setState({refresh:false,})
        }
        axios.get(uri)
            .then(response => {
                this.setState({
                    loading:false,
                    refresh:false,
                    pages:response.data.pageCount,
                    errorText:null,
                    ads: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.ads.getAllData(), ...response.data.data]),
                })

                console.log("MY SUB ADS   ",response.data.data)
            
            }).catch(error => {
                this.setState({loading:false,refresh:false})
                console.log(error.response);
                if (!error.response) {
                    this.setState({errorText:Strings.noConnection})
                }
            })
    
}

      subCategoriesSection = () =>{
          const {isRTL,data} = this.props
          return(
            <View style={{width:responsiveWidth(100),marginTop:moderateScale(2)}}>
               {/* <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(5), alignSelf:isRTL?'flex-end':'flex-start', color:'black',fontSize:responsiveFontSize(7),marginBottom:moderateScale(3)}} >{Strings.selectCategory}</Text>*/}
                <ScrollView  showsHorizontalScrollIndicator={false} horizontal style={{width:responsiveWidth(100),marginTop:moderateScale(0)}}>
                    {data.child.map((item,index)=>(
                        <TouchableOpacity
                        onPress={()=>{    
                            // Alert.alert("index  "+index) 
                             //postion=index              
                             this.setState({i:index, subCategoryId:item.id})
                             this.page=1;
                             this.getAds(1,true,this.state.sort,item.id)
                             //this.getSubCategoriesAds(1,false,item.id,this.state.sort)
                         }}
                         style={{ margin:moderateScale(0),marginHorizontal:moderateScale(6), justifyContent:'center',alignItems:'center'}}>
                        <View
                         style={{backgroundColor:this.state.i==index?'#679C8A':'white', elevation:2, borderRadius:35, width:70,height:70,justifyContent:'center',alignItems:'center',shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1}} >
                            <FastImage style={{width:40,height:40}} source={{uri:item.img}} />
                        </View>
                        <Text style={{marginTop:moderateScale(2), color:'#679C8A',fontSize:responsiveFontSize(6)}} >{isRTL?item.arabicName:item.name}</Text>
                        </TouchableOpacity>
                         /*<TouchableOpacity
                         onPress={()=>{    
                            // Alert.alert("index  "+index) 
                             //postion=index              
                             this.setState({i:index, subCategoryId:item.id})
                             this.page=1;
                             this.getAds(1,true,this.state.sort,item.id)
                             //this.getSubCategoriesAds(1,false,item.id,this.state.sort)
                         }}
                          style={{marginBottom:moderateScale(3), elevation:2, borderRadius:moderateScale(1), backgroundColor:this.state.i==index?'#679C8A':'white', minWidth:responsiveWidth(30),height:responsiveHeight(6),justifyContent:'center',alignItems:'center',marginHorizontal:moderateScale(5),borderColor:'#679C8A',borderWidth:1}} >
                             <Text style={{marginHorizontal:moderateScale(5), textAlign:'center', color:this.state.i==index?'white':'#679C8A',fontSize:responsiveFontSize(6)}} >{isRTL?item.arabicName:item.name}</Text>
                         </TouchableOpacity> */
                    ))}
                </ScrollView>
               {/* <FlatList 
                showsHorizontalScrollIndicator={false}
                horizontal
                data={data.child}
                renderItem={({item,index})=>{
                   // state={postion:-1,}
                return(
                <TouchableOpacity
                onPress={()=>{    
                   // Alert.alert("index  "+index) 
                    //postion=index              
                    this.setState({subCategoryId:item.id})
                    this.page=1;
                    this.getAds(1,true,this.state.sort,item.id)
                    //this.getSubCategoriesAds(1,false,item.id,this.state.sort)
                }}
                 style={{marginBottom:moderateScale(3), elevation:2, borderRadius:moderateScale(1), backgroundColor:'white', minWidth:responsiveWidth(30),height:responsiveHeight(6),justifyContent:'center',alignItems:'center',marginHorizontal:moderateScale(5)}} >
                    <Text style={{marginHorizontal:moderateScale(5), textAlign:'center', color:'#679C8A',fontSize:responsiveFontSize(6)}} >{isRTL?item.arabicName:item.name}</Text>
                </TouchableOpacity> 
                )}}
                />
            */}
            </View>
          ) 
      }

      filterAndSortSection = () =>{
          const {data,isRTL} = this.props
          return(
              
              <View style={{borderBottomColor:'#d7dade',borderBottomWidth:0.5, marginVertical:moderateScale(5), width:responsiveWidth(100),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                  <TouchableOpacity
                  onPress={()=>this.setState({showModal:true})}
                   style={{flex:1, flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}} >
                        <Icon name='filter-variant' type='MaterialCommunityIcons' style={{color:'#AAAAAA'}} />
                        <Text style={{color:'#AAAAAA', marginHorizontal:moderateScale(3)}} >{Strings.filter}</Text>
                  
                  </TouchableOpacity>

                  
                  <Menu
                 
                  onBackdropPress={()=>{this.setState({open:false})}}
                  //opened={true}
                 // onSelect={(val)=>{this.setState({open:true})}}
                   opened={this.state.open}
                   style={{flex:1, flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                    <MenuTrigger 
                    onPress={()=>{this.setState({open:true})}}
                    style={{ flex:1, flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                    
                        <Icon  name='sort' type='MaterialCommunityIcons' style={{color:'#AAAAAA'}} />
                        <Text style={{color:'#AAAAAA', marginHorizontal:moderateScale(3)}} >{Strings.sort}</Text>         
                    
                    </MenuTrigger>

                    <MenuOptions customStyles={{width:responsiveWidth(36)}} optionsContainerStyle={{width:responsiveWidth(36)}} >                       
                       
                        <MenuOption onSelect={() =>{
                               this.setState({sort:true,open:false})
                               this.getAds(1,true,true,this.state.subCategoryId)
                        }} >
                        <View  style={{flexDirection:'row',alignItems:'center' }} >
                        <Checkbox
                        onChange={(name, checked) => {
                            this.setState({sort:true,open:false})
                            this.getAds(1,true,true,this.state.subCategoryId)
                        }}           
                        checked={this.state.sort?true:false}
                        style={{marginHorizontal:moderateScale(4), borderColor:'#679C8A', backgroundColor: "white",color: '#679C8A',borderRadius: 0}}
                        />
                            <Text  style={{marginHorizontal:moderateScale(4),}} >{Strings.lowToHeigh}</Text>
                        </View>
                        </MenuOption>

                        <MenuOption onSelect={() =>{
                             this.setState({sort:false,open:false})
                             this.getAds(1,true,false,this.state.subCategoryId)
                        }} >
                        <View  style={{marginTop:moderateScale(6), flexDirection:'row',alignItems:'center' }} >
                        <Checkbox 
                        onChange={(name, checked) => {
                            this.setState({sort:false,open:false})
                             this.getAds(1,true,false,this.state.subCategoryId)
                        }}            
                        checked={this.state.sort?false:true}
                        style={{marginHorizontal:moderateScale(4), borderColor:'#679C8A', backgroundColor: "white",color: '#679C8A',borderRadius: 0}}
                        />
                            <Text style={{marginHorizontal:moderateScale(4),}} >{Strings.newest}</Text>
                        </View>
                        </MenuOption>

                    </MenuOptions>
                    </Menu>
                       {/* <Menu
                        ref={this.setMenuRef}
                        button={
                        <TouchableOpacity onPress={()=>{
                            this.showMenu()
                        }} style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}> 
                           <Icon  name='sort' type='MaterialCommunityIcons' style={{color:'#AAAAAA'}} />
                            <Text style={{color:'#AAAAAA', marginHorizontal:moderateScale(3)}} >{Strings.sort}</Text>         
                        </TouchableOpacity>
                        }
                        >
                            <MenuItem
                              onPress={()=>{
                                this.hideMenu()    
                                this.setState({sort:true})
                                this.getAds(1,true,true,this.state.subCategoryId)
                            }}>
                                
                            {Strings.lowToHeigh}
                            </MenuItem>
                            <MenuItem onPress={()=>{
                                this.hideMenu() 
                                this.setState({sort:false})
                                this.getAds(1,true,false,this.state.subCategoryId)
                            }}>{Strings.newest}</MenuItem>    
                            
                        </Menu>
                        */}
               
                        
              </View>
          )
      }



    render(){
        const {navigator,data,isRTL,currentUser} = this.props;
        const {selectedCity,cities, showBrandModal, selectedBrand,brandLoad,brands,brandText,brandImage,usage,year,color2,jopRequire,selectedJobType, beds,baths,space,rent,color, priceTo,priceFrom,showModal,selectedAddress,filterLoading} = this.state;
        return(
            <MenuProvider>
            <View
            
               style={{flex:1,backgroundColor:'white'}}>
                 <FlatAppHeader
                 onPress={()=>{
                     if(data.type == 'MOTOR'){
                        navigator.push({
                            screen: 'AddCarAds',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data}
                        })
                     }else if(data.type == 'REAL-STATE'){
                        navigator.push({
                            screen: 'AddBuildingAds',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data}
                        })
                     }else if(data.type == 'JOPS'){
                        navigator.push({
                            screen: 'AddJobAds',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data}
                        })
                     }else{
                        navigator.push({
                            screen: 'AddAds',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data}
                        })
                     }
                 }}
                  add  navigator={navigator} title={isRTL?data.arabicName:data.name} />
                 
                 {this.subCategoriesSection()}
                 {this.filterAndSortSection()}

                {/*this.state.soretDialog&&
                <View style={{backgroundColor:'red', zIndex:1000,elevation:4, position:'absolute',left:this.state.x,top:this.state.y, width:responsiveWidth(40),height:responsiveHeight(15)}} >

                </View>
                */}


                {this.state.loading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.state.ads.length>0?
                <FlatList    
                numColumns={2}        
               data={this.state.ads}
                ListFooterComponent={this.renderFooter}
                renderItem={({item})=>(
                    <HomeAdsCard onPress={()=>{
                        this.props.navigator.push({
                         screen:item.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                         animated:true,
                         animationType:'slide-horizontal',
                         passProps:{data:item}
                       })
                     }} navigator={this.props.navigator} data={item}
                      />
                )}
                onEndReached={() => {
                   
                    if(this.page <= this.state.pages){
                        this.page++;                     
                        this.getAds(this.page,false,this.state.sort,this.state.subCategoryId)                       
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.refresh}
                    onRefresh={() => {
                      this.page = 1
                      
                    this.getAds(1,true,this.state.sort,this.state.subCategoryId)
                    }
                    } />
                }
                  onEndReachedThreshold={.5}        
                />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Text style={{color:'black'}}>{Strings.noDataAtRecent} </Text>
                </View>

                }

                <Modal
                onRequestClose={()=>this.setState({showModal:false})}
                animationType="slide"
                transparent={false}
                visible={showModal}
                >

                    <ImageBackground
                     source={require('../assets/imgs/header.png')}
                     style={{elevation:1,justifyContent:'center',alignItems:'center', height:responsiveHeight(12),width:responsiveWidth(100)}} 
                     >
                    <View style={{width:responsiveWidth(90),justifyContent:'space-between', flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}} >
                        <TouchableOpacity
                        onPress={()=>{
                            this.refs._rangeSlider.setLowValue(1000);
                            this.refs._rangeSlider.setHighValue(5000);
                            this.setState({beds:3,baths:1,space:85,color:1,rent:'month', priceTo:5000,priceFrom:1000,selectedAddress:' ',jopRequire:' ',jobType:'Full Time'})
                        }}
                        >
                            <Text style={{fontSize:responsiveFontSize(8),color:'white'}} >{Strings.clearAll}</Text>
                        </TouchableOpacity>


                        <Text style={{fontSize:responsiveFontSize(8),color:'white'}} >{Strings.filter}</Text>

                        <TouchableOpacity
                            onPress={()=>{
                                this.setState({showModal:false})
                            }}
                        >
                            <Icon style={{fontSize:responsiveFontSize(9), color:'white'}} type='AntDesign' name='close' />
                        </TouchableOpacity>
                    </View>

                    </ImageBackground>
                 
                 <ScrollView>
                    <View style={{ marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7)}}> {Strings.price}</Text>
                    <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center', marginTop:moderateScale(1),width:responsiveWidth(90)}}>                 
                    <RangeSlider
                        ref="_rangeSlider"
                        style={{width: responsiveWidth(90),height:20}}
                        gravity={'top'}
                        labelStyle='none'
                        min={1}
                        max={10000}
                        step={1}
                        selectionColor="#679C8A"
                        blankColor="#ccc"
                        initialLowValue={priceFrom}
                        initialHighValue={priceTo}
                        onValueChanged={(low, high, fromUser) => {
                            console.log("low  ",low,"      hight    ",high)
                            
                            this.setState({priceFrom:low,priceTo:high})
                    }}/>
                                 

                    </View>
                    
                    </View>
                 
                    <View style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(90),alignSelf:'center'}} >
                        <Text style={{color:'black',}}>{Strings.from}</Text>
                         <View style={{marginHorizontal:moderateScale(3), flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(25),height:responsiveHeight(7.5)}}>
                            {/*<Text style={{color:'black'}}>{priceFrom}</Text>*/}
                            <TextInput 
                            underlineColorAndroid='transparent'
                            style={{height:responsiveHeight(7.5)}}
                            value={""+priceFrom}
                            keyboardType='phone-pad'
                            onChangeText={(val)=>{
                                
                                let price  = Number(val)
                                if(price<priceTo){
                                if(price>10000){
                                    this.setState({priceFrom:price})
                                    this.refs._rangeSlider.setLowValue(10000);  
                                }else{
                                    this.setState({priceFrom:price}) 
                                    this.refs._rangeSlider.setLowValue(price);         
                                }
                            }
                            }}
                            />
                            <Text style={{color:'black'}}>{Strings.aed}</Text>
                        </View>
                        <Text  style={{color:'black',marginHorizontal:moderateScale(3)}}>{Strings.to}</Text>
                         <View style={{flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(25),height:responsiveHeight(7.5)}}>
                            {/*<Text style={{color:'black'}}>{priceTo }</Text>*/}
                            <TextInput 
                             keyboardType='phone-pad'
                            underlineColorAndroid='transparent'
                            style={{height:responsiveHeight(7.5)}}
                            value={""+priceTo}
                            onChangeText={(val)=>{
                               
                                let price  = Number(val)
                                if(price>priceFrom){
                                if(price>10000){
                                    this.setState({priceTo:price})
                                    this.refs._rangeSlider.setHighValue(10000); 
                                }else{
                                    this.setState({priceTo:price}) 
                                    this.refs._rangeSlider.setHighValue(price); 
                                }
                            }
                            }}
                            />
                            <Text style={{color:'black'}}>{Strings.aed}</Text>
                        </View>

                    </View>
                    
                    {/*<View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.location}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        //value={selectedAddress}
                        onChangeText={(val)=>{this.setState({selectedAddress:val})}}
                        placeholder='UAE,ras elkhaimah,streed 5'underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {selectedAddress.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>*/}

                    {/*aniii*/}
                    <View style={{marginVertical:moderateScale(3),marginTop:moderateScale(5), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.subCategory}</Text>
                        <View style={{borderWidth:1, marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{width:responsiveWidth(85)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'black' }}
                        selectedValue={this.state.selectedSubcategory2}
                        onValueChange={(val,index)=>{
                            console.log("count VAL  ",val)
                            this.setState({selectedSubcategory2:val})
                        }}
                        >
                            {data.child.map((val,)=>{     
                                return(
                                <Picker.Item label={isRTL?val.arabicName:val.name} value={val.id} />
                                )
                            })}
                        </Picker>
                        </View>
                        </View>

                    </View>

                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.selectedCity}</Text>
                        <View style={{borderWidth:1, marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{width:responsiveWidth(85)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'black' }}
                        selectedValue={selectedCity}
                        onValueChange={(val,index)=>{
                            console.log("count VAL  ",val)
                            this.setState({selectedCity:val})
                        }}
                        >
                            {cities.map((val,)=>{     
                                return(
                                <Picker.Item label={isRTL?val.arabicName:val.cityName} value={val.id} />
                                )
                            })}
                        </Picker>
                        </View>
                        </View>

                    </View>
                    
                    {data.type=='REAL-STATE'&&
                     <View style={{width:responsiveWidth(100)}} >
                         <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.rentalFrequency}</Text>
                        <View style={{marginVertical:moderateScale(7),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',width:responsiveWidth(90)}} >
                            <TouchableOpacity
                            onPress={()=>{
                                this.setState({color:0,rent:'year'})
                            }}
                            style={{height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:color==0?'#679C8A':'white'}}>
                                <Text style={{color:color==0?'white':'gray'}}>{Strings.yearly}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                            onPress={()=>{
                                this.setState({color:1,rent:'month'})
                            }}
                            style={{height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:color==1?'#679C8A':'white'}}>
                                <Text style={{color:color==1?'white':'gray'}}>{Strings.monthly}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                            onPress={()=>{
                                this.setState({color:2,rent:'week'})
                            }}
                            style={{height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:color==2?'#679C8A':'white'}}>
                                <Text style={{color:color==2?'white':'gray'}}>{Strings.weekly}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                            onPress={()=>{
                                this.setState({color:3,rent:'day'})
                            }}
                            style={{height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:color==3?'#679C8A':'white'}}>
                                <Text style={{color:color==3?'white':'gray'}}>{Strings.daily}</Text>
                            </TouchableOpacity>
                            
                        </View>
                        </View>
                    
                        <View style={{alignSelf:'center', flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-around',width:responsiveWidth(90),marginVertical:moderateScale(7)}} >
                            <View style={{flexDirection:'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(28),height:responsiveHeight(8)}}>
                                    <TextInput
                                    keyboardType='phone-pad'
                                    underlineColorAndroid='transparent'
                                    onChangeText={(val)=>{
                                    this.setState({beds:Number(val)})
                                    }}
                                    value={""+beds}
                                    style={{height:responsiveHeight(8),width:responsiveWidth(15), color:'black'}}/>
                                    <Text style={{marginHorizontal:moderateScale(3),color:'black'}}>{Strings.bed}</Text>
                                </View>
                                <View style={{flexDirection:'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(28),height:responsiveHeight(8)}}>
                                    <TextInput
                                    keyboardType='phone-pad'
                                    underlineColorAndroid='transparent'
                                    onChangeText={(val)=>{
                                        this.setState({baths:Number(val)})
                                    }}
                                    value={""+baths}
                                    style={{height:responsiveHeight(8),width:responsiveWidth(15), color:'black'}}/>
                                    <Text style={{marginHorizontal:moderateScale(3),color:'black'}}>{Strings.baths}</Text>
                                </View>
                                <View style={{flexDirection:'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(28),height:responsiveHeight(8)}}>
                                    <TextInput
                                    keyboardType='phone-pad'
                                    underlineColorAndroid='transparent'
                                    onChangeText={(val)=>{
                                        this.setState({space:Number(val)})
                                    }}
                                    value={""+space}
                                    style={{width:responsiveWidth(15), color:'black'}}/>
                                    <Text style={{marginHorizontal:moderateScale(3), color:'black'}}>{Strings.space}</Text>
                                </View>
                        </View>
                
                    
                    </View>   
                    }

                    {data.type=='JOPS'&&
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(100)}}>
                        <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.jobType}</Text>
                        <View style={{borderWidth:1, marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{width:responsiveWidth(85)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{ color:'black' }}
                        selectedValue={selectedJobType}
                        onValueChange={(val)=>{
                            this.setState({selectedJobType:val})
                        }}
                        >
                            <Picker.Item label={Strings.fullTime} value='Full Time' />
                            <Picker.Item label={Strings.partTime} value='Part Time' />
                            <Picker.Item label={Strings.remotely} value='remotely' />
                        </Picker>
                        </View>
                        </View>

                    </View>

                        
                        <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.jopRequire}</Text>
                        <View style={{justifyContent:'flex-start', alignSelf:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                         multiline={true}                           
                        onChangeText={(val)=>{this.setState({jopRequire:val})}}
                        placeholder={Strings.jopRequire} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                        </View>
                        {/*jopRequire.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        */}
                        </View>

                    </View>
                    }

                    {data.type=='MOTOR'&&
                    <View style={{marginVertical:moderateScale(1), width:responsiveWidth(100)}}>
                        <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.color}</Text>
                        <View style={{ flexWrap:'wrap', marginTop:moderateScale(3), flexDirection:isRTL?'row-reverse':'row',alignSelf:isRTL?'flex-end':'flex-start',flexWrap:'wrap'}}   >
                        {this.state.colores.map(c=>(
                         <ImageBackground                        
                         source={{uri:c.img}}
                         style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,marginBottom:moderateScale(5),elevation:5, marginHorizontal:moderateScale(5),width:40,height:40,borderRadius:20,justifyContent:'center',alignItems:'center'}}
                         imageStyle={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, elevation:5,width:40,height:40,borderRadius:20}}
                         >

                        <TouchableOpacity
                        onPress={()=>this.setState({color2:c.colorName,colorId:c.id})}
                         style={{elevation:5,width:40,height:40,borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                            {color2==c.colorName&&
                            <Icon name='check' type='Entypo' style={{fontSize:responsiveFontSize(4),color:c.colorName.toLowerCase()=='white'?'black':'white'}} />
                            }
                        </TouchableOpacity>
                        </ImageBackground>   
                        ))}
                        
                    </View>

                    </View>
                        
                        <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.year}</Text>
                        <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                            <TextInput
                            onChangeText={(val)=>{this.setState({year:val})}}
                            placeholder={Strings.year} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                        </View>
                       
                    </View>
                  
                        <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.usage}</Text>
                        <View style={{marginVertical:moderateScale(7),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',alignSelf:isRTL?'flex-end':'flex-start'}} >
                            <TouchableOpacity
                            onPress={()=>{
                                this.setState({usage:'New'})
                            }}
                            style={{height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:usage=='New'?'#679C8A':'white'}}>
                                <Text style={{color:usage=='New'?'white':'gray'}}>{Strings.new}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                            onPress={()=>{
                                this.setState({usage:'Used'})
                            }}
                            style={{marginHorizontal:moderateScale(5), height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:usage=='Used'?'#679C8A':'white'}}>
                                <Text style={{color:usage=='Used'?'white':'gray'}}>{Strings.used}</Text>
                            </TouchableOpacity>
                            
                        </View>
                        </View>

                        <View style={{width:responsiveWidth(90), alignSelf:'center', marginTop:moderateScale(5)}}>
                <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.brand}</Text>
                    {
                        brandLoad?
                        <View style={{alignSelf:'center', marginTop:moderateScale(2), justifyContent:'center',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                            <ActivityIndicator />
                        </View>
                        :
                    <TouchableOpacity 
                     onPress={()=>this.setState({showBrandModal:true})}
                    style={{alignSelf:'center', marginTop:moderateScale(2),flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                         <FastImage resizeMode='center' source={{uri:brandImage}} style={{marginHorizontal:moderateScale(4), width:responsiveWidth(12),height:responsiveHeight(5)}} />   
                        <Text style={{ fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(2)}} >{brandText}</Text>
                        </View>
                        <View style={{marginHorizontal:moderateScale(4)}}>
                            <Icon name='caretdown' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(6)}} />
                        </View>
                    </TouchableOpacity>
                    }
                </View>

                        <Modal
                 onRequestClose={()=>this.setState({showBrandModal:false})}
                     animationType="slide"
                     transparent={false}
                     visible={showBrandModal}
                    >
                        
                        <TouchableOpacity
                         onPress={()=>this.setState({showBrandModal:false})}
                         style={{alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10),marginBottom:moderateScale(5), marginHorizontal:moderateScale(10)}}>
                            <Icon name='close' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(11)}} />
                        </TouchableOpacity>
                        
                        
                        {brands.map(data=>(
                             <TouchableOpacity 
                             onPress={()=>{
                                this.setState({selectedBrand:data.id, showBrandModal:false,brandText:data.brandname,brandImage:data.img})
                                //this.getModels(data.id)
                             }}
                            style={{height:responsiveHeight(10),flexDirection:isRTL?'row-reverse':'row',alignItems:'center', width:responsiveWidth(100),borderBottomColor:'#d7dade',borderBottomWidth:0.5}}>
                             <FastImage resizeMode='center' source={{uri:data.img}} style={{marginHorizontal:moderateScale(4), width:responsiveWidth(12),height:responsiveHeight(5)}} />   
                            <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(2)}} >{data.brandname}</Text>
                            </TouchableOpacity>
                        ))}
                       
                    </Modal>   

                    </View>
                    }

                    <TouchableOpacity
                        
                        style={{marginBottom:moderateScale(20),  width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(2)}}
                        onPress={()=>{                       
                           
                            this.setState({filterLoading:true});
                            let url=''                        
                            url=`${BASE_END_POINT}ads?priceFrom=${priceFrom}&priceTo=${priceTo}&city=${selectedCity}`                         
                            
                            if(this.state.selectedSubcategory2){
                                url=`${BASE_END_POINT}ads?priceFrom=${priceFrom}&priceTo=${priceTo}&city=${selectedCity}&subCategory=${this.state.selectedSubcategory2}`
                            }

                            if(data.type=='JOPS'){
                                url = `${url}&jopRequirements=${jopRequire.replace(/\s/g, '').length?jopRequire:''}&jopType=${selectedJobType}`
                            }
                            if(data.type=='REAL-STATE'){
                                url = `${url}&beds=${beds}&baths=${baths}&space=${space}&rental=${rent}`
                            }
                            if(data.name=='MOTOR'){
                            url = `${url}&usage=${usage}&brand=${brandText}&color=${color2}&year=${yrar.replace(/\s/g, '').length?year:''}`
                            }
                            axios.get(url)
                            .then(response=>{
                                  console.log('get Ads   ',response.data.data)
                                this.setState({showModal:false, filterLoading:false});
                               if(response.data.data.length>0){
                                navigator.push({
                                    screen:'HomeSearchResults',
                                    animated: true,
                                    animationType:'slide-horizontal',
                                    passProps:{data:response.data.data}
                                })
                            }else{
                                RNToasty.Info({title:Strings.noDataAtRecent})
                            }
                              }).catch(error=>{
                                console.log('Error  ',error)
                                console.log('Error  ',error.reponse)
                                this.setState({filterLoading:false});
                              })
                        
                        
                        
                        
                        }}
                        >
                        <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.apllyFilter}</Text>
                    </TouchableOpacity>
                    
                    </ScrollView>

                    {filterLoading && <LoadingDialogOverlay title={Strings.wait}/>}

                </Modal>
                
            </View>
            </MenuProvider>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,   
    currentCountry:state.auth.currentCountry,
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(CategoryDetails);

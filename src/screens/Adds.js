import React,{Component} from 'react';
import {View,RefreshControl,FlatList, StyleSheet,NetInfo,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Badge,Icon } from 'native-base';
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import AdsCard from '../components/AdsCard'
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import {getAds,showDeleteAdsDialog} from '../actions/AdsAction'
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';

class Adds extends Component {

   // page=1;
    state= {
        /*errorText:null,
        ads: new DataProvider(),
        refresh:false,
        loading:true,
        pages:null,*/
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };

          componentWillUnmount(){
            this.props.removeItem()
    }

    constructor(props) {
        super(props);     
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(24);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
               // this.getAdds(this.page,false);
               console.log("constructor")
               this.props.getAds(1,false,this.props.currentUser.user.id)
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    //this.getAdds(this.page,true);
                    console.log("constructor 2")
                    this.props.getAds(1,true,this.props.currentUser.user.id)
                }
            }
          );
          
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

    getAdds(page, refresh) {
            //this.setState({loading:true})
            let uri = `${BASE_END_POINT}ads?owner=${this.props.currentUser.user.id}&page=${page}&limit=20`
            axios.get(uri)
                .then(response => {
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        ads: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.ads.getAllData(), ...response.data.data]),
                    })
                    console.log("ADS   ",response.data.data)
                    
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error.response);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    deleteAds = (notID) => {
        console.log(notID)
      axios.put(`${BASE_END_POINT}ads/${notID}/private`,{},{
          headers: {
            'Content-Type': 'application/json',
            //this.props.currentUser.token
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
      }).then(Response=>{
          console.log("notification is deleted")
          console.log(Response)
          this.props.getAds(1,true,this.props.currentUser.user.id)

          //this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
      }).catch(error=>{
          console.log(error.response)
      })
  }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
         <AdsCard  data={data} navigator={this.props.navigator} />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.props.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,loading,refresh,ads,currentUser,page,pages,isRTL} = this.props;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                 <FlatAppHeader menu navigator={navigator} title={Strings.adds} />
            
                {loading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.props.ads.length>0?
                <FlatList   
                data={ads}  
                extraData={this.props}
                renderItem={({item})=> 
                <AdsCard  data={item} navigator={this.props.navigator} />
                }
                onEndReached={() => {
                    console.log("onEndReached")
                   if(page<pages){
                    
                    this.props.getAds(page+1,false,currentUser.user.id) 
                   }
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={refresh}
                    onRefresh={() => {
                        console.log("onRefresh")
                      this.props.getAds(1,true,currentUser.user.id)
                    }
                    } />}
                  onEndReachedThreshold={.5}
        
                />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Text style={{color:'black'}}>{Strings.noDataAtRecent} </Text>
                </View>

                }

                 <Dialog
               width={responsiveWidth(70)}
              
               visible={this.props.showDeleteDialog}
               onTouchOutside={() => {
               this.props.showDeleteAdsDialog(0,false)
               }}
               >    
               <View style={{width:responsiveWidth(70)}}>
                    <View style={{alignSelf:'center',marginTop:moderateScale(10)}}>
                    <Icon name='delete' type='MaterialCommunityIcons' style={{color:'red',fontSize:responsiveFontSize(8)}} />
                    </View>
                    <Text style={{textAlign:'center',width:responsiveWidth(60), fontSize:responsiveFontSize(6), alignSelf:'center',color:'black',marginTop:moderateScale(3)}} >{Strings.areYouShure2}</Text>
                    <View style={{alignSelf:'center',width:responsiveWidth(60),justifyContent:'space-between',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',marginVertical:moderateScale(10)}} >
                       <TouchableOpacity 
                       onPress={()=>{
                        this.props.showDeleteAdsDialog(0,false)
                    }}
                       >
                       <Text style={{fontSize:responsiveFontSize(7), color:'gray'}}>{Strings.cancel}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        onPress={()=>{
                            this.deleteAds(this.props.id)
                            this.props.showDeleteAdsDialog(0,false)
                        }}
                        >
                        <Text style={{fontSize:responsiveFontSize(7), color:'red'}}>{Strings.delete}</Text>
                        </TouchableOpacity>
                       
                    </View>
               </View>        
               </Dialog>
                
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser: state.auth.currentUser, 
    loading: state.ads.loading,
    refresh: state.ads.refresh,
    ads: state.ads.ads,
    page: state.ads.page,
    pages:state.ads.pages,
    showDeleteDialog: state.ads.showDeleteDialog,
    id: state.ads.id,
})

const mapDispatchToProps = {
    removeItem,
    getAds,
    showDeleteAdsDialog
}

export default connect(mapStateToProps,mapDispatchToProps)(Adds);

import React,{Component} from 'react';
import {View, Modal, NetInfo,TouchableOpacity,TextInput,Text,RefreshControl,ScrollView,FlatList,ActivityIndicator} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import {Icon,Thumbnail} from 'native-base';
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import { TypingAnimation } from 'react-native-typing-animation';
import ImagePicker from 'react-native-image-crop-picker';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import FastImage from 'react-native-fast-image'
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import ChatCard from '../components/ChatCard'
import strings from '../assets/strings';
import io from 'socket.io-client';
import {removeItem} from '../actions/MenuActions';
//import RNCloudinary from 'react-native-cloudinary';
//import RNCloudinary from 'react-native-cloudinary-x'
import {UploadImage,init} from 'react-native-cloudinary-x'
import {getUnseenMessages,openImage} from '../actions/ChatAction'
import FlatAppHeader from '../common/FlatAppHeader'

class DirectChat extends Component {

    socket=null;
    index=0;
    page=1;
    flat_list=null;
    state= {
      messageSeen:false,
        typing:false,
        errorText:null,
        messages: [],
        refresh:false,
        loading:true,
        load:false,
        pages:null,
        top:false,
        msg:null,
        img:'',
        once:false,

    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };

    
    constructor(props) {
        super(props);        
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(11);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
              init('874358384852174','Y4U-OtSiSwGW_IAuXW6WhNT-SNc', "boody-car")
              //RNCloudinary.config("boody-car",'874358384852174',  'Y4U-OtSiSwGW_IAuXW6WhNT-SNc');
                this.getUnseenCount()
                this.seenMessages()

            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }


    componentDidMount(){

        this.socket = io("https://a3lanha.herokuapp.com/chat",{ query: `id=${this.props.currentUser.user.id}`} );  
        this.socket.on('connect', () => {
            console.log("connect   " + this.socket.connected); // true
            console.log("connect   " + this.socket.id);
            console.log(this.socket);
            
          });

          this.socket.on('disconnect', () => {
            console.log("disconnect  "+this.socket.disconnected); // true
          });

          this.socket.on('newMessage', (data) => {
            console.log("newMessage   " + data); // true
            console.log(data); // true
            this.setState({
              messageSeen:false,
              messages: [...this.state.messages,data] ,
              typing:false 
          }) 
          });

          this.socket.on('seen', (data) => {
            console.log("seen   " + data); // true
            console.log(data); // true
            this.setState({messageSeen:true})
            //this.getMessages(0)
            //this.setState({friendMessages:[...this.state.friendMessages,data]})
          });

          this.socket.on('typing', (data) => {
            console.log("typing   " + data); // true
            console.log(data); // true
            this.setState({typing:true})
            //this.setState({friendMessages:[...this.state.friendMessages,data]})
          });

          this.socket.on('stopTyping', (data) => {
            console.log("StopTyping   " + data); // true
            console.log(data); // true
            //this.getMessages()
            this.setState({typing:false})
          });

          this.socket.on('announcements', (data) => {
            console.log("announcements   " );
            console.log(data) // true
          });

        console.log("socket id",  this.socket.id)
        this.enableDrawer()  
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                  //this.getMessages(this.page,false);
                  this.seenMessages()
                  init('874358384852174','Y4U-OtSiSwGW_IAuXW6WhNT-SNc', "boody-car")
                  //RNCloudinary.config("boody-car",'874358384852174',  'Y4U-OtSiSwGW_IAuXW6WhNT-SNc');
                }
            }
          );
          
         
    }
    
    componentWillUnmount(){
     // this.props.removeItem()
      //this.props.getUnseenMessages(this.props.currentUser.token)
    }

    getUnseenCount= () => {
      axios.get(`${BASE_END_POINT}messages/unseenCount`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
      .then(response=>{
        console.log("UN SEEN")
        console.log(response.data.unseen)
        /*this.setState({
          unseenCount: response.data
          })*/
        
      })
      .catch(error=>{
        console.log(error.response)
      })
    }
    enableDrawer = () => {
      this.props.navigator.setDrawerEnabled({
        side: 'left',
        enabled: this.props.isRTL?false:true,
    });
    this.props.navigator.setDrawerEnabled({
        side: 'right',
        enabled: this.props.isRTL?true:false,
    });
    }

    componentDidUpdate(){
      this.enableDrawer()
    }

    getMessages(page, refresh) {
              const {admin} = this.props;
              if(refresh){
                this.setState({refresh:true})
              }
            //friendId=${this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id}
            axios.get(`${BASE_END_POINT}messages?userId=${this.props.currentUser.user.id}&&friendId=${this.props.data.id}&&page=${page}&&limit=20`)
                .then(response => {
                    console.log("MEssages")
                    console.log(response.data)
                    console.log(response)
                    this.setState({refresh:false, pages:response.data.pageCount, loading:false,messages:[...response.data.data.reverse(),...this.state.messages]})
                }).catch(error => {
                    this.setState({loading:false})
                    console.log("message error");
                    console.log(error.response);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }


    seenMessages() {
      const {admin} = this.props;
      //friendId=${this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id}
      axios.put(`${BASE_END_POINT}messages?userId=${this.props.currentUser.user.id}&&friendId=${this.props.data.id}`)
          .then(response => {
              console.log("Update Seen")
              console.log(response.data)
              console.log(response)
              this.getMessages(this.page,false);
              
          }).catch(error => {
              this.setState({loading:false})
              console.log("seen error");
              console.log(error.response);
              if (!error.response) {
                  this.setState({errorText:Strings.noConnection})
              }
          })
  
}

    renderFooter = () => {
        return (
          this.state.load ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

      renderHeader = () => {
        return (
          this.state.load ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        
        <ChatCard
        chatRow={data}
         />
    </View> 
     );
    }

    leftComponent = () => (
      this.state.typing?
        <View style={{marginHorizontal:moderateScale(15), flexDirection:'row'}}>
       {/* <Text style={{color:'white', marginHorizontal:moderateScale(1.5)}} >write now</Text> */}
        <TypingAnimation 
              dotColor="white"
              dotMargin={3}
              dotAmplitude={3}
              dotSpeed={0.15}
              dotRadius={2.5}
              dotX={12}
              dotY={6}
          />
        </View>
        :
        <View />
    )
    


      
    render(){
        const {navigator,isRTL,admin,data} = this.props;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader navigator={navigator} title={data.username} /> 
                <TouchableOpacity
                 onPress={()=>{
                  this.page++ 
                  if(this.page <= this.state.pages){             
                    console.log(this.page)                                 
                    this.getMessages(this.page,true)                   
                  }else{
                    console.log('enough')
                  }
                 }}
                  style={{borderRadius:moderateScale(7), marginVertical:moderateScale(10), alignSelf:'center',justifyContent:'center',alignItems:'center',height:responsiveHeight(6),width:responsiveWidth(25),backgroundColor:'#e6e4e1'}} >
                      <Text style={{color:'black'}}>{Strings.loadMore}</Text>
                 </TouchableOpacity>
                <ScrollView 
                ref={ref => this.scrollView = ref}
                onContentSizeChange={(contentWidth, contentHeight)=>{        
                    this.scrollView.scrollToEnd({animated: true});
                }}
                 style={{marginBottom:moderateScale(28), width:responsiveWidth(100)}}>

                {this.state.loading?
                <View style={{height:responsiveHeight(72), flex:1,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size='large' />
               </View>                
                : 
                <View style={{flex:1}} >
                  
                <FlatList
                //extraData={this.state}
                 /*onScroll={(event)=>{
                     if(event.nativeEvent.contentOffset.y==0){
                         this.setState({top:true,load:true,messages:[
                            {msg:'heloo'},
                             ...this.state.messages,
                         ]})
                         console.log("current length")
                     }
                     console.log("current length   "+event.nativeEvent.contentOffset.y)
                 }}*/
               // ListFooterComponent={this.renderFooter}
                //ListHeaderComponent={this.renderHeader}
                //onEndReachedThreshold={0.5}
                /*onEndReached={({ distanceFromEnd }) => {
                this.setState({load:true})
                }}*/
                ref={(ref)=>this.flat_list=ref}
                onContentSizeChange={() => !this.state.top&&this.flat_list.scrollToEnd({animated: true})}
                onLayout={() => !this.state.top&&this.flat_list.scrollToEnd({animated: true})}
                data={this.state.messages}
                renderItem={({item,index})=>{
                    this.index=index;
                    console.log(item)
                    return(
                        <ChatCard  chatRow={item} />                        
                    )
                }}
                
                />

               
                </View>
                }

                
                
               
                { this.state.img?
                <View style={{marginTop:moderateScale(5), flexDirection:'row',marginHorizontal:moderateScale(6),}}>
                  <FastImage style={{marginRight:moderateScale(5), width:responsiveWidth(20),height:responsiveHeight(20)}} source={{uri:this.state.img}} />
                  <TouchableOpacity
                   onPress={()=>{this.setState({img:null})}}
                  >
                  <View>
                  <Icon name='close' type='EvilIcons' style={{color:'black', fontsize:12}} />
                  </View>
                  </TouchableOpacity>
                </View>
                :null}

                </ScrollView>

                <View style={{width:responsiveWidth(98), position:'absolute',bottom:50,left:0,right:0, flexDirection:'row',}}>          
                  {this.state.messageSeen&&
                    <Text style={{marginHorizontal:moderateScale(5)}} >{strings.seen}</Text>
                  }          
               </View>

                <View style={{position:'absolute',bottom:0,left:0,right:0, borderWidth:2,borderColor:'white', backgroundColor:'white', elevation:12, alignSelf:'center', width:responsiveWidth(100),height:responsiveHeight(8),flexDirection:'row',alignItems:'center'}}>
                   
                <TouchableOpacity 
                     onPress={()=>{
                      ImagePicker.openPicker({
                        width: 300,
                        height: 400,
                        cropping: true
                      }).then(image => {
                        console.log(image)
                        console.log(image.path)                      
                        this.setState({img:image.path})
                        
                      });
                     }}
                    style={{
                      marginHorizontal:moderateScale(5), justifyContent:'center',alignItems:'center', backgroundColor:'transparent'}}>
                        <Icon type='EvilIcons' name='camera' style={{color:'#679C8A',fontsize:15}} />
                    </TouchableOpacity>

                    <TextInput
                   onFocus={()=>{
                    console.log("FOCUS")
                    this.socket.emit("seen",{
                       //toId: this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id ,
                       toId:this.props.data.id,
                       myId:this.props.currentUser.user.id
                    } );

                    }}
                    value={this.state.msg}
                    placeholder='write message'

                    onChangeText={(val)=>{
                      if(!val || val.length==0){
  
                          this.socket.emit("stopTyping",{
                            toId:this.props.data.id
                        })
                      }
                      this.setState({msg:val,once:true})
                      if(val && val.length>0){
                        console.log('typing')
                        this.socket.emit("typing",{
                          toId:this.props.data.id
                        });
                      }
                    }}
                    underlineColorAndroid='transparent'
                    style={{width:responsiveWidth(65),marginHorizontal:moderateScale(7)}}
                     />
                    <TouchableOpacity 
                     onPress={()=>{
                       
                      if(this.state.msg || this.state.img){
                        if(this.state.img){
                          var data = new FormData(); 
                          data.append('img',{
                            uri: this.state.img,
                            type: 'multipart/form-data',
                            name: 'productImages'
                          }) 
                          
                          axios.post(`${BASE_END_POINT}messages/upload`, data, {
                            headers: {
                              'Content-Type': 'multipart/form-data',
                              'Authorization': `Bearer ${this.props.currentUser.token}`
                            },
                          })
                          .then(response=>{
                            console.log("img  ",response.data)
                            
                            const data = {
                              text:this.state.msg?this.state.msg:'',
                              image:response.data,
                              user:{
                                  _id:this.props.currentUser.user.id
                              },
                            }
                            this.socket.emit("newMessage",{
                              //toId: this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id ,
                              toId:this.props.data.id,
                              data: data
                           } );
                           this.setState({
                            messageSeen:false,
                            top:false,
                            msg:'',
                            img:null,
                            messages: [...this.state.messages,data]  ,
                            once:false,
                          })
                          
                          })
                          .catch(err => {
                            console.log("Uploaded image Error  ",err.response)
                            console.log(err)
                          })
                        }
                        else{
                          const data = {
                            text:this.state.msg,
                            image:'',
                            user:{
                                _id:this.props.currentUser.user.id
                            },
                          }
                          this.socket.emit("newMessage",{
                            //toId: this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id ,
                            toId:this.props.data.id,
                            data: data
                          });
                          this.setState({
                          messageSeen:false,
                          top:false,
                          msg:'',
                          img:null,
                          messages: [...this.state.messages,data]  ,
                          })
                        }
                      }
                     }}
                    style={{justifyContent:'center',alignItems:'center', backgroundColor:'transparent'}}>
                        <Icon type='MaterialIcons' name='send' style={{color:'#679C8A',fontsize:15}} />
                    </TouchableOpacity>
                </View>

                <Modal
                animationType="slide"
                visible={this.props.open}
                 style={{height:responsiveHeight(100),width:responsiveWidth(100)}}
                >
                    <TouchableOpacity
                    onPress={()=>{
                      this.props.openImage(false,'')
                    }}
                    style={{backgroundColor:'transparent',marginHorizontal:moderateScale(7),marginVertical:moderateScale(7)}}
                    >
                        <Icon type='AntDesign' name='closecircle'  style={{color:'black',fontSize:responsiveFontSize(10)}} />
                    </TouchableOpacity>
                    <FastImage 
                    resizeMode='contain'
                    source={{uri:this.props.image}}
                    style={{height:responsiveHeight(80),width:responsiveWidth(100),alignSelf:'center'}} />
                </Modal>
     
                  
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser:state.auth.currentUser, 
    barColor: state.lang.color ,
    open:state.chat.open,
    image:state.chat.image,
})

const mapDispatchToProps = {
  removeItem,
  getUnseenMessages,
  openImage
}

export default connect(mapStateToProps,mapDispatchToProps)(DirectChat);

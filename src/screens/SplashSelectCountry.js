import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Image, StatusBar, ImageBackground, TouchableOpacity, Text, Modal, ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import { Button, Icon, Radio } from 'native-base';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import Strings from '../assets/strings';
import { getUser, userToken, setCurrentCountry } from '../actions/AuthActions';
import { changeLanguage, changeColor } from '../actions/LanguageActions';
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
//import firebase,{Notification } from 'react-native-firebase';
import { putLocalOrder } from '../actions/OrderAction';
import Checkbox from 'react-native-custom-checkbox';
import FastImage from 'react-native-fast-image';
//import countries from '../assets/countries';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
//AIzaSyCyVT2Q8UQd38TcTwug3yCRv2DTcQRQBIc
import publicIP from 'react-native-public-ip';

class SplashSelectCountry extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#679C8A'
    };

    state = {
        countryApp:null,
        countries: [],
        countryLoad: true,
        selectedCountry: '',
        countryImage: '',
        countryText: '',
        openModal: false,
        googleCountryName: '',
        hideButtin: false,
    }

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    getCountryNameFromIP = (ip) => {
        axios.post(`${BASE_END_POINT}countryName`, JSON.stringify({
            ipAddress: ip
        }), {
            headers: {
                'Content-Type': 'application/json',

            },
        }).then(response => {
            console.log("country ip   ", response.data.countryName)
            console.log("country ip   ", response)
            //this.setState({googleCountryName:})
            this.getCountries(response.data.countryName)
        }).catch(error => {
            console.log("country error ip   ", error)
            console.log("country error ip   ", error.response)
        })
    }


    componentDidMount() {
        //this.permissions()
        this.disableDrawer();


        /*publicIP()
            .then(ip => {
                console.log("ip  ",ip);
                // '47.122.71.234'
                this.getCountryNameFromIP(ip)
            })
            .catch(error => {
                console.log("ip error  ", error);

                // 'Unable to get IP address.'
        })*/

        this.getCountries()
    }

    getCountries = (ipCountry) => {
        const {isRTL} = this.props
        axios.get(`${BASE_END_POINT}countries`)
            .then(response => {
                console.log('countries   ', response.data)

                this.setState({ countryLoad: false, countries: response.data.data, selectedCountry: response.data.data[0].id, countryImage: response.data.data[0].img, countryText:isRTL?response.data.data[0].arabicName:response.data.data[0].countryName })
                AsyncStorage.setItem('@COUNTRY',JSON.stringify(response.data.data[0]))
                this.props.setCurrentCountry(response.data.data[0])
                

                /*if (response.data.data.length > 0) {
                    this.setState({ countryLoad: false, countries: response.data.data, selectedCountry: response.data.data[0].id, countryImage: response.data.data[0].img, countryText:isRTL?response.data.data[0].arabicName:response.data.data[0].countryName })
                    AsyncStorage.setItem('@COUNTRY',JSON.stringify(response.data.data[0]))
                    this.props.setCurrentCountry(response.data.data[0])

                    response.data.data.map((val, index) => {
                        if (ipCountry.toLowerCase() == val.countryName.toLowerCase()) {
                            console.log("match country")
                            this.props.setCurrentCountry(val)
                            this.setState({hideButtin: true, selectedCountry: val.id, countryImage: val.img, countryText:isRTL?val.arabicName:val.countryName})
                            return
                        } else {
                            console.log("not match country")
                            AsyncStorage.setItem('@COUNTRY',JSON.stringify(response.data.data[0]))
                            this.props.setCurrentCountry(response.data.data[0])
                        }
                    })
                }*/

            }).catch(error => {
                console.log("countries error   ", error);
                console.log("error   ", error.response);
            })
    }




    render() {
        const { isRTL, navigator } = this.props
        const { showModal, selectedCountry, countries, countryImage, countryText, countryLoad, } = this.state;
        return (
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }} source={require('../assets/imgs/splashScreen2.png')} >


                <View style={{ marginTop: responsiveHeight(45), width: responsiveWidth(80), alignSelf: 'center' }}>
                    <Text style={{ fontFamily: 'Roboto-Regular', alignSelf: isRTL ? 'flex-end' : 'flex-start', color: 'white', fontSize: responsiveFontSize(7) }}>{Strings.chooseYourCountry}</Text>
                </View>

                <TouchableOpacity
                    onPress={() => { this.setState({ openModal: true }) }}
                    style={{ borderRadius: moderateScale(2), backgroundColor: '#D4E2DD', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', marginTop: moderateScale(3), marginVertical: moderateScale(7), width: responsiveWidth(80) }}>
                    {countryLoad ?
                        <View style={{ borderRadius: moderateScale(2), backgroundColor: '#D4E2DD', justifyContent: 'center', alignItems: 'center', alignSelf: 'center', marginTop: moderateScale(3), marginVertical: moderateScale(7), width: responsiveWidth(80) }} >
                            <ActivityIndicator />
                        </View>
                        :
                        <View style={{ borderRadius: moderateScale(2), backgroundColor: '#D4E2DD', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', width: responsiveWidth(80) }}>
                            <View style={{ alignItems: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', height: responsiveHeight(8) }}>
                                <View style={{ borderRightWidth: !isRTL ? 0.3 : 0, borderLeftWidth: isRTL ? 0.3 : 0, justifyContent: 'center', alignItems: 'center', height: responsiveHeight(8) }} >
                                    <FastImage resizeMode='contain' source={{ uri: countryImage }} style={{ marginHorizontal: moderateScale(5), width: responsiveWidth(10), height: responsiveHeight(5) }} />
                                </View>
                                <Text style={{color:'black', marginHorizontal: moderateScale(5) }} >{countryText}</Text>
                            </View>
                            <Icon name='chevron-small-down' type='Entypo' style={{ marginHorizontal: moderateScale(5), color: 'black', fontSize: responsiveFontSize(7) }} />
                        </View>
                    }

                </TouchableOpacity>

                <Modal
                    onRequestClose={() => this.setState({ openModal: false })}
                    animationType="slide"
                    transparent={false}
                    visible={this.state.openModal}
                >
                    <TouchableOpacity
                        onPress={() => { this.setState({ openModal: false }) }}
                        style={{ alignSelf: 'flex-end', margin: moderateScale(10) }}>
                        <Icon name='closecircle' type='AntDesign' />
                    </TouchableOpacity>
                    {countries.map((val, index) =>
                        <TouchableOpacity
                            onPress={() => {
                                AsyncStorage.setItem('@COUNTRY',JSON.stringify(val))
                                this.props.setCurrentCountry(val)
                                this.setState({selectedCountry: val.id, countryText:isRTL?val.arabicName:val.countryName, countryImage: val.img, openModal: false })
                            }}
                            style={{ marginBottom: moderateScale(5), borderBottomWidth: 0.4, borderBottomColor: 'gray', flexDirection: 'row', width: responsiveWidth(100), alignItems: 'center', marginTop: moderateScale(5) }}>
                            <FastImage resizeMode='contain' source={{ uri: val.img }} style={{ marginHorizontal: moderateScale(5), width: responsiveWidth(10), height: responsiveHeight(5) }} />
                            <Text style={{color:'black',}}  >{isRTL?val.arabicName:val.countryName}</Text>
                        </TouchableOpacity>
                    )}
                </Modal>

                <TouchableOpacity
                    disabled={countries.length > 0 ? false : true}
                    style={{ width: responsiveWidth(40), alignSelf: 'center', height: responsiveHeight(8), alignItems: 'center', justifyContent: 'center', backgroundColor: '#C7BE75', marginTop: moderateScale(7), borderRadius: moderateScale(3) }}
                    onPress={() => {

                        navigator.resetTo({
                            screen: 'Home',
                            animated: true,
                            animationType:'slide-horizontal'
                        })

                    }}
                >

                    <Text style={{ color: 'white', fontSize: responsiveFontSize(8) }}>{Strings.next}</Text>

                </TouchableOpacity>



            </ImageBackground>
        );
    }
}

const mapToStateProps = state => ({
    currentUser: state.auth.currentUser,
    isRTL: state.lang.RTL,
    currentCountry: state.auth.currentCountry,
})


const mapDispatchToProps = {
    setCurrentCountry,
}



export default connect(mapToStateProps, mapDispatchToProps)(SplashSelectCountry);


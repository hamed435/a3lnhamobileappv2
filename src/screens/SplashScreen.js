import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,Image,StatusBar,ImageBackground,Text
} from 'react-native';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import Strings from '../assets/strings';
import {getUser,userToken,setCurrentCountry} from '../actions/AuthActions';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
import firebase,{Notification } from 'react-native-firebase';
import {putLocalOrder} from '../actions/OrderAction';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {currentFavList} from '../actions/FavouriteAction';
import FastImage from 'react-native-fast-image'
import {IosRootNavigator} from '../actions/Navigator'
import * as Animatable from 'react-native-animatable';

//new splash 
export let homeNavigator = null


class SplashScreen extends Component {

 
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#679C8A',
    };
    
    constructor(props){
        super(props);
        this.props.IosRootNavigator(this.props.navigator)
        homeNavigator=this.props.navigator;
    }
    
    
    

    checkToken = async (token) => {
        this.props.userToken(token);
        t = await AsyncStorage.getItem('@BoodCarToken')    
        if(t){
            console.log('Exist Token')
            if(t==token){
                console.log('Token is equal')
            }else{
                console.log('New Token')
                console.log(t)
                console.log(token)
                axios.put(`${BASE_END_POINT}updateToken`, JSON.stringify({
                    oldToken: t,
                    newToken: token,
                  }), {
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${this.props.currentUser.token}`
                  },
                }).then(rsponse=>{
                    AsyncStorage.setItem('@BoodCarToken',token)
                }).catch(error=>{
                    console.log("token Error")
                    console.log(error)
                    console.log(error.response)
                })     
            }
        }else{
            console.log("save token")
            AsyncStorage.setItem('@BoodCarToken',token)
        } 

        
    }

    ServerNotification(title,body) {
        console.log("ANI CAPO   ",title,"    ",body)
        let notification = new firebase.notifications.Notification();
        notification = notification.setNotificationId(new Date().valueOf().toString())
        .setTitle( title)
        .setBody(body)
        .setSound("bell.mp3")
        .setData({
          now: new Date().toISOString()
        });
       notification.ios.badge = 10
       //notification.android.setAutoCancel(true);  
        notification.android.setBigPicture("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png", "https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg", "content title", "summary text")
        notification.android.setColor("red")
        notification.android.setColorized(true)
        notification.android.setOngoing(true)
        notification.android.setPriority(firebase.notifications.Android.Priority.High)
        notification.android.setSmallIcon("ic_launcher")
        notification.android.setVibrate([300])
        notification.android.addAction(new firebase.notifications.Android.Action("view", "ic_launcher", "VIEW"))
       // notification.android.addAction(new firebase.notifications.Android.Action("reply", "ic_launcher", "REPLY").addRemoteInput(new firebase.notifications.Android.RemoteInput("input")) )
       notification.android.setChannelId("test-channel")

        firebase.notifications().displayNotification(notification)
    
      }

      
    
    componentDidMount(){  
        this.disableDrawer();
        setTimeout(()=>{
        this.checkLogin();       
        },3000)
        console.log('cdm')
        this.checkLanguage();
        this.checkCurrentCountry()      
        //this.checkColor()

        firebase.messaging().hasPermission()
        .then(enabled => {
            if (enabled) {
            console.log(" i have permession")
            } else {
                console.log(" i not have permession")
            firebase.messaging().requestPermission()
            } 
        });   
        
        firebase.messaging().getToken().then(token => {
            console.log("TOKEN (getFCMToken)", token);
            this.checkToken(token);
          }).catch(error=>{
            console.log("TOKEN Error");         
            console.log(error);
          });



        firebase.notifications().onNotification((notification) => {
            console.log("mohamed anwer bas")
            console.log(notification)
            console.log(notification._title,notification._body)
          
            this.ServerNotification(notification._title,notification._body)
        });

      
        firebase.notifications().onNotificationOpened((notificationOpen) => {
            console.log("noti open")
            console.log(notificationOpen)
            console.log("noti body   ",notificationOpen.notification)
            if(notificationOpen.action == 'android.intent.action.VIEW'){
                  this.props.navigator.push({
                    screen:'Home',
                    animated:true,
                    animationType:'slide-horizontal'
                })
            }
        });   
     
    }


    
    
    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    checkLogin = async () => {      
        const userJSON = await AsyncStorage.getItem('@QsathaUser');
        if(userJSON){
            const userInfo = JSON.parse(userJSON);
            console.log('USER  ',userInfo)
             
            if(userInfo.user.type=='CLIENT'){
                
                this.props.getUser(userInfo);
                console.log('enter  ') 
                console.log(this.props.currentUser)       
                this.props.navigator.resetTo({
                     screen: 'Home', 
                    //screen: 'AddBuildingAds',                         
                     animated: true,
                     animationType:'slide-horizontal'
                 });         
            }           
        }else{
            console.log('out  ')
            this.props.getUser(null);
            this.props.navigator.resetTo({
                screen:'SplashSelectLanguage',
                //screen:'ForgetPassword',
                animated: true,
                animationType:'slide-horizontal'
                //passProps: {hideMenu:true}
            })
        }
        
    }

    checkLanguage = async () => {
        console.log("lang0   "+lang)
       const lang = await AsyncStorage.getItem('@lang');
       console.log("lang   "+lang)
       if(lang){
        if(lang==='ar'){
            this.props.changeLanguage(true);
            Strings.setLanguage('ar')
        }else{
            this.props.changeLanguage(false);
            Strings.setLanguage('en')
        } 
       }else{
        this.props.changeLanguage(false);
        Strings.setLanguage('en')
    } 
           
    }

    checkCurrentCountry = async () => {
        
       const c = await AsyncStorage.getItem('@COUNTRY');
       console.log("ccccccc   "+c)
       if(c){
        const data = JSON.parse(c);
        this.props.setCurrentCountry(data)
      } 
           
    }

    checkColor = async () => {
       const color = await AsyncStorage.getItem('@color');
       console.log("color  ",color)
       if(color){
        this.props.changeColor(color);
       }
       
    }

   
    render(){
        return(
            <View style={{backgroundColor:'#679C8A',alignItems:'center',justifyContent:'center', flex:1 }} >
                <Animatable.View duration={1500} animation='slideInUp' >
                    <Image  style={{width:responsiveWidth(60),height:responsiveHeight(27)}} resizeMode='contain' source={require('../assets/imgs/spl1.png')} />
                </Animatable.View>
            </View>
        );
    }
}

const mapToStateProps = state => ({
    currentUser : state.auth.currentUser,
    userTokens: state.auth.userToken,
    orders: state.order.orders,
    ordersData: state.order.ordersData,
})

const mapDispatchToProps = {
    getUser,
    changeLanguage,
    changeColor,
    userToken,
    putLocalOrder,
    currentFavList,
    IosRootNavigator,
    setCurrentCountry
}

export default connect(mapToStateProps,mapDispatchToProps)(SplashScreen);


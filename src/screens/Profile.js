import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,Keyboard,ScrollView,TouchableOpacity,Text,FlatList
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import LottieView from 'lottie-react-native';
import HomeAdsCard from '../components/HomeAdsCard'



class Profile extends Component {

    state = {
        myAds:[],
        adsLoading:true,
        myFollowers:[],
        followersLoading:true,

    }

    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };

    getFollowers = () => {
        let uri = `${BASE_END_POINT}follow/${this.props.currentUser.user.id}/followers?limit=5`
        axios.get(uri)
        .then(response => {
            this.setState({followersLoading:false,myFollowers:response.data.data})
            console.log('followers   ',response.data)
        }).catch(error => {
            this.setState({followersLoading:false,refresh:false})
            console.log("error   ",error);
            console.log("error   ",error.response);
        })

    }

    getAds = () => {
        let uri = `${BASE_END_POINT}ads?owner=${this.props.currentUser.user.id}&limit=8`
        axios.get(uri)
        .then(response => {
            console.log('ADS   ',response.data)
            this.setState({adsLoading:false,myAds:response.data.data})
            
        }).catch(error => {
            this.setState({adsLoading:false})
            console.log("error   ",error);
            console.log("error   ",error.response);
        })

    }


    
    
    componentDidMount(){    
        this.disableDrawer();
        this.getFollowers()
        this.getAds();
    }

    componentWillUnmount(){
        this.props.removeItem()
      }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.disableDrawer()
    }

   
    
    
    renderFollowersSection = () =>{
        const {currentUser,isRTL,navigator} = this.props;
        return(


            <View style={{alignSelf:'center',marginTop:moderateScale(5),width:responsiveWidth(95)}}>
                <View style={{alignSelf:'center',  marginHorizontal:moderateScale(7),marginVertical:moderateScale(3), flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center',width:responsiveWidth(95)}}>
                    <Text style={{fontFamily:'Roboto-Regular',color:'black'}} >{Strings.myFollowers}</Text>
                    <TouchableOpacity
                    onPress={()=>{
                        navigator.push({
                            screen: 'MyFollowers',
                            animated: true,
                            animationType:'slide-horizontal'
                        })
                    }}
                    >
                        <Text style={{fontFamily:'Roboto-Regular',color:'#D3D3D3',fontSize:responsiveFontSize(6)}} >{Strings.seeAll}</Text>
                    </TouchableOpacity>
                </View>
                {this.state.myFollowers.length>0?
                <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal
                data={this.state.myFollowers}
                renderItem={({item})=>(
                    <TouchableOpacity
                    onPress={()=>{
                        navigator.push({
                            screen: 'FriendProfile',
                            animated: true,
                            animationType:'slide-horizontal',
                            passProps:{data:item.follower}
                        })
                    }}
                    activeOpacity={1}
                     style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1,marginTop:moderateScale(3), backgroundColor:'white', justifyContent:'center',alignItems:'center', marginHorizontal:moderateScale(2),width:responsiveWidth(25),height:responsiveHeight(23),borderRadius:moderateScale(2),elevation:2,marginBottom:moderateScale(20)}}>
                        <Thumbnail source={item.follower.img?{uri:item.follower.img}:require('../assets/imgs/profileicon.jpg')} />
                        <Text style={{fontFamily:'Roboto-Regular',marginTop:moderateScale(3), fontSize:responsiveFontSize(6), color:'black',textAlign:'center',alignSelf:'center'}}>{item.follower.username}</Text>
                    </TouchableOpacity>
                )}
                />
                :
                <View style={{width:responsiveWidth(90),height:responsiveHeight(20),justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontFamily:'Roboto-Regular',}}>{Strings.noDataAtRecent}</Text>
                </View>
                }

            </View>
        )
    }

    renderAdsSection = () =>{
        const {currentUser,isRTL,navigator} = this.props;
        return(
            <View style={{alignSelf:'center',marginTop:moderateScale(5),width:responsiveWidth(95)}}>
                <View style={{alignSelf:'center', marginHorizontal:moderateScale(7),marginVertical:moderateScale(3), flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center',width:responsiveWidth(95)}}>
                    <Text style={{fontFamily:'Roboto-Regular',color:'black'}} >{Strings.myAds}</Text>
                    <TouchableOpacity
                    onPress={()=>{
                        navigator.push({
                            screen:'Adds',
                            animated:true,
                            animationType:'slide-horizontal'
                        })
                    }}
                    >
                        <Text style={{fontFamily:'Roboto-Regular',color:'#D3D3D3',fontSize:responsiveFontSize(6)}} >{Strings.seeAll}</Text>
                    </TouchableOpacity>
                </View>
                {this.state.myAds.length>0?
                <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal
                data={this.state.myAds}
                renderItem={({item})=>(
                    <HomeAdsCard onPress={()=>{
                        this.props.navigator.push({
                         screen: item.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                         animated:true,
                         passProps:{data:item},
                         animationType:'slide-horizontal'
                       })
                   }} navigator={this.props.navigator} data={item} />
                )}
                />
                :
                <View style={{width:responsiveWidth(90),height:responsiveHeight(20),justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontFamily:'Roboto-Regular',}}>{Strings.noDataAtRecent}</Text>
                </View>
                }
            </View>
        )
    }

    renderProfileInfoSections = () => {
        const {currentUser,isRTL,navigator} = this.props;
        return(
            <View style={{marginBottom:moderateScale(10), backgroundColor:'white', alignSelf:'center', elevation:0, flexDirection:'row',alignItems:'center', height:responsiveHeight(15), width:responsiveWidth(100),marginTop:moderateScale(5)}}>
                 <TouchableOpacity 
                    onPress={()=>{
                    navigator.push({
                        screen: 'PersonalInfo',
                        animated:true,
                        animationType:'slide-horizontal'
                    })
                }}
                style={{borderLeftWidth:0.3,borderLeftColor:'black', flex:1,justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontFamily:'Roboto-Regular',alignSelf:'center', textAlign:'center',marginBottom:moderateScale(2)}}>{Strings.profileInfo}</Text>
                    <Icon name='info' type='Entypo' style={{color:'gray', fontSize:responsiveFontSize(8)}} />
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={()=>{
                    navigator.push({
                        screen: 'ChangeProfilePhoto',
                        animated:true,
                        animationType:'slide-horizontal'
                    })
                }}
                style={{borderLeftWidth:0.3,borderLeftColor:'black',flex:1,justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontFamily:'Roboto-Regular',alignSelf:'center', textAlign:'center', marginBottom:moderateScale(2)}} >{Strings.profileImage}</Text>
                    <Icon name='images' type='Entypo'  style={{color:'gray', fontSize:responsiveFontSize(8)}} />
                </TouchableOpacity>
                {this.props.currentUser.user.signUpFrom=='normal'&&
                <TouchableOpacity 
                onPress={()=>{
                    navigator.push({
                        screen: 'ChangePassword',
                        animated:true,
                        animationType:'slide-horizontal'
                    })
                }}
                style={{borderLeftWidth:0.3,borderLeftColor:'black',flex:1,justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontFamily:'Roboto-Regular',alignSelf:'center', textAlign:'center', marginBottom:moderateScale(2)}} >{Strings.changePassword}</Text>
                    <Icon name='textbox-password' type='MaterialCommunityIcons'  style={{color:'gray', fontSize:responsiveFontSize(8)}} />
                </TouchableOpacity>
                }
            </View>

        )

    }
  
  

    render(){
        const {currentUser,isRTL,navigator} = this.props;
       
        return(
            <View style={{ flex:1,backgroundColor:'white' }}>
                <FlatAppHeader menu navigator={navigator} title={Strings.profile} />
                <ScrollView >
                <View style={{alignSelf:'center',marginTop:moderateScale(8),justifyContent:"flex-end",alignItems:'flex-end'}}>
                    <FastImage style={{height:150,width:150,borderRadius:75}} source={currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.jpg')} />
                    <TouchableOpacity
                      onPress={()=>{
                        navigator.push({
                            screen: 'ChangeProfilePhoto',
                            animated:true,
                            animationType:'slide-horizontal'
                        })
                    }}
                     style={{marginTop:moderateScale(-22), backgroundColor:'#679C8A', height:50,width:50,borderRadius:25,justifyContent:'center',alignItems:'center'}} >
                        <Icon name='camera-alt' type='MaterialIcons' style={{color:'white',fontSize:responsiveFontSize(10)}} />
                    </TouchableOpacity>
                </View>
                {this.renderProfileInfoSections()}

                
                {this.state.adsLoading? 
                <LottieView
                style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                source={require('../assets/animations/smartGarbageLoading.json')}
                autoPlay
                loop
                />
                :
                this.renderAdsSection()
                }

                {this.state.followersLoading? 
                <LottieView
                style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                source={require('../assets/animations/smartGarbageLoading.json')}
                autoPlay
                loop
                />
                :
                this.renderFollowersSection()
                }
                </ScrollView>
                
            {this.state.updateLoading && <LoadingDialogOverlay title={Strings.waitUpdateProfile}/>}
               
            </View>
            
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(Profile);


import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,Image,StatusBar,ImageBackground,TouchableOpacity,Text
} from 'react-native';
import { connect } from 'react-redux';
import {Button,Icon,Radio} from 'native-base';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import Strings from '../assets/strings';
import {getUser,userToken} from '../actions/AuthActions';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
//import firebase,{Notification } from 'react-native-firebase';
import {putLocalOrder} from '../actions/OrderAction';
import Checkbox from 'react-native-custom-checkbox';


class SplashSelectLanguage extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#679C8A',
       
    };

    state={
        showModal: false,
        lang: this.props.isRTL,
        showBtn:true,
    }
    
    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    componentDidMount(){
        this.disableDrawer();
    }

   
   
    render(){
        const {isRTL,navigator} = this.props
        const {showModal,lang,showBtn} = this.state;
        return(
            <ImageBackground   style={{flex:1, backgroundColor:'white'}} source={require('../assets/imgs/splashScreen3.png')} >
            
                <View style={{ marginTop:responsiveHeight(45), width:responsiveWidth(80),alignSelf:'center'}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:'black',fontSize:responsiveFontSize(7)}}>{Strings.selectLanguage}</Text>
                </View>

                <TouchableOpacity
                activeOpacity={1}
                 onPress={()=>{this.setState({showBtn:!showBtn, showModal:!showModal})}}
                 style={{backgroundColor:'white', marginTop:moderateScale(3), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(80),alignSelf:'center',borderRadius:moderateScale(2),elevation:1,shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(4)}} >{lang?'اللغة العربية':'English'}</Text>
                    <View style={{marginHorizontal:moderateScale(4)}}>
                        <Icon name={ showModal?'chevron-small-up':'chevron-small-down'} type='Entypo'  style={{color:'black',fontSize:responsiveFontSize(7)}} />
                    </View>
                </TouchableOpacity>

                {showModal&&
                <View style={{height:responsiveHeight(22),backgroundColor:'white', alignItems:'center', width:responsiveWidth(80),alignSelf:'center',borderRadius:moderateScale(2),elevation:1,shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1}}>
                <TouchableOpacity
                 onPress={()=>{
                    this.props.changeLanguage(false);
                    Strings.setLanguage('en');
                    AsyncStorage.setItem('@lang','en')
                    console.log('lang   '+ this.props.isRTL) 
                      this.setState({showBtn:true, lang:false,showModal:false})
                     }}
                 style={{borderBottomWidth:0.3,borderBottomColor:'#DBDBDB', backgroundColor:'white', marginTop:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(80)}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(4)}} >English</Text>
                    {!lang&&
                    <View style={{marginHorizontal:moderateScale(4)}}>
                    <Checkbox            
                        checked={lang?false:true}
                        style={{marginHorizontal:moderateScale(4), borderColor:'#679C8A', backgroundColor: "#679C8A",color: 'white',borderRadius: 0}}
                        /*onChange={(name, checked) => {
                        this.setState({ agreeTerms: checked });
                        }}*/
                    />
                    </View>
                    }
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={()=>{
                    this.props.changeLanguage(true);
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang','ar')
                        console.log('lang   '+ this.props.isRTL) 
                     this.setState({showBtn:true, lang:true,showModal:false})
                     }}
                style={{backgroundColor:'white', marginVertical:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(80)}}>
                    <Text style={{color:"black",fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(4)}} >اللغة العربية</Text>
                    {lang&&
                     <View style={{marginHorizontal:moderateScale(4)}}>
                     <Checkbox               
                         checked={lang?true:false}
                         style={{marginHorizontal:moderateScale(4), borderColor:'#679C8A', backgroundColor: "#679C8A",color: 'white',borderRadius: 0}}
                         /*onChange={(name, checked) => {
                         this.setState({ agreeTerms: checked });
                         }}*/
                     />
                     </View>
                    }
                </TouchableOpacity>
                </View>
                }
            
                {showBtn&&
                <TouchableOpacity
                    style={{width:responsiveWidth(37),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(15),borderRadius:moderateScale(3)}}
                    onPress={()=>{
                    
                    navigator.resetTo({
                        screen: 'SplashSelectCountry',
                        animated: true,
                        animationType:'slide-horizontal'
                    })
                    }}
                    >
                    <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.next}</Text>
                </TouchableOpacity>
                }
           </ImageBackground>
        );
    }
}

const mapToStateProps = state => ({
    currentUser : state.auth.currentUser,
    isRTL: state.lang.RTL,  
})

const mapDispatchToProps = {
    changeLanguage,
}


export default connect(mapToStateProps,mapDispatchToProps)(SplashSelectLanguage);


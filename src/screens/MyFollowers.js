import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,NetInfo,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon} from 'native-base'
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import MyFollowersCard from '../components/MyFollowersCard'

class MyFollowers extends Component {

    page=1;
    state= {
        errorText:null,
        followers: new DataProvider(),
        refresh:false,
        loading:true,
        pages:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };
   
     componentWillUnmount(){
        this.props.removeItem()
    }

    constructor(props) {
        super(props);  
        this.renderLayoutProvider = new LayoutProvider(
          () => 3,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(13);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getFollwers(this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getFollwers(this.page,true);
                }
            }
          );      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

    getFollwers(page, refresh) {
            //this.setState({loading:true})
            let uri = `${BASE_END_POINT}follow/${this.props.currentUser.user.id}/followers?page=${page}&limit=20`
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        followers: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.followers.getAllData(), ...response.data.data]),
                    })
                    console.log('followers   ',response.data)
                    
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log("error   ",error);
                    console.log("error   ",error.response);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <MyFollowersCard 
       data={data}
       navigator={this.props.navigator}
        onPress={()=>{
            this.props.navigator.push({
                screen:'AddsDetails',
                animated:true,
                animationType:'slide-horizontal',
                passProps: {
                    data:data
                }
            })
        }}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,isRTL} = this.props;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader menu navigator={navigator} title={Strings.myFollowers} />
               
                {this.state.loading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.state.followers._data.length>0 ?
                <RecyclerListView            
                layoutProvider={this.renderLayoutProvider}
                dataProvider={this.state.followers}
                rowRenderer={this.renderRow}
                renderFooter={this.renderFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.state.pages){
                        this.page++;
                        this.getFollwers(this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.refresh}
                    onRefresh={() => {
                      this.page = 1
                      this.getFollwers(1, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}
        
                />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Text style={{color:'black'}}>{Strings.noDataAtRecent} </Text>
                </View>

                }
                
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,  
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(MyFollowers);

import { Navigation } from 'react-native-navigation';
import { Provider } from "react-redux";
import store from "../store";

import SplashScreen from '../screens/SplashScreen';



import ChangeProfilePhoto from './ChangeProfilePhoto'
import PersonalInfo from './PersonalInfo'
import MyFollowers from './MyFollowers';
import Chat from './Chat';



import Adds from './Adds';
import MenuContent from "../components/MenuContent";


import Login from '../screens/Login';
import Signup from '../screens/Signup';
import Home from '../screens/Home';
import SelectLanguage from './SelectLanguage';

import ChangePassword from './ChangePassword';
import Profile from './Profile';
import ForgetPassword from './ForgetPassword';
import ChatPeople from './ChatPeople'
import DirectChat from './DirectChat'
import SplashSelectLanguage from './SplashSelectLanguage'
import SplashSelectCountry from './SplashSelectCountry'
import FriendProfile from './FriendProfile'
import Settings from './Settings'
import MyFavoriutes from './MyFavoriutes'
import AddAds from './AddAds'
import AdsDescription from './AdsDescription'
import UpdateAds from './UpdateAds'
import CategoryDetails from './CategoryDetails'
import AdsComments from './AdsComments';
import JobAdsDescription from './JobAdsDescription'
import AddJobAds from './AddJobAds'
import AddBuildingAds from './AddBuildingAds'
import AdsTypes from './AdsTypes';
import UpdateJobAds from  './UpdateJobAds'
import UpdateBuildingAds from './UpdateBuildingAds'
import HomeSearchResults from './HomeSearchResults'
import Usages from './Usages'
import AddCarAds from './AddCarAds'
import UpdateCarAds from './UpdateCarAds'
import Complaints from './Complaints'
import Suggesting from './Suggesting'
import SelectCountry from './SelectCountry'

export function registerScreens() {
     Navigation.registerComponent("SelectCountry", () => SelectCountry, store, Provider);
     Navigation.registerComponent("Suggesting", () => Suggesting, store, Provider);
     Navigation.registerComponent("Complaints", () => Complaints, store, Provider);
     Navigation.registerComponent("UpdateCarAds", () => UpdateCarAds, store, Provider);
     Navigation.registerComponent("AddCarAds", () => AddCarAds, store, Provider);
     Navigation.registerComponent("SplashScreen", () => SplashScreen, store, Provider);
     Navigation.registerComponent("Usages", () => Usages, store, Provider);
     Navigation.registerComponent("HomeSearchResults", () => HomeSearchResults, store, Provider);
     Navigation.registerComponent("UpdateBuildingAds", () => UpdateBuildingAds, store, Provider);
     Navigation.registerComponent("UpdateJobAds", () => UpdateJobAds, store, Provider);
     Navigation.registerComponent("AdsTypes", () => AdsTypes, store, Provider);
     Navigation.registerComponent("AddBuildingAds", () => AddBuildingAds, store, Provider);
     Navigation.registerComponent("AddJobAds", () => AddJobAds, store, Provider);
     Navigation.registerComponent("JobAdsDescription", () => JobAdsDescription, store, Provider);
     Navigation.registerComponent("AdsComments", () => AdsComments, store, Provider);
     Navigation.registerComponent("CategoryDetails", () => CategoryDetails, store, Provider);
     Navigation.registerComponent("UpdateAds", () => UpdateAds, store, Provider);
     Navigation.registerComponent("AdsDescription", () => AdsDescription, store, Provider);
     Navigation.registerComponent("AddAds", () => AddAds, store, Provider);
     Navigation.registerComponent("MyFavoriutes", () => MyFavoriutes, store, Provider);
     Navigation.registerComponent("Settings", () => Settings, store, Provider);
     Navigation.registerComponent("FriendProfile", () => FriendProfile, store, Provider);
     Navigation.registerComponent("SplashSelectCountry", () => SplashSelectCountry, store, Provider);
     Navigation.registerComponent("SplashSelectLanguage", () => SplashSelectLanguage, store, Provider);
     Navigation.registerComponent("ChangeProfilePhoto", () => ChangeProfilePhoto, store, Provider);
     Navigation.registerComponent("PersonalInfo", () => PersonalInfo, store, Provider);
     Navigation.registerComponent("MyFollowers", () => MyFollowers, store, Provider);
     
     Navigation.registerComponent("DirectChat", () => DirectChat, store, Provider);
     Navigation.registerComponent("ChatPeople", () => ChatPeople, store, Provider);
     Navigation.registerComponent("Chat", () => Chat, store, Provider);   

     Navigation.registerComponent("Adds", () => Adds, store, Provider);
     
     Navigation.registerComponent("Login", () => Login, store, Provider);
     Navigation.registerComponent("MenuContent", () => MenuContent, store, Provider);
     Navigation.registerComponent("Signup", () => Signup, store, Provider);
     Navigation.registerComponent("Home", () => Home, store, Provider)
     Navigation.registerComponent("SelectLanguage", () => SelectLanguage, store, Provider);
         
     Navigation.registerComponent("ChangePassword", () => ChangePassword, store, Provider);
     Navigation.registerComponent("Profile", () => Profile, store, Provider);
     Navigation.registerComponent("ForgetPassword", () => ForgetPassword, store, Provider);

    

    }


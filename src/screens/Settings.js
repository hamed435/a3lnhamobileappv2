import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,Image,StatusBar,Switch,TouchableOpacity,Text,Modal
} from 'react-native';
import { connect } from 'react-redux';
import {Button,Icon,Radio} from 'native-base';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import Strings from '../assets/strings';
import {getUser,userToken} from '../actions/AuthActions';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
//import firebase,{Notification } from 'react-native-firebase';
import {putLocalOrder} from '../actions/OrderAction';
import Checkbox from 'react-native-custom-checkbox';
import FastImage from 'react-native-fast-image';
import countries from '../assets/countries';
import FlatAppHeader from '../common/FlatAppHeader'; 
import {selectMenu,removeItem} from '../actions/MenuActions';
import { RNToasty } from 'react-native-toasty';

class Settings extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1'
    };

    state={
        openNotification:true
    }
    
    disableDrawer = () => {
        
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.disableDrawer()
    }

    componentDidMount(){
        this.disableDrawer();
        this.checkNotificationState()
    }

    componentWillUnmount(){
        this.props.removeItem()
    }

    checkNotificationState = async () => {
        const state = await AsyncStorage.getItem('notiState')
        console.log('State  ',state)
        this.setState({openNotification:state=='true'})
    }

    turnOnNotification = () => {
      
      axios.put(`${BASE_END_POINT}enableNotif`,{}, {
          headers: {
            'Content-Type': 'application/json',
            //this.props.currentUser.token
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
      }).then(Response=>{
          console.log("notification is on")
          RNToasty.Success({title:Strings.postTurnOn})
          AsyncStorage.setItem('notiState','true')
         
      }).catch(error=>{
          console.log(error.response)
      })
    }

    turnOfNotification = () => {
      
        axios.put(`${BASE_END_POINT}disableNotif`,{}, {
            headers: {
              'Content-Type': 'application/json',
              //this.props.currentUser.token
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
        }).then(Response=>{
            console.log("notification is on")
            RNToasty.Success({title:Strings.postTurnOf})
            AsyncStorage.setItem('notiState','false')
           
        }).catch(error=>{
            console.log(error.response)
        })
      }

   
    render(){
        const {isRTL,navigator} = this.props
        const {openNotification} = this.state;
        return(
           <View style={{flex:1,backgroundColor:'white'}} >
                <FlatAppHeader menu navigator={navigator} title={Strings.settings} />
                <TouchableOpacity 
                onPress={()=>{
                    navigator.push({
                        screen: 'Profile',
                        animated: true,
                        animationType:'slide-horizontal'
                    })
                }}
                style={{marginTop:moderateScale(5),flexDirection:isRTL?'row-reverse':'row',borderBottomColor:'gray',borderBottomWidth:0.5,width:responsiveWidth(100),alignItems:'center',height:responsiveHeight(8)}}>
                    <View style={{marginHorizontal:moderateScale(4)}}>
                        <Icon name='user-alt' type='FontAwesome5' style={{fontSize:responsiveFontSize(7), color:'#7D7D7D'}} />
                    </View>
                    <Text style={{color:'#7D7D7D'}}>{Strings.personalInformation}</Text>
                </TouchableOpacity>
                <View style={{flexDirection:isRTL?'row-reverse':'row',borderBottomColor:'gray',borderBottomWidth:0.5,width:responsiveWidth(100),justifyContent:'space-between' ,alignItems:'center',height:responsiveHeight(10)}}>
                    <View style={{flexDirection:isRTL?'row-reverse':'row', marginHorizontal:moderateScale(5)}}>
                        <Icon name='bell' type='FontAwesome' style={{fontSize:responsiveFontSize(7), color:'#7D7D7D'}} />
                        <Text style={{marginHorizontal:moderateScale(3), color:'#7D7D7D'}}>{Strings.notifications}</Text>
                    </View>
                    <Switch
                    style={{marginHorizontal:moderateScale(5)}}
                    onValueChange = {(val)=>{
                        this.setState({openNotification:val})
                        if(val){
                            this.turnOnNotification()
                        }else{
                            this.turnOfNotification()
                        }
                    }}
                    value={openNotification}
                    />
                </View>


           </View>
        );
    }
}

const mapToStateProps = state => ({
    currentUser : state.auth.currentUser,
    isRTL: state.lang.RTL,  
})

const mapDispatchToProps = {
    removeItem,
}


export default connect(mapToStateProps,mapDispatchToProps)(Settings);


import React,{Component} from 'react';
import {View,Text,TouchableOpacity,TextInput,ScrollView} from 'react-native';
import {  moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import * as colors from '../assets/colors'
import AppInput from '../common/AppInput';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import AppText from '../common/AppText';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import {BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import LoadingOverlay from '../components/LoadingOverlay';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import strings from '../assets/strings';


const MyButton =  withPreventDoubleClick(Button);



class ChangePassword extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#707D67',
    };

    state= {
        loading:false,
        currentPassword: ' ',
        newPassword: ' ',
        confirmPassword: ' ',
        hidePassword:true,

        confirmHidePassword: true,
    }

  
    componentDidMount(){    
        this.disableDrawer(); 
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.disableDrawer()
    }

    onChangePassword = (currentPassword,newPassword) => {
         this.setState({loading:true}) 
         axios.put(`${BASE_END_POINT}user/updatePassword`, JSON.stringify({
            currentPassword:currentPassword,
            newPassword:newPassword
         }) , {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          }).then(response=>{
              console.log('update user profile done')      
              console.log(response.data)
              this.setState({loading:false,noConnection:null});           
              RNToasty.Success({title:Strings.passwordChanges})
              this.props.navigator.pop()
        
        }).catch(error=>{
            console.log(error.response)
            this.setState({loading:false})
            if(!error.response){
                this.setState({noConnection:Strings.noConnection})
            }
            
            if(error.response.status == 400){
                RNToasty.Error({title:Strings.oldPassError})
            }
        })
       
    }

    render(){
        const { isRTL,navigator,currentUser } = this.props;
        const {confirmHidePassword,currentPassword,newPassword,confirmPassword,hidePassword} = this.state
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader navigator={navigator} title={Strings.changePassword}/>
                
                <ScrollView
                showsVerticalScrollIndicator={false}
                >
                <View style={{alignSelf:'center',marginTop:moderateScale(8),justifyContent:"flex-end",alignItems:'flex-end'}}>
                    <FastImage style={{height:150,width:150,borderRadius:75}} source={currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.jpg')} />
                    <TouchableOpacity
                      onPress={()=>{
                        navigator.push({
                            screen: 'ChangeProfilePhoto',
                            animated:true,
                            animationType:'slide-horizontal'
                        })
                    }}
                     style={{marginTop:moderateScale(-22), backgroundColor:'#679C8A', height:50,width:50,borderRadius:25,justifyContent:'center',alignItems:'center'}} >
                        <Icon name='camera-alt' type='MaterialIcons' style={{color:'white',fontSize:responsiveFontSize(10)}} />
                    </TouchableOpacity>
                </View>
                
                <View style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1,marginBottom:moderateScale(15),marginTop:moderateScale(8), width:responsiveWidth(90),alignSelf:'center',backgroundColor:'white',elevation:2}}>
                    <Text style={{marginBottom:moderateScale(8), fontSize:responsiveFontSize(8),color:'black',marginTop:moderateScale(5),alignSelf:'center'}} >{Strings.changeYourPassword}</Text>
                   
                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{marginHorizontal:moderateScale(2),color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.currentPassword}</Text>
                        <View style={{justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(84),alignSelf:'center' ,borderRadius:moderateScale(2),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                            <TextInput                 
                            onChangeText={(val)=>{this.setState({currentPassword:val})}}
                            placeholder={Strings.enteryourCurrentPassword} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(0),width:responsiveWidth(80), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                        </View>
                        {currentPassword.length==0&&
                        <Text style={{marginHorizontal:moderateScale(2),color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>

                    <View style={{marginVertical:moderateScale(4), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{marginHorizontal:moderateScale(2),color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.newPassword}</Text>
                    <View style={{alignSelf:'center', flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(84),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                
                        secureTextEntry={hidePassword}
                        onChangeText={(val)=>{this.setState({newPassword:val})}}
                        placeholder={Strings.enterYourNewPaswword} underlineColorAndroid='transparent' style={{width:responsiveWidth(78), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                        <TouchableOpacity 
                        onPress={()=>{this.setState({hidePassword:!hidePassword})}}
                        >
                            <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={hidePassword?'eye-off':'eye'} type='Feather' />
                        </TouchableOpacity>
                    </View>
                    {newPassword.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

                   {/* <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{marginHorizontal:moderateScale(2),color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.newPassword}</Text>
                        <View style={{backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(84),alignSelf:'center' ,borderRadius:moderateScale(2),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                            <TextInput                 
                            onChangeText={(val)=>{this.setState({newPassword:val})}}
                            placeholder={Strings.newPassword} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                        </View>
                        {newPassword.length==0&&
                        <Text style={{marginHorizontal:moderateScale(2),color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>
                    */}

                    
                <View style={{marginVertical:moderateScale(4), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{marginHorizontal:moderateScale(2),color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.confirmPassword}</Text>
                    <View style={{alignSelf:'center', flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(84),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                
                        secureTextEntry={confirmHidePassword}
                        onChangeText={(val)=>{this.setState({confirmPassword:val})}}
                        placeholder={Strings.confirmYourNewPassword} underlineColorAndroid='transparent' style={{width:responsiveWidth(78), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                        <TouchableOpacity 
                        onPress={()=>{this.setState({confirmHidePassword:!confirmHidePassword})}}
                        >
                            <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={confirmHidePassword?'eye-off':'eye'} type='Feather' />
                        </TouchableOpacity>
                    </View>
                    {confirmPassword.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

{/*
                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{marginHorizontal:moderateScale(2),color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.confirmPassword}</Text>
                        <View style={{backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(84),alignSelf:'center' ,borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                            <TextInput                 
                            onChangeText={(val)=>{this.setState({confirmPassword:val})}}
                            placeholder={Strings.confirmPassword} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                        </View>
                        {confirmPassword.length==0&&
                        <Text style={{marginHorizontal:moderateScale(2),color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>
                    */}

                    <View style={{width:responsiveWidth(90),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',height:responsiveHeight(8),marginTop:moderateScale(3),marginBottom:moderateScale(7)}}>
                        <TouchableOpacity
                        onPress={()=>{navigator.pop()}}
                        >
                            <Text style={{marginHorizontal:moderateScale(2),color:'gray',fontSize:responsiveFontSize(6)}}>{Strings.cancel}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                        onPress={()=>{
                            if(!currentPassword.replace(/\s/g, '').length){
                                this.setState({currentPassword:''})
                            }
                            if(!newPassword.replace(/\s/g, '').length){
                                this.setState({newPassword:''})
                            }
                            if(!confirmPassword.replace(/\s/g, '').length){
                                this.setState({confirmPassword:''})
                            }
                            
            
                            if(currentPassword.replace(/\s/g, '').length&&newPassword.replace(/\s/g, '').length&&confirmPassword.replace(/\s/g, '').length){
                            this.onChangePassword(currentPassword,newPassword,confirmPassword);
                            }
                        }}
                        >
                            <Text style={{color:'black',fontSize:responsiveFontSize(7)}}>{Strings.save}</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                </ScrollView>

                {this.state.loading&&<LoadingDialogOverlay title={Strings.wait}/>}
            </View>
        )
    }
}


const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps)(ChangePassword);


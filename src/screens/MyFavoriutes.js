import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,NetInfo,Text,FlatList} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import FavoritueCard from '../components/FavoritueCard'
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';

class MyFavoriutes extends Component {

    page=1;
    state= {
        errorText:null,
        //favorites: new DataProvider(),
        favorites: [],
        refresh:false,
        loading:true,
        pages:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };

          componentWillUnmount(){
            this.props.removeItem()
          }

    constructor(props) {
        super(props);     
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(49);
            dim.height = responsiveHeight(35);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getFav(this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getFav(this.page,true);
                }
            }
          );      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

    getFav(page, refresh) {
            //this.setState({loading:true})
            let uri = `${BASE_END_POINT}favourite/${this.props.currentUser.user.id}/users?page=${page}&limit=20`
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        favorites: refresh?response.data.data:[...this.state.favorites,...response.data.data],
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        
                        //favorites: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.favorites.getAllData(), ...response.data.data]),
                    })

                    console.log("MYFAV   ",response.data.data)
                
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error.response);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <FavoritueCard 
        data={data.add}
        navigator={this.props.navigator}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator} = this.props;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                 <FlatAppHeader menu navigator={navigator} title={Strings.myFav} />
                {this.state.loading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.state.favorites.length>0?
                <FlatList
                numColumns={2}
                ListFooterComponent={this.renderFooter}
                onEndReached={() => {      
                    if(this.page <= this.state.pages){
                        this.page++;
                        this.getFav(this.page, false);
                    }  
                }}
                refreshControl={
                <RefreshControl colors={["#B7ED03"]}
                refreshing={this.state.refresh}
                onRefresh={() => {
                  this.page = 1
                  this.getFav(this.page, true);
                }}
                />
                }
              onEndReachedThreshold={.5} 
                data={this.state.favorites}
                renderItem={({item})=>{
                    console.log("list item",item)
                    return(
                        <FavoritueCard 
                        data={item.add}
                        navigator={navigator}
                        />
                    )
                }}
                />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                <Text style={{color:'black'}}>{Strings.noDataAtRecent} </Text>
            </View>
                
                }
                
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,   
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(MyFavoriutes);


/*

                this.state.favorites._data.length>0?
                <RecyclerListView  
                extendedState={this.state}           
                layoutProvider={this.renderLayoutProvider}
                dataProvider={this.state.favorites}
                rowRenderer={this.renderRow}
                renderFooter={this.renderFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.state.pages){
                        this.page++;
                        this.getFav(this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.refresh}
                    onRefresh={() => {
                      this.page = 1
                      this.getFav(this.page, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}        
                />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Text style={{color:'black'}}>{Strings.noDataAtRecent} </Text>
                </View>
                

*/
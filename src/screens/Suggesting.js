import React,{Component} from 'react';
import {View,ScrollView,Text,TouchableOpacity,TextInput} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';
import {Icon} from 'native-base'
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'


class Suggesting extends Component {

    state={
        load:false,
        details:' ',
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };
   
     componentWillUnmount(){
        this.props.removeItem()
    }
   

    componentDidMount(){
        this.enableDrawer() 
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

  

   

    render(){
        const {navigator,isRTL} = this.props;
        const {load,details} = this.state
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader menu navigator={navigator} title={Strings.suggestion} />


                <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.suggestion}</Text>
                    <View style={{justifyContent:'flex-start', alignSelf:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                        multiline={true}
                        
                        onChangeText={(val)=>{this.setState({details:val})}}
                        placeholder={Strings.enterYourDetails} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(2), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {details.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>


                    <TouchableOpacity
                        style={{marginBottom:moderateScale(20),  width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(2)}}
                        onPress={()=>{
                            if(!details.replace(/\s/g, '').length){
                                this.setState({details:''})
                            }
                            
                            if(details.replace(/\s/g, '').length){
                                var suggestion ={
                                    description:details,
                                }
                            
                                this.setState({load:true})
                                axios.post(`${BASE_END_POINT}suggestion`,JSON.stringify(suggestion), {
                                    headers: {
                                      'Content-Type': 'application/json',
                                    },
                                  }).then(response=>{
                                      console.log('add Ads')
                                    this.setState({load:false});
                                    RNToasty.Success({title:Strings.yourSuggestSend})
                                    navigator.resetTo({
                                        screen:'Home',
                                        animated: true,
                                        animationType:'slide-horizontal'
                                    })
                                  }).catch(error=>{
                                    console.log('Error  ',error)
                                    console.log('Error  ',error.reponse)
                                    this.setState({load:false});
                                    if(error.response.status == 505){
                                        RNToasty.Error({title:Strings.youAreBlockedToReport})
                                    }
                                  })
                            }
                        }}
                        >
                        <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.send}</Text>
                    </TouchableOpacity>

                    {this.state.load && <LoadingDialogOverlay title={Strings.wait}/>}

            </View>   
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,  
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(Suggesting);

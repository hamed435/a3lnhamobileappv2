import React,{Component} from 'react';
import {Modal,TextInput, TouchableOpacity, FlatList, View,RefreshControl,StyleSheet,NetInfo,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon} from 'native-base'
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import MyFollowersCard from '../components/MyFollowersCard'
import CommentCard from '../components/CommentCard'
import Stars from 'react-native-stars';

class AdsComments extends Component {

    page=1;
    state= {
        errorText:null,
        comments: [],
        refresh:false,
        loading:true,
        pages:null,
        showModal:false,
        rate:2.5,
        comment: ' ',
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };
   
 
    constructor(props) {
        super(props);  
        this.renderLayoutProvider = new LayoutProvider(
          () => 3,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(13);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getComments(this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getComments(this.page,true);
                }
            }
          );      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

    getComments(page, refresh) {
            //this.setState({loading:true})
            let uri = `${BASE_END_POINT}ads/${this.props.adsId}/findAdsRate?page=${page}&limit=20`
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        comments:this.state.refresh? [...response.data.data]:[...this.state.comments,...response.data.data],
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                       
                    })
                    console.log('followers   ',response.data)
                    
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log("error   ",error);
                    console.log("error   ",error.response);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <MyFollowersCard 
       data={data}
       navigator={this.props.navigator}
        onPress={()=>{
            this.props.navigator.push({
                screen:'AddsDetails',
                animated:true,
                animationType:'slide-horizontal',
                passProps: {
                    data:data
                }
            })
        }}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,isRTL} = this.props;
        const {comment} = this.state
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader  navigator={navigator} title={Strings.comments} />
                
                {this.state.loading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.state.comments.length>0 ?
                <FlatList
                style={{marginBottom:moderateScale(25)}}
                data={this.state.comments}
                renderItem={({item})=><CommentCard navigator={navigator} data={item} />}
                
                ListFooterComponent={this.renderFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.state.pages){
                        this.page++;
                        this.getComments(this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                  refreshing={this.state.refresh}
                  onRefresh={() => {
                      this.page = 1
                      this.getComments(1, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}
                />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Text style={{color:'black'}}>{Strings.noDataAtRecent} </Text>
                </View>

                }
                

                <Modal
                onRequestClose={()=>this.setState({showModal:false})}
                animationType="slide"
                transparent={false}
                visible={this.state.showModal}
                >
                  {/*  <TouchableOpacity
                    onPress={()=>{this.setState({showModal:false})}}
                    style={{alignSelf:'flex-end',margin:moderateScale(10)}}>
                    <Icon name='closecircle' type='AntDesign' />
                    </TouchableOpacity>
                  */}
                   <FlatAppHeader  navigator={navigator} title={Strings.rate} />

                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:'center', marginTop:moderateScale(6)}} >{Strings.writeaReview}</Text>
                    <View style={{marginTop:moderateScale(8),alignSelf:'center' }}>
                    <Stars                                  
                    default={this.state.rate}
                    count={5}
                    half={true}
                    update={(val)=>{this.setState({rate: val})}}
                    fullStar={<Icon name='star' type='MaterialCommunityIcons' style={[styles.myStarStyle]}/>}
                    emptyStar={<Icon name={'star-outline'} type='MaterialCommunityIcons' style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
                    halfStar={<Icon name={'star-half'} type='MaterialIcons' style={[styles.myStarStyle]}/>}
                    />
                    </View>
                    <Text style={{color:'black',fontSize:responsiveFontSize(5),alignSelf:'center'}} >{Strings.tabStar}</Text>

                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <View style={{backgroundColor:'#e8edf2',  justifyContent:'flex-start', alignSelf:'center',  elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                        multiline={true}                      
                        onChangeText={(val)=>{this.setState({comment:val})}}
                        placeholder={Strings.enterYourComment} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                        {comment.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>

                    <TouchableOpacity
                    onPress={()=>{
                        if(!comment.replace(/\s/g, '').length){
                            this.setState({comment:''})
                        }else{
                          const  data={
                                rate:this.state.rate,
                                comment:this.state.comment
                            }
                            axios.put(`${BASE_END_POINT}ads/${this.props.adsId}/rate`,JSON.stringify(data),{
                                headers: {
                                  'Content-Type': 'application/json',
                                  'Authorization': `Bearer ${this.props.currentUser.token}`
                                },
                              })
                              .then(response=>{
                                this.setState({showModal:false})
                                this.page = 1
                                this.getComments(1, true);
                              })
                              .catch(error=>{
                                  console.log("ERROR   ",error.response)
                              })
                        }
                    }}
                    style={{width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(3)}}               
                    >
                        <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.submit}</Text>
                    </TouchableOpacity>

                </Modal>

                <TouchableOpacity
                onPress={()=>{this.setState({showModal:true})}}
                style={{width:responsiveWidth(100),height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',position:'absolute',bottom:0,left:0,right:0}}               
                >
                    <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.addComment}</Text>
                </TouchableOpacity>
                
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    myStarStyle: {
      color: '#FFC416',
      backgroundColor: 'transparent',
      textShadowColor: 'black',
     
      fontSize:responsiveFontSize(17),
      margin:0,
      padding:0,
    },
    myEmptyStarStyle: {
      color: 'gray',
      fontSize:responsiveFontSize(17),
      margin:0,
      padding:0,
    }
  });



const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,  
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(AdsComments);

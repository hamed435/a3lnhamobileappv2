import React,{Component} from 'react';
import {View,RefreshControl ,StyleSheet,NetInfo,Text,TouchableOpacity,FlatList} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import {Icon} from 'native-base'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import FavoritueCard from '../components/FavoritueCard'
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import HomeAdsCard from '../components/HomeAdsCard'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

class HomeSearchResults extends Component {

    page=1;
    state= {
        
        ads: new DataProvider(),
        
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };

         

    constructor(props) {
        super(props);     
        this.renderLayoutProvider = new LayoutProvider(
            () => 2,
            (type, dim) => {
              dim.width = responsiveWidth(50);
              dim.height = responsiveHeight(35);
            },
        );

      }

      componentDidMount(){
        this.enableDrawer()
      }

      componentDidUpdate(){
        this.enableDrawer()
      }

    
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

  
    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
            <HomeAdsCard onPress={()=>{
               this.props.navigator.push({
                screen:data.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                animated:true,
                animationType:'slide-horizontal',
                passProps:{data:data}
              })
            }} navigator={this.props.navigator} data={data}
             />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

      

      
     
    render(){
        const {navigator,data,isRTL} = this.props;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                 <FlatAppHeader  navigator={navigator} title={Strings.results} />
                 
                 <FlatList
                 numColumns={2}
                 data={data}
                 renderItem={({item})=>(
                    <HomeAdsCard onPress={()=>{
                        this.props.navigator.push({
                         screen:item.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                         animated:true,
                         animationType:'slide-horizontal',
                         passProps:{data:item}
                       })
                     }} navigator={this.props.navigator} data={item}
                      />
                 )}
                 />
              
               
            
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,   
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(HomeSearchResults);

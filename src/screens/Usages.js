import React,{Component} from 'react';
import {View,ScrollView,Text,ActivityIndicator} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';
import {Icon} from 'native-base'
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';


class Usages extends Component {

    state={
        load:true,
        usage:null
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };
   
     componentWillUnmount(){
        this.props.removeItem()
    }

    getUsage = ()=>{
        axios.get(`${BASE_END_POINT}about`)
        .then(response=>{
            this.setState({usage:response.data[0],load:false})
        })
        .catch(error=>{
            this.setState({load:false})
            console.log('error   ',error)
        })
    }

   

    componentDidMount(){
        this.enableDrawer() 
        this.getUsage()    
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

  

   

    render(){
        const {navigator,isRTL} = this.props;
        const {load,usage} = this.state
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader menu navigator={navigator} title={Strings.usages} />
                {load?
                <View style={{alignSelf:'center', marginTop:moderateScale(25)}}>
                    <ActivityIndicator size='large' />
                </View>
                :
                <ScrollView style={{width:responsiveWidth(100)}} >
                    <View style={{alignSelf:'center', marginTop:moderateScale(7), width:responsiveWidth(90)}} >
                        <Text style={{color:'#679C8A',fontSize:responsiveFontSize(12), alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.about}</Text>
                        <View style={{borderRadius:moderateScale(3), backgroundColor:'#f2f2f0', marginTop:moderateScale(4)}}>
                            <Text style={{margin:moderateScale(4), color:'#646464'}}>{usage.about}</Text>
                        </View>
                    </View>

                    <View style={{alignSelf:'center', marginTop:moderateScale(7), width:responsiveWidth(90)}} >
                        <Text style={{color:'#679C8A',fontSize:responsiveFontSize(12), alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.usages}</Text>
                        <View style={{borderRadius:moderateScale(3),backgroundColor:'#f2f2f0', marginTop:moderateScale(4)}}>
                            <Text style={{margin:moderateScale(4), color:'#646464'}}>{usage.usage}</Text>
                        </View>
                    </View>

                    <View style={{alignSelf:'center', marginTop:moderateScale(7), width:responsiveWidth(90)}} >
                        <Text style={{color:'#679C8A',fontSize:responsiveFontSize(12), alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.conditions}</Text>
                        <View style={{borderRadius:moderateScale(3),backgroundColor:'#f2f2f0', marginTop:moderateScale(4)}}>
                            <Text style={{margin:moderateScale(4), color:'#646464'}}>{usage.conditions}</Text>
                        </View>
                    </View>

                    <View style={{ marginBottom:moderateScale(7), alignSelf:'center', marginTop:moderateScale(7), width:responsiveWidth(90)}} >
                        <Text style={{color:'#679C8A',fontSize:responsiveFontSize(12), alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.privacy}</Text>
                        <View style={{borderRadius:moderateScale(3),backgroundColor:'#f2f2f0', marginTop:moderateScale(4)}}>
                            <Text style={{margin:moderateScale(4), color:'#646464'}}>{usage.privacy}</Text>
                        </View>
                    </View>

                </ScrollView>
                }
            </View>   
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,  
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(Usages);

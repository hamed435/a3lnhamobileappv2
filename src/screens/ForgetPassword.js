import React, { Component } from "react";
import AsyncStorage  from '@react-native-community/async-storage'
import {
     View, StyleSheet, Dimensions, Keyboard,
     StatusBar, ImageBackground, KeyboardAvoidingView, NetInfo,
      TouchableOpacity, Platform, Alert, TextInput, ActivityIndicator,Text
     } from "react-native";
import { Button,Item,Label,Icon } from "native-base";
import { AppInput, AppText, AppHeader } from "../common";
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import { connect } from "react-redux"
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import allStrings from '../assets/strings';
import * as colors from '../assets/colors';
import Swiper from "react-native-swiper";
import { API_ENDPOINT } from '../AppConfig';
const { width, height } = Dimensions.get('window');
import axios from 'axios'
import { BASE_END_POINT } from '../AppConfig';
import CodeInput from 'react-native-confirmation-code-input';
import { RNToasty } from 'react-native-toasty';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import FlatAppHeader from '../common/FlatAppHeader';
import CountDown from 'react-native-countdown-component';

titles=[allStrings.forgetPassword,allStrings.verifyEmail,allStrings.newPassword]

class ForgetPassword extends Component {
    
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };

    state={
        currentPage:0,
        email:' ',
        password: ' ',
        confirmPassword: ' ',
        loading:false,
        isFullcode:false,
        code:null,
        hidePassword:true,
    }
    swiper=null;


    sendCode(code) {
        console.log("email   ",this.state.email)
        let body = { 
            email: this.state.email.toLowerCase(),
            verifycode: code
         }
         this.setState({ loading: true, })
        axios.post(`${BASE_END_POINT}confirm-code`, JSON.stringify(body), {
            headers: { 'Content-Type': 'application/json', },
        }).then(response => {
            this.setState({
                currentPage: 2, loading: false,
            })
            //this.swiper.scrollBy(1);

        }).catch(error => {
            this.setState({ loading: false })
            if (error.request.status === 400) {              
                RNToasty.Error({title:allStrings.correctCode})
            }
            if(!error.response){
                this.setState({noConnection:'allStrings.noConnection'})
            }
            console.log('code error')
            console.log(error)
            console.log(error.response)
        });
    }
    _onFulfill(code) {
        this.sendCode(code);
    }

    
    sendNewPassword() {
        let body = { email: this.state.email.toLowerCase(), newPassword: this.state.password }
        this.setState({ loading: true, })
        axios.post(`${BASE_END_POINT}reset-password`, JSON.stringify(body), {
            headers: { 'Content-Type': 'application/json', },
        }).then(response => {
            this.setState({ loading: false, })
            RNToasty.Success({title:allStrings.passwordCahngedSuccessfuly})
            this.props.navigator.pop()
        }).catch(error => {
            this.setState({ loading: false, })
            if(!error.response){
                this.setState({noConnection:'allStrings.noConnection'})
            }
            console.log('new Password error')
            console.log(error)
            console.log(error.response)
        });
    }

    renderInsertEmailPage = () => {
        const { navigator,isRTL } = this.props
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {allStrings.email}</Text>
                <View style={{justifyContent:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                <TextInput 
                keyboardType='email-address'
                onChangeText={(val)=>{this.setState({email:val})}}
                placeholder={allStrings.enterYourEmail} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                </View>
                {this.state.email.length==0&&
                <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {allStrings.require}</Text>
                }
            </View>
                <TouchableOpacity
                style={{width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(18),borderRadius:moderateScale(3)}}
                onPress={()=>{               
                if(!this.state.email.replace(/\s/g, '').length){
                    this.setState({email:''})
                } 
               
                if(this.state.email.replace(/\s/g, '').length){
                    const body = {
                        email:this.state.email.toLowerCase()
                    }
                    this.setState({loading: true})
                    axios.post(`${BASE_END_POINT}sendCode`, JSON.stringify(body), {
                     headers: { 'Content-Type':  'application/json', },
                    })
                    .then(response => {
                        console.log('phone done')
                        console.log(response.data)        
                        this.setState({currentPage: 1,loading: false})
                       // this.swiper.scrollBy(0);
                    })
                    .catch(error => {
                    this.setState({loading:false});
                    if (error.request.status ===  400) {
                        RNToasty.Error({title:allStrings.enterCorrectEmail})
                    }
                    if(!error.response){
                        this.setState({noConnection:'allStrings.noConnection'});
                    }
                    console.log('phone error')
                    console.log(error)
                    console.log(error.response)
                    });
                            
                }            
                }}
                >
                <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{allStrings.send}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    
    renderVerifyCodePage = () =>{
        const { navigator,isRTL } = this.props
        const {code,isFullcode} = this.state
        return(
            <View style={{ flex: 1,backgroundColor:'white'}} >
             <Text style={{marginHorizontal:moderateScale(5), marginTop:moderateScale(5),color:'#828282'}} >{allStrings.enterPin}</Text>
              
              
              <View style={{justifyContent:'center',alignItems:'center', alignSelf:'center',marginTop:moderateScale(20),height:responsiveHeight(15)}}>
              
                    <CodeInput
                    ref="codeInputRef1"
                    secureTextEntry
                    //compareWithCode='1234'
                    codeLength={4}
                    className={'border-b'}
                    space={5}
                    size={30}
                    inputPosition='center'
                    activeColor='black'//'rgba(49, 180, 4, 1)'
                    inactiveColor='#C1C1C1'
                    onFulfill={(code) => {
                        console.log("code is ", code)
                        this.setState({isFullcode:true,code:code})
                    }}
                    />
                </View>

                <TouchableOpacity
                style={{width:responsiveWidth(90),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(2)}}
                onPress={()=>{               
                if(this.state.isFullcode){
                    this.sendCode(this.state.code)
                   
                } else{
                    RNToasty.Info({title:allStrings.pinCode})
                }            
                }}
                >
                    <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{allStrings.verify}</Text>
                </TouchableOpacity>
               
                <View style={{marginTop:moderateScale(2),width:responsiveWidth(90),alignSelf:'center',justifyContent:'space-between',flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <TouchableOpacity
                    onPress={()=>{
                        const body = {
                            email:this.state.email.toLowerCase()
                        }
                        this.setState({loading: true})
                        axios.post(`${BASE_END_POINT}sendCode`, JSON.stringify(body), {
                         headers: { 'Content-Type':  'application/json', },
                        })
                        .then(response => {
                            console.log('phone done')
                            console.log(response.data)        
                            this.setState({loading: false})
                            RNToasty.Success({title:allStrings.weResendTheCodeToYourEmail})
                            
                        })
                        .catch(error => {
                        this.setState({loading:false});
                        
                        console.log('phone error')
                        console.log(error)
                        console.log(error.response)
                        });
                                
                      
                    }}
                    >
                        <Text style={{color:'#A3A3A3'}}>{allStrings.resenCode}</Text>
                    </TouchableOpacity>
                    <CountDown
                    until={60 * 2 + 30}
                    size={responsiveFontSize(7)}
                    onFinish={() => {
                        const body = {
                            email:this.state.email.toLowerCase()
                        }
                        this.setState({loading: true})
                        axios.post(`${BASE_END_POINT}sendCode`, JSON.stringify(body), {
                         headers: { 'Content-Type':  'application/json', },
                        })
                        .then(response => {
                            console.log('phone done')
                            console.log(response.data)        
                            this.setState({loading: false})
                            RNToasty.Success({title:allStrings.weResendTheCodeToYourEmail})
                        })
                        .catch(error => {
                        this.setState({loading:false});
                        
                        console.log('phone error')
                        console.log(error)
                        console.log(error.response)
                        });
                                
                    }}
                    digitStyle={{backgroundColor: 'transparent'}}
                    digitTxtStyle={{color: '#A3A3A3'}}
                    timeToShow={['M', 'S']}
                    timeLabels={{m: '', s: ''}}
                />
                </View>
            
            </View>       
        )
    }

    renderInsertPasswordlPage = () => {
        const { navigator,isRTL } = this.props
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <View style={{marginVertical:moderateScale(4), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.password}</Text>
                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                
                        secureTextEntry={this.state.hidePassword}
                        onChangeText={(val)=>{this.setState({password:val})}}
                        placeholder={Strings.enterYourPassword} underlineColorAndroid='transparent' style={{width:responsiveWidth(81), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                        <TouchableOpacity 
                        onPress={()=>{this.setState({hidePassword:!this.state.hidePassword})}}
                        >
                            <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={this.state.hidePassword?'eye-off':'eye'} type='Feather' />
                        </TouchableOpacity>
                    </View>
                    {this.state.password.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

            <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {allStrings.confirmPassword}</Text>
                <View style={{justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                <TextInput 
                secureTextEntry
                onChangeText={(val)=>{this.setState({confirmPassword:val})}}
                placeholder={Strings.enterYourConfirmPassword} underlineColorAndroid='transparent' style={{width:responsiveWidth(86), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                </View>
                {this.state.confirmPassword.length==0&&
                <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {allStrings.require}</Text>
                }
            </View>

                <TouchableOpacity
                style={{width:responsiveWidth(90),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(18),borderRadius:moderateScale(2)}}
                onPress={()=>{ 
               
                if(!this.state.password.replace(/\s/g, '').length){
                    this.setState({password:''})
                } 
                if(!this.state.confirmPassword.replace(/\s/g, '').length){
                    this.setState({confirmPassword:''})
                } 
                if(this.state.password!=this.state.confirmPassword){
                    RNToasty.Error({title:allStrings.errorConfirmPassword})
                }
                if(this.state.password.replace(/\s/g, '').length&&this.state.confirmPassword.replace(/\s/g, '').length){
                    this.sendNewPassword()
                }             
                }}
                >
                <Text style={{color:'white',fontSize:responsiveFontSize(7)}}>{allStrings.save}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const { navigator,isRTL } = this.props
        const {currentPage} = this.state
        return (
            <View style={{flex:1,backgroundColor:'white'}}>
                 <FlatAppHeader navigator={navigator} title={titles[currentPage]} />
                   <View style={{flex:1}}>
                       {/* <Swiper
                            style={{flex:1}}
                            loop={false}
                            autoplay={false}
                            scrollEnabled={false}
                            showsButtons={false}
                            showsPagination={false}
                            ref={(s) => this.swiper = s}
                            index={this.state.currentPage}                            
                        >
                        
                            
                            
                        </Swiper>
                       */}
                       {
                       this.state.currentPage ==0&&
                       this.renderInsertEmailPage()
                       }
                       
                       {
                       this.state.currentPage ==1&&
                       this.renderVerifyCodePage()
                       }

                       {this.state.currentPage ==2&&
                       this.renderInsertPasswordlPage()
                       }
                       
                    </View>
                    
                    {this.state.loading&& <LoadingDialogOverlay title={allStrings.wait}/>}
                
            </View>
        )
    }
}


const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser, 
})

export default connect(mapToStateProps)(ForgetPassword);
import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,TextInput,Modal,ScrollView,TouchableOpacity,Text,FlatList,ActivityIndicator,ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import LottieView from 'lottie-react-native';
import Slider from 'react-native-slider'
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import MapView, {Marker} from 'react-native-maps';


class UpdateCarAds extends Component {

    state = {
        lat:this.props.data.location[0],
        lng:this.props.data.location[1],
        showDialog:false,
       countries:[],
       cities:[],
       categories:[],
       subCategories:[], 
       images:this.props.data.img, 
       selectedCountry:this.props.data.country,
       selectedCity:this.props.data.city,
       selectedGoal:0,
       title:this.props.data.title,
       price:Number(this.props.data.price) ,
       selectedAddress:this.props.data.address,
       details: this.props.data.description,
       email: this.props.data.email,
       phone: this.props.data.phone.substring(5),
       participants:this.props.data.participate?this.props.data.participate:[],
       updateLoading:false,
       load:false,
       imgFlag:false,
       index:0,
       participationName:'',
       users:[],
       val:'',
       id:null,
       selectImgFlag:false,
       catLoad:false,
       subCatLoad:true,
       imgDialge:false,
       usage:this.props.data.usage,
        color:1,
        beds:this.props.data.beds,
        baths:this.props.data.baths,
        space:this.props.data.space,
        selectedCategory:this.props.data.category.id,
       selectedSubCategory:this.props.data.subCategory.id,
       currency:this.props.data.country.currency,
       countryCode:this.props.data.country.countryCode,
       categoryModal:false,
       subCategoryModal:false,
       subCategoryText:this.props.isRTL?this.props.data.subCategory.arabicName:this.props.data.subCategory.name,

       selectedBrand:0,
       brandLoad:true,
       brands:[],
       brandText:' ',
       brandImage:'',
       showBrandModal:false,
 
 
       selectedModal:0,
       modalLoad:true,
       modals:[],
       showModalModal:false,
       modalText:'',
 
       color2:this.props.data.color.colorName,
       colorId:this.props.data.color.id,     
       colores:[],
       year:Number(this.props.data.year),
       mileage:this.props.data.mileage,
       years:[],
    }

    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#707D67',
    };


        
    componentDidMount(){
        console.log("ADS DATA  ",this.props.data)    
        this.disableDrawer()
        this.getCountries()
        this.getSubCategories(this.props.data.category.id)      
        this.getCities(this.props.data.country.id);
        this.getBrands()
        this.getColors()

        const currentYear=new Date().getFullYear()
        var list = [];
        for (var i = 1900; i <= currentYear; i++) {
            list.push(i);
            console.log("YEAR   ",i)
        }
        this.setState({years:list})
    }

    componentWillUnmount(){
       // this.props.removeItem()
      }

      getColors() {   
        axios.get(`${BASE_END_POINT}color`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
            },
          })
            .then(response => {
                console.log('Citirs   ',response.data.data)
                this.setState({colores:response.data.data})
                
            
            }).catch(error => {
                console.log("Citirs error   ",error);
                console.log("error   ",error.response);         
            })   
    }

    componentDidUpdate(){
        this.disableDrawer()
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    getUsers(user) {   
        axios.put(`${BASE_END_POINT}searchUser`,JSON.stringify({search:user}),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
            .then(response => {
                this.setState({users:response.data})
                
                console.log('USer   ',response.data)
               
            }).catch(error => {
                console.log("user   ",error);
                console.log("users   ",error.response);         
            })   
    }

    getCategories(flag) {
        this.setState({catLoad:true})   
        axios.get(`${BASE_END_POINT}categories`)
            .then(response => {
                var id=0
                if(flag){
                    id=this.props.data.category.id
                }else{
                    id=response.data.data[0].id
                }
                
                this.setState({categories:response.data.data,catLoad:false})
                console.log('Categories   ',response.data)
                //this.getSubCategories(id,flag?true:false)
            }).catch(error => {
                this.setState({catLoad:false})
                console.log("error   ",error);
                console.log("error   ",error.response);         
            })   
}

getSubCategories(id) {  
    this.setState({subCatLoad:true}) 
    axios.get(`${BASE_END_POINT}categories/${id}/sub-categories`)
        .then(response => {
            if(response.data.data.length>0){
            this.setState({subCategories:response.data.data,subCatLoad:false,})
            }else{
                this.setState({subCategories:response.data.data,subCatLoad:false,})
            }
            console.log('sub Categories   ',response.data)
        
        }).catch(error => {
            this.setState({subCatLoad:false}) 
            console.log("sub error   ",error);
            console.log("error   ",error.response);         
        })   
}

getCountries() {   
    axios.get(`${BASE_END_POINT}countries`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
        },
      })
        .then(response => {
            this.setState({countries:response.data.data})
            
            console.log('countries   ',response.data)
           // this.getCities(response.data.data[0].id)
        
        }).catch(error => {
            console.log("countries error   ",error);
            console.log("error   ",error.response);         
        })   
}

    getCities(countryId,flag) {   
        axios.get(`${BASE_END_POINT}countries/${countryId}/cities`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
            },
          })
            .then(response => {
                console.log('Citirs   ',response.data.data)
                this.setState({cities:response.data.data})
                if(flag){
                    this.setState({selectedCity:response.data.data[0].id})
                }
            
            }).catch(error => {
                console.log("Citirs error   ",error);
                console.log("error   ",error.response);         
            })   
    }

    getBrands() {
        this.setState({brandLoad:true})   
        axios.get(`${BASE_END_POINT}brand`)
            .then(response => {
                this.setState({brandText:response.data.data[0].brandname,brandImage:response.data.data[0].img, brands:response.data.data,selectedBrand:response.data.data[0].id,brandLoad:false})
                console.log('brand   ',response.data)
                //this.getSubCategories(response.data.data[0].id)
                this.getModels(response.data.data[0].id)
            }).catch(error => {
                this.setState({brandLoad:false})
                console.log("error   ",error);
                console.log("error   ",error.response);         
            })   
    }

    getModels(id) {
        this.setState({modalLoad:true})   
        axios.get(`${BASE_END_POINT}model/${id}/brands`)
            .then(response => {
                console.log('models   ',response.data)

                this.setState({modals:response.data,selectedModal:response.data[0].id,modalText:response.data[0].modelname,modalLoad:false})
                
            }).catch(error => {
                this.setState({modalLoad:false})
                console.log("error   ",error);
                console.log("error   ",error.response);         
            })   
    }
    



    render(){
        const {data,currentUser,isRTL,navigator} = this.props;
        const {colorId,colores,color2,year,mileage, modalText,showBrandModal, selectedBrand,brandLoad,brands,brandText,brandImage, selectedModal,modalLoad,modals,showModalModal, subCategoryText, categoryModal,subCategoryModal,currency,countryCode,usage, beds,baths,space, color,imgDialge, catLoad,subCatLoad, index,imgFlag,selectedCity, images, countries,cities, categories,subCategories, selectedCategory,selectedSubCategory, selectedGoal, title,price,selectedCountry,selectedAddress,details,email,phone, participants} = this.state;
        return(
            <View style={{ flex:1,backgroundColor:'white' }}>
                <FlatAppHeader  navigator={navigator} title={Strings.updateAds} />
                <ScrollView>
                    {/* #CFCECE */}
                    <Text style={{marginHorizontal:moderateScale(10), marginTop:moderateScale(5), alignSelf:isRTL?'flex-end':'flex-start', color:'black',fontSize:responsiveFontSize(7)}}>{Strings.attachPhoto}</Text>
                    <View style={{flexWrap:'wrap', flexDirection:isRTL?'row-reverse':'row', marginVertical:moderateScale(5),width:responsiveWidth(96),alignSelf:'center'}}>
                                                                                   
                        <TouchableOpacity
                        onPress={()=>{this.setState({imgDialge:true})}} style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, marginTop:moderateScale(4), marginHorizontal:moderateScale(4), alignSelf:isRTL?'flex-end':'flex-start', width:responsiveWidth(27),height:responsiveHeight(18),justifyContent:'center',alignItems:'center',backgroundColor:'white',elevation:1,borderRadius:moderateScale(3)}}>
                            <Icon style={{fontSize:responsiveFontSize(8),color:'#CFCECE'}} name='plus' type='Entypo' />
                        </TouchableOpacity>
                        {images.length==0&&imgFlag&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.require}</Text>
                        }
                                   
                        {images.map((val,index)=>(
                        
                        <ImageBackground
                        source={{uri:val}}                      
                         style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,marginHorizontal:moderateScale(4),alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(2), width:responsiveWidth(25),height:responsiveHeight(18),justifyContent:'center',alignItems:'center',backgroundColor:'white',elevation:1,borderRadius:moderateScale(3)}}>
                            
                        </ImageBackground>
                        
                        ))
                    }
                    </View>
                  
                    <TouchableOpacity 
                     onPress={()=>this.setState({categoryModal:true})}
                    style={{alignSelf:'center', marginTop:moderateScale(7),flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(4)}} >{isRTL?data.category.arabicName:data.category.name}</Text>
                        <View style={{marginHorizontal:moderateScale(4)}}>
                            <Icon name='caretdown' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(4)}} />
                        </View>
                    </TouchableOpacity>
                    <Modal
                    onRequestClose={()=>this.setState({categoryModal:false})}
                     animationType="slide"
                     transparent={false}
                     visible={categoryModal}
                    >
                        <TouchableOpacity
                         onPress={()=>this.setState({categoryModal:false})}
                         style={{alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10),marginBottom:moderateScale(5), marginHorizontal:moderateScale(10)}}>
                            <Icon name='close' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(11)}} />
                        </TouchableOpacity>
                        <TouchableOpacity 
                         onPress={()=>this.setState({selectedCategory:data.id, categoryModal:false})}
                        style={{height:responsiveHeight(10),justifyContent:'center', width:responsiveWidth(100),borderBottomColor:'#d7dade',borderBottomWidth:0.5}}>
                        <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(6)}} >{isRTL?data.category.arabicName:data.category.name}</Text>
                        </TouchableOpacity>
                    </Modal>


                    {/*<View style={{alignSelf:'center', width:responsiveWidth(90), marginTop:moderateScale(5)}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.category}</Text>
                        <View style={{marginTop:moderateScale(2),justifyContent:'center',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{ width:responsiveWidth(85)}}>
                        {!catLoad? 
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'#CFCECE' }}
                        selectedValue={selectedCategory}
                        onValueChange={(val)=>{
                            console.log("CAT VAL  ",val)
                            this.setState({selectedCategory:val})
                            //this.getSubCategories(val)

                        }}
                        >
                            <Picker.Item label={isRTL?data.category.arabicName:data.category.name} value={data.category.id} />                 
                        
                        </Picker>
                        :
                        <ActivityIndicator size='small'  />
                        }
                       </View>
                    </View>
                       
                    </View>
                    */}

                    <TouchableOpacity 
                     onPress={()=>this.setState({subCategoryModal:true})}
                    style={{alignSelf:'center', marginTop:moderateScale(7),flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(4)}} >{subCategoryText}</Text>
                        <View style={{marginHorizontal:moderateScale(4)}}>
                            <Icon name='caretdown' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(4)}} />
                        </View>
                    </TouchableOpacity>
                    <Modal
                    onRequestClose={()=>this.setState({subCategoryModal:false})}
                     animationType="slide"
                     transparent={false}
                     visible={subCategoryModal}
                    >
                        
                        <TouchableOpacity
                         onPress={()=>this.setState({subCategoryModal:false})}
                         style={{alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10),marginBottom:moderateScale(5), marginHorizontal:moderateScale(10)}}>
                            <Icon name='close' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(11)}} />
                        </TouchableOpacity>
                        
                        
                        {subCategories.map(data=>(
                             <TouchableOpacity 
                             onPress={()=>this.setState({subCategoryText:isRTL?data.arabicName:data.name, selectedSubCategory:data.id, subCategoryModal:false})}
                            style={{height:responsiveHeight(10),justifyContent:'center', width:responsiveWidth(100),borderBottomColor:'#d7dade',borderBottomWidth:0.5}}>
                            <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(6)}} >{isRTL?data.arabicName:data.name}</Text>
                            </TouchableOpacity>
                        ))}
                       
                    </Modal>



                   {/* <View style={{alignSelf:'center', width:responsiveWidth(90), marginTop:moderateScale(5)}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.subCategory}</Text>
                        <View style={{marginTop:moderateScale(2),justifyContent:'center',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{ width:responsiveWidth(85)}}>
                        {!subCatLoad?
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'#CFCECE' }}
                        selectedValue={selectedSubCategory}
                        onValueChange={(val)=>{
                            this.setState({selectedSubCategory:val})
                        }}
                        >
                           {subCategories.map(val=>(
                                <Picker.Item label={isRTL?val.arabicName:val.name} value={val.id} />
                           ))}
                        </Picker>
                        :
                        <ActivityIndicator size='small'  />
                        }
                       </View>
                    </View>
                       
                    </View>
                    */}
                    
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.usage}</Text>
                    <View style={{marginVertical:moderateScale(7),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',alignSelf:isRTL?'flex-end':'flex-start'}} >
                        <TouchableOpacity
                        onPress={()=>{
                            this.setState({color:0,usage:'New'})
                        }}
                         style={{height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:color==0?'#679C8A':'white'}}>
                            <Text style={{color:color==0?'white':'gray'}}>{Strings.new}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                        onPress={()=>{
                            this.setState({color:1,usage:'Used'})
                        }}
                         style={{marginHorizontal:moderateScale(5), height:responsiveHeight(7),borderWidth:0.5,borderColor:'gray', justifyContent:'center',alignItems:'center',width:responsiveWidth(20),borderRadius:moderateScale(4),backgroundColor:color==1?'#679C8A':'white'}}>
                            <Text style={{color:color==1?'white':'gray'}}>{Strings.used}</Text>
                        </TouchableOpacity>
                        
                    </View>
                    </View>



                    <View style={{width:responsiveWidth(90), alignSelf:'center', marginTop:moderateScale(5)}}>
                <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.brand}</Text>
                    {
                        brandLoad?
                        <View style={{alignSelf:'center', marginTop:moderateScale(2), justifyContent:'center',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                            <ActivityIndicator />
                        </View>
                        :
                    <TouchableOpacity 
                     onPress={()=>this.setState({showBrandModal:true})}
                    style={{alignSelf:'center', marginTop:moderateScale(2),flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                         <FastImage resizeMode='center' source={{uri:brandImage}} style={{marginHorizontal:moderateScale(4), width:responsiveWidth(12),height:responsiveHeight(5)}} />   
                        <Text style={{ fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(2)}} >{brandText}</Text>
                        </View>
                        <View style={{marginHorizontal:moderateScale(4)}}>
                            <Icon name='caretdown' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(4)}} />
                        </View>
                    </TouchableOpacity>
                    }
                </View>

                <Modal
                 onRequestClose={()=>this.setState({showBrandModal:false})}
                     animationType="slide"
                     transparent={false}
                     visible={showBrandModal}
                    >
                        
                        <TouchableOpacity
                         onPress={()=>this.setState({showBrandModal:false})}
                         style={{alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10),marginBottom:moderateScale(5), marginHorizontal:moderateScale(10)}}>
                           <Icon name='close' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(11)}} />
                        </TouchableOpacity>
                        
                        
                        {brands.map(data=>(
                             <TouchableOpacity 
                             onPress={()=>{
                                this.setState({selectedBrand:data.id, showBrandModal:false,brandText:data.brandname,brandImage:data.img})
                                this.getModels(data.id)
                             }}
                            style={{height:responsiveHeight(10),flexDirection:isRTL?'row-reverse':'row',alignItems:'center', width:responsiveWidth(100),borderBottomColor:'#d7dade',borderBottomWidth:0.5}}>
                             <FastImage resizeMode='center' source={{uri:data.img}} style={{marginHorizontal:moderateScale(4), width:responsiveWidth(12),height:responsiveHeight(5)}} />   
                            <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(2)}} >{data.brandname}</Text>
                            </TouchableOpacity>
                        ))}
                       
                    </Modal>
                  
                  
                    <View style={{width:responsiveWidth(90), alignSelf:'center', marginTop:moderateScale(5)}}>
                <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.modal}</Text>
                    {
                        modalLoad?
                        <View style={{alignSelf:'center', marginTop:moderateScale(2), justifyContent:'center',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                            <ActivityIndicator />
                        </View>
                        :
                    <TouchableOpacity 
                     onPress={()=>this.setState({showModalModal:true})}
                    style={{alignSelf:'center', marginTop:moderateScale(2),flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >                        
                        <Text style={{ fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(4)}} >{modalText}</Text>     
                        <View style={{marginHorizontal:moderateScale(4)}}>
                            <Icon name='caretdown' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(4)}} />
                        </View>
                    </TouchableOpacity>
                    }
                </View>

                <Modal
                   onRequestClose={()=>this.setState({showModalModal:false})}
                     animationType="slide"
                     transparent={false}
                     visible={showModalModal}
                    >
                        
                        <TouchableOpacity
                         onPress={()=>this.setState({showModalModal:false})}
                         style={{alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10),marginBottom:moderateScale(5), marginHorizontal:moderateScale(10)}}>
                           <Icon name='close' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(11)}} />
                        </TouchableOpacity>
                        
                        
                        {modals.map(data=>(
                             <TouchableOpacity 
                             onPress={()=>this.setState({selectedModal:data.id, showModalModal:false,modalText:data.modelname})}
                            style={{height:responsiveHeight(10),flexDirection:isRTL?'row-reverse':'row',alignItems:'center', width:responsiveWidth(100),borderBottomColor:'#d7dade',borderBottomWidth:0.5}}>
                            <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(5)}} >{data.modelname}</Text>
                            </TouchableOpacity>
                        ))}
                       
                    </Modal> 
                  
                  
                  

                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.year}</Text>
                    {/*<View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={year}
                        onChangeText={(val)=>{this.setState({year:val})}}
                        placeholder={Strings.enterYear} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                        </View>*/}
                    <View style={{borderWidth:1, marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{width:responsiveWidth(85)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'black' }}
                        selectedValue={year}
                        onValueChange={(val,index)=>{
                            console.log("count VAL  ",val)
                            this.setState({year:val})
                        }}
                        >
                            {this.state.years.map((val,)=>{
                                console.log("YP  ",val)     
                                return(
                                <Picker.Item label={""+val} value={val} />
                                )
                            })}
                        </Picker>
                        </View>
                    </View>
                    {year.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>
                  
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.mileage}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={mileage}
                        onChangeText={(val)=>{this.setState({mileage:val})}}
                        placeholder={Strings.enterMileages} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {mileage.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>

                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.color}</Text>
                    <View style={{ flexWrap:'wrap', marginTop:moderateScale(3), flexDirection:isRTL?'row-reverse':'row',alignSelf:isRTL?'flex-end':'flex-start',flexWrap:'wrap'}}   >
                        {colores.map(c=>(
                         <ImageBackground                        
                         source={{uri:c.img}}
                         style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,marginBottom:moderateScale(5),elevation:5, marginHorizontal:moderateScale(5),width:40,height:40,borderRadius:20,justifyContent:'center',alignItems:'center'}}
                         imageStyle={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, elevation:5,width:40,height:40,borderRadius:20}}
                         >

                        <TouchableOpacity
                        onPress={()=>this.setState({color2:c.colorName,colorId:c.id})}
                         style={{elevation:5,width:40,height:40,borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                            {color2==c.colorName&&
                            <Icon name='check' type='Entypo' style={{fontSize:responsiveFontSize(4),color:c.colorName.toLowerCase()=='white'?'black':'white'}} />
                            }
                        </TouchableOpacity>
                        </ImageBackground>   
                        ))}
                                     

                    </View>
                    </View>
























                    
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.title}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={title}
                        onChangeText={(val)=>{this.setState({title:val})}}
                        placeholder={Strings.enterTitle} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {title.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>
              
                    <View style={{ marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.price}</Text>
                    <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center', marginTop:moderateScale(1),width:responsiveWidth(90)}}>
                    <Slider
                    minimumTrackTintColor='#679C8A'
                    maximumValue={100000}
                    thumbTintColor='#679C8A'
                    thumbStyle={{height:20,width:20}}
                    trackStyle={{color:'#679C8A',backgroundColor:'#EDEDED',height:responsiveHeight(2),borderRadius:moderateScale(5)}}
                    style={{width:responsiveWidth(50)}}
                    value={price>100000?100000:price}
                    onValueChange={(value) => {
                        console.log("steps  ",value); 
                        //console.log("steps  ",Math.floor(price*1000));
                        this.setState({price:Math.round(value)})
                    }}
                    />
                        <View style={{flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(35),height:responsiveHeight(8)}}>
                            <TextInput
                            keyboardType='phone-pad'
                            underlineColorAndroid='transparent'
                            onChangeText={(val)=>{
                                if(Number(val)<=1000000000){
                                    this.setState({price:Number(val)})
                                }
                            }}
                            value={""+price}
                             style={{height:responsiveHeight(8),width:responsiveWidth(25), color:'black'}}/>
                            <Text style={{marginHorizontal:moderateScale(2), color:'black'}}>{selectedCountry.currency}</Text>
                        </View>
                    </View>
                    
                    </View>
                    
                    <View style={{alignSelf:'center', flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-around',width:responsiveWidth(90),marginVertical:moderateScale(7)}} >
                    <View style={{flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(28),height:responsiveHeight(8)}}>
                            <TextInput

                            keyboardType='phone-pad'
                            underlineColorAndroid='transparent'
                            onChangeText={(val)=>{
                               this.setState({beds:Number(val)})
                            }}
                            value={""+beds}
                             style={{height:responsiveHeight(8),width:responsiveWidth(15), color:'black'}}/>
                            <Text style={{marginHorizontal:moderateScale(3),color:'black'}}>{Strings.bed}</Text>
                        </View>
                        <View style={{flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(28),height:responsiveHeight(8)}}>
                            <TextInput
                            
                            keyboardType='phone-pad'
                            underlineColorAndroid='transparent'
                            onChangeText={(val)=>{
                                this.setState({baths:Number(val)})
                            }}
                            value={""+baths}
                             style={{height:responsiveHeight(8),width:responsiveWidth(15), color:'black'}}/>
                            <Text style={{marginHorizontal:moderateScale(3),color:'black'}}>{Strings.baths}</Text>
                        </View>
                        <View style={{flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(28),height:responsiveHeight(8)}}>
                            <TextInput
                           
                            keyboardType='phone-pad'
                            underlineColorAndroid='transparent'
                            onChangeText={(val)=>{
                                this.setState({space:Number(val)})
                            }}
                            value={""+space}
                             style={{height:responsiveHeight(8),width:responsiveWidth(15), color:'black'}}/>
                            <Text style={{marginHorizontal:moderateScale(3), color:'black'}}>{Strings.space}</Text>
                        </View>
                    </View>
                    
                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.selectCountry}</Text>
                        <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        
                        
                        {countries.length>0&&
                        <FastImage style={{height:responsiveHeight(4),width:responsiveWidth(12), marginHorizontal:moderateScale(5)}} source={{uri:selectedCountry.img}}  />
                        }  
                        
                        <View style={{width:responsiveWidth(countries.length==0?88:68)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(countries.length==0?88:68), color:'black' }}
                        selectedValue={selectedCountry.id}
                        onValueChange={(val,index)=>{
                            console.log("count VAL  ",val)
                            this.setState({selectedCountry:countries[index]})
                            this.getCities(val,true)
                        }}
                        >
                            {countries.map((val,index)=>{
                               
                                return(
                                <Picker.Item label={isRTL?val.arabicName:val.countryName} value={val.id} />
                                )
                            })}
                        </Picker>
                        </View>

                        </View>

                    </View>

                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.selectedCity}</Text>
                        <View style={{borderWidth:1, marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{width:responsiveWidth(85)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'black' }}
                        selectedValue={selectedCity}
                        onValueChange={(val,index)=>{
                            console.log("count VAL  ",val)
                            this.setState({selectedCity:val})
                        }}
                        >
                            {cities.map((val,)=>{     
                                return(
                                <Picker.Item label={isRTL?val.arabicName:val.cityName} value={val.id} />
                                )
                            })}
                        </Picker>
                        </View>
                        </View>

                    </View>
       


                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.address}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(88),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                         value={selectedAddress}
                        onChangeText={(val)=>{this.setState({selectedAddress:val})}}
                        placeholder={Strings.enterYourAddress} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {selectedAddress.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>
                    
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.details}</Text>
                    <View style={{justifyContent:'flex-start', alignSelf:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                         value={details}
                        multiline={true}
                        
                        onChangeText={(val)=>{this.setState({details:val})}}
                        placeholder={Strings.enterYourDetails} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                        </View>
                        {details.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>
                    
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                         value={email}
                         keyboardType='email-address'
                        onChangeText={(val)=>{this.setState({email:val})}}
                        placeholder={Strings.enterYourEmail} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {email.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>

                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.phone}</Text>
                    <View style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <View style={{height:responsiveHeight(7.5), justifyContent:'center',alignItems:'center',borderRightWidth:!isRTL?0.9:0,borderLeftWidth:isRTL?0.9:0,borderColor:'#EDEDED',width:responsiveWidth(11)}}>
                            <Text style={{color:'gray'}}>{selectedCountry.countryCode}</Text>
                        </View>
                        <TextInput
                        value={phone}
                        keyboardType='phone-pad'
                        onChangeText={(val)=>{this.setState({phone:val})}}
                        placeholder={Strings.enterYourPhone} underlineColorAndroid='transparent' style={{width:responsiveWidth(75), marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {phone.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>   
                    <View style={{marginVertical:moderateScale(7),alignSelf:'center',height:responsiveHeight(40),width:responsiveWidth(90),marginTop:moderateScale(5)}}>
                        <MapView
                        style={{height:responsiveHeight(40),width:responsiveWidth(90),}}
                        region={{
                            latitude: this.state.lat,
                            longitude: this.state.lng,
                            latitudeDelta: 0.8,
                            longitudeDelta: 0.8,
                        }}
                        onPress={(coordinate)=>{
                            this.setState({
                                lat:coordinate.nativeEvent.coordinate.latitude,
                                lng:coordinate.nativeEvent.coordinate.longitude,
                            })
                        }}
                        showsCompass
                        showsIndoors
                        showsUserLocation
                        showsTraffic
                        zoomControlEnabled
                        >
                            <Marker
                            coordinate={{
                            latitude: this.state.lat,
                            longitude: this.state.lng,
                            latitudeDelta: 0.8,
                            longitudeDelta: 0.8,
                            }}
                            />
                        </MapView>
                    </View>

                    {/*
                    <View style={{marginVertical:moderateScale(7),alignSelf:'center', width:responsiveWidth(90),marginTop:moderateScale(5)}}>
                       <Text style={{fontSize:responsiveFontSize(2.6),color:'black', alignSelf:isRTL?'flex-end':'flex-start'}}>
                            {Strings.participants}
                       </Text>
                       <View style={{marginTop:moderateScale(5), width:responsiveWidth(90),justifyContent:'space-between',alignItems:'center', flexDirection:isRTL?'row-reverse':'row',alignItems:'center',alignSelf:isRTL?'flex-end':'flex-start'}}>
                           <View  style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row',}}>
                                <FastImage  source={require('../assets/imgs/profileicon.jpg')} style={{width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(1)}} />
                                <FastImage source={require('../assets/imgs/profileicon.jpg')} style={{width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(1)}} />
                                <FastImage source={require('../assets/imgs/profileicon.jpg')} style={{width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(1)}} />
                                <View style={{backgroundColor:'#679C8A', justifyContent:'center',alignItems:'center', width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(1)}}>
                                    <Text style={{color:'white'}} >+10</Text>
                                </View>
                           </View>

                           <TouchableOpacity
                           onPress={()=>{this.setState({showDialog:true})}} 
                            style={{backgroundColor:'#679C8A', justifyContent:'center',alignItems:'center', width:30,height:30,borderRadius:15,marginHorizontal:moderateScale(0.5)}}>
                               <Icon name='user-plus' type='FontAwesome5' style={{fontSize:responsiveFontSize(2.2), color:'white'}} />
                           </TouchableOpacity>
                           
                       
                    </View>
                    </View>
                    */}
                    <TouchableOpacity
                        
                        style={{marginBottom:moderateScale(20),  width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(2)}}
                        onPress={()=>{
                           
                        //const {images,  selectedGoal,selectedCountry,selectedAddress, participants} = this.state;
                        if(!email.replace(/\s/g, '').length){
                            this.setState({email:''})
                        }
                        if(!title.replace(/\s/g, '').length){
                            this.setState({title:''})
                        }
                        if(!phone.replace(/\s/g, '').length){
                            this.setState({phone:''})
                        }
                       

                        if(!selectedAddress.replace(/\s/g, '').length){
                            this.setState({selectedAddress:''})
                        }
                        
                        if(!details.replace(/\s/g, '').length){
                            this.setState({details:''})
                        }

                       
                        if(!mileage.replace(/\s/g, '').length){
                            this.setState({mileage:''})
                        }

                        if(images.length==0){
                            this.setState({images:[],imgFlag:true})
                        }

                        if(mileage.replace(/\s/g, '').length&&phone.replace(/\s/g, '').length&&images.length>0&&email.replace(/\s/g, '').length&&title.replace(/\s/g, '').length&&details.replace(/\s/g, '').length&&selectedAddress.replace(/\s/g, '').length){
                            var data = new FormData(); 
                            data.append("description",details)
                            data.append("price",price)
                            data.append("address",selectedAddress)
                            data.append("email",email)
                            data.append("lat",this.state.lat)
                            data.append("long",this.state.lng)
                            data.append("phone",selectedCountry.countryCode+phone)
                            data.append("title",title)
                            data.append("category",selectedCategory)
                            data.append("subCategory",selectedSubCategory)
                            data.append("country",selectedCountry.id)
                            //
                            data.append("usage",usage)
                            data.append("brand",brandText)
                            data.append("modal",modalText)
                            data.append("mileage",mileage)
                            data.append("color",colorId)
                            data.append("year",year)
                            //
                            //data.append("participate[]",9)
                            
                            const parts = this.state.participants;
                            console.log(parts)
                            if(parts.length>0){
                                
                          for(var i=0 ; i<parts.length; i++){
                                    data.append("participate[]",parts[i])
                                }
                            }
                            data.append("city",selectedCity)
                            if(this.state.selectImgFlag){
                            images.filter(img=>{                               
                                data.append('img',{
                                    uri: img,
                                    type: 'multipart/form-data',
                                    name: 'productImages'
                                    })                             
                            })
                        }
                            this.setState({load:true});
                            axios.put(`${BASE_END_POINT}ads/${this.props.data.id}`, data, {
                                headers: {
                                  'Content-Type': 'application/json',
                                  'Authorization': `Bearer ${this.props.currentUser.token}`
                                },
                              }).then(response=>{
                                  console.log('add Ads')
                                this.setState({load:false});
                                RNToasty.Success({title:Strings.adsUpdated})
                                navigator.resetTo({
                                    screen:'Home',
                                    animated: true,
                                    animationType:'slide-horizontal'
                                })
                              }).catch(error=>{
                                console.log('Error  ',error)
                                console.log('Error  ',error.reponse)
                                this.setState({load:false});
                                 if(error.response.status == 403){
                                    RNToasty.Error({title:Strings.accountDeleted})
                                }
                              })
                        }
                        
                        
                        
                        }}
                        >
                        <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.save}</Text>
                    </TouchableOpacity>


                </ScrollView>

                
                <Dialog
                width={responsiveWidth(90)}          
                visible={this.state.imgDialge}
                onTouchOutside={() => {
                this.setState({ imgDialge: false });
                }}
                >   
                <View style={{width:responsiveWidth(90),marginVertical:moderateScale(10)}}>
                
                <TouchableOpacity
                 onPress={()=>{
                     this.setState({imgDialge:false})
                     ImagePicker.openCamera({
                        width: 500,
                        height: 500,

                      }).then(image => {
                          
                        this.setState({selectImgFlag:true, imgFlag:false, images:[image.path] });
                      });
                }}
                 style={{width:responsiveWidth(90),borderBottomColor:'#679C8A',borderBottomWidth:1 ,height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(5)}}>
                    <Icon type='Entypo' name='camera' style={{fontSize:responsiveFontSize(8), color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}}>{Strings.camera}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>{
                    this.setState({imgDialge:false})
                    ImagePicker.openPicker({
                                
                        multiple: true,
                        waitAnimationEnd: false,
                        includeExif: true,
                        forceJpg: true,

                      }).then(images => {
                        this.setState({selectImgFlag:true, images: images.map(i =>i.path) });
                      });
                }}
                 style={{width:responsiveWidth(90),height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(7)}}>
                    <Icon type='MaterialIcons' name='photo-library' style={{fontSize:responsiveFontSize(8),color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}} >{Strings.gallery}</Text>
                </TouchableOpacity>

                
                </View>        
                </Dialog>

            <Dialog
            width={responsiveWidth(90)}
           
            visible={this.state.showDialog}
            onTouchOutside={() => {
            this.setState({ showDialog: false });
            }}
            >    
            <View style={{width:responsiveWidth(90)}}>
               <Text style={{alignSelf:'center', marginTop:moderateScale(15),fontSize:responsiveFontSize(2.5),color:'black'}} >{Strings.selectParticipants}</Text>
               <View style={{marginTop:moderateScale(15), borderRadius:moderateScale(4), width:responsiveWidth(80),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',borderWidth:1,borderColor:'#C6C6C6'}}>
                <TextInput 
                onChangeText={(val)=>{
                    console.log("ooo   ",val)
                    this.setState({val:val})
                    this.getUsers(val)
                }}
                value={this.state.val}
                 style={{width:responsiveWidth(70)}} />
                <Icon name='search' type='EvilIcons' style={{color:'#C6C6C6',fontSize:responsiveFontSize(3)}} />
               </View>

               <ScrollView style={{maxHeight:responsiveHeight(30), borderRadius:moderateScale(4), marginTop:moderateScale(0.5),backgroundColor:'white',elevation:2,width:responsiveWidth(80),alignSelf:'center'}} >
                {this.state.users.map(val=>(
                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({val:val.email,id:val.id,users:[]})
                    }}
                     style={{ borderBottomColor:'gray',borderBottomWidth:0.3, width:responsiveWidth(80),height:responsiveHeight(8),justifyContent:'center'}} >
                        <Text style={{marginHorizontal:moderateScale(5)}}>{val.email}</Text>
                    </TouchableOpacity>
                ))}
               </ScrollView>

                <TouchableOpacity
                onPress={()=>{
                   if(this.state.id){
                    this.setState({showDialog:false, participants:[...this.state.participants,this.state.id],id:null,val:'',users:[]})
                   }
                }}
                 style={{marginHorizontal:moderateScale(10),marginBottom:moderateScale(20), alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',width:responsiveWidth(30),height:responsiveHeight(8),borderRadius:moderateScale(4),backgroundColor:'#679C8A'}}>
                    <Text style={{color:'white',fontSize:responsiveFontSize(2.5)}}>{Strings.done}</Text>
                </TouchableOpacity>
            </View>        
            </Dialog>
     
              
            {this.state.load && <LoadingDialogOverlay title={Strings.wait}/>}
               
            </View>
            
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(UpdateCarAds);


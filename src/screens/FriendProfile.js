import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,TextInput,Modal,ScrollView,TouchableOpacity,Text,FlatList
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import LottieView from 'lottie-react-native';
import HomeAdsCard from '../components/HomeAdsCard'
import strings from '../assets/strings';




class FriendProfile extends Component {

    state = {
      load:false,
      unfollow:false,
      myAds:[],
      adsLoading:true,
      favDone:false,
      isFollow:false,
      disableFollowButton:false,
    }

    constructor(props){
        super(props);
        
    }

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#707D67',
    };

    follow = () => {
        console.log("ID   ",this.props.data.id)
        this.setState({disableFollowButton:true})
        axios.post(`${BASE_END_POINT}follow/${this.props.data.id}/follow`, {}, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
           console.log('Follow Done');
           this.setState({isFollow:true,disableFollowButton:false})
          RNToasty.Success({title:Strings.youFollowHimNow})
          const user = {...this.props.currentUser,user:{...response.data.myUser}}
          console.log('current user')
          console.log(user)
          this.props.getUser(user)
          AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
        }).catch(error => {
            this.setState({disableFollowButton:false})
            if(error.status==403){
                //RNToasty.Info({title:Strings.youFollowHimNow})
            }
            console.log('Follow error  ',error.status);
            console.log('Follow error  ',error.respone);
            console.log('Follow error  ',error);
            
          });
    }

    unFollow = () => {
        this.setState({disableFollowButton:true})
        axios.put(`${BASE_END_POINT}follow/${this.props.data.id}/unfollow`, {}, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
           console.log('un Follow Done');
           this.setState({isFollow:false,disableFollowButton:false})
           const user = {...this.props.currentUser,user:{...response.data.myuser}}
           console.log('current user')
           console.log(user)
           this.props.getUser(user)
           AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
          //RNToasty.Success({title:Strings.you})
        }).catch(error => {
            if(error.status==403){
                //RNToasty.Info({title:Strings.youFollowHimNow})
            }
            this.setState({disableFollowButton:false})
            console.log('unFollow error  ',error.status);
            console.log('unFollow error  ',error.respone);
            console.log('unFollow error  ',error);
            
          });
    }


    


        
    componentDidMount(){    
        this.disableDrawer();
        this.getAds();
        console.log('Friend Profile   ',this.props.data)
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.disableDrawer()
    }

   
    getAds = () => {
        let uri = `${BASE_END_POINT}ads?owner=${this.props.data.id}`
        axios.get(uri)
        .then(response => {
            console.log('ADS   ',response.data)
            this.setState({adsLoading:false,myAds:response.data.data})
            
        }).catch(error => {
            this.setState({adsLoading:false})
            console.log("error   ",error);
            console.log("error   ",error.response);
        })

    }

    renderAdsSection = () =>{
        const {currentUser,isRTL,navigator} = this.props;
        return(
            <View style={{alignSelf:'center',marginTop:moderateScale(5),width:responsiveWidth(95)}}>
                <View style={{alignSelf:'center', marginHorizontal:moderateScale(7),marginVertical:moderateScale(3), flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center',width:responsiveWidth(95)}}>
                    <Text style={{color:'black'}} >{Strings.ads}</Text>
                    <Text style={{color:'#D3D3D3',fontSize:responsiveFontSize(6)}} > </Text>
                </View>
                {this.state.myAds.length>0?
                <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal
                data={this.state.myAds}
                renderItem={({item})=>(
                    <HomeAdsCard onPress={()=>{
                        this.props.navigator.push({
                         screen: item.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                         animated:true,
                         animationType:'slide-horizontal',
                         passProps:{data:item}
                       })
                   }} navigator={this.props.navigator} data={item} />
                )}
                />
                :
                <View style={{width:responsiveWidth(90),height:responsiveHeight(20),justifyContent:'center',alignItems:'center'}}>
                    <Text>{Strings.noDataAtRecent}</Text>
                </View>
                }
            </View>
        )
    }
    

    render(){
        const {isRTL,navigator,data,currentUser} = this.props;
        const {unFollow} = this.state;      
        return(
            <View style={{ flex:1,backgroundColor:'white' }}>
                <FlatAppHeader navigator={navigator} title={data.username} />
                <ScrollView style={{marginBottom:moderateScale(10)}}>
                <View style={{alignSelf:'center',marginTop:moderateScale(8),justifyContent:"center",alignItems:'center'}}>
                    <FastImage style={{height:100,width:100,borderRadius:50}} source={data.img?{uri:data.img}:require('../assets/imgs/profileicon.jpg')} />
                </View>

                {this.props.currentUser&&
                <TouchableOpacity 
                disabled={this.state.disableFollowButton}
                disabled={this.state.disableFollowButton}
                        onPress={()=>{
                           if(currentUser){
                               if(currentUser.user.following.includes(data.id)){
                                console.log('exist follow')
                                this.unFollow()
                               }else{
                                console.log('not exist follow')
                                this.follow()
                               }
                            
                           }else{
                            navigator.push({
                                screen: 'Login',
                                animated: true,
                                animationType:'slide-horizontal'
                            })
                           }
                        }}
                         style={{alignSelf:'center',marginTop:moderateScale(10),marginBottom:moderateScale(15), backgroundColor:'#679C8A', height:responsiveHeight(7),width:responsiveWidth(23),borderRadius:moderateScale(4),justifyContent:'center',alignItems:'center'}}>
                      <Text style={{alignSelf:'center',textAlign:'center', color:'white',fontSize:responsiveFontSize(6)}}>{!currentUser?Strings.follow: currentUser.user.following.includes(data.id)?Strings.unFollow:this.state.isFollow? Strings.unFollow:Strings.follow}</Text>
                 </TouchableOpacity>
                }
                
                

                <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.name}</Text>
                    <View style={{backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8), justifyContent:'center'}}>
                        <TextInput 
                         editable={false}
                        value={data.username}
                         underlineColorAndroid='transparent' style={{textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                  
                </View>

                <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                    <View style={{backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8), justifyContent:'center'}}>
                        <TextInput
                        editable={false}
                        value={data.email}
                         underlineColorAndroid='transparent' style={{textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                   
                </View>

                <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.phone}</Text>
                    <View style={{backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8), justifyContent:'center'}}>
                        <TextInput
                        editable={false}
                        value={data.phone[0]?data.phone[0]:strings.noDataAtRecent}
                        underlineColorAndroid='transparent' style={{textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                </View>

                <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.country}</Text>
                    <View style={{backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8), justifyContent:'center'}}>
                        <TextInput
                        editable={false}
                        value={data.country?data.country:strings.noDataAtRecent}
                        underlineColorAndroid='transparent' style={{textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                </View>

                {this.state.adsLoading? 
                <LottieView
                style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                source={require('../assets/animations/smartGarbageLoading.json')}
                autoPlay
                loop
                />
                :
                this.renderAdsSection()
                }
              

                </ScrollView>

               
            {this.state.updateLoading && <LoadingDialogOverlay title={Strings.waitUpdateProfile}/>}
               
            </View>
            
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(FriendProfile);


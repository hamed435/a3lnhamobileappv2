import React,{Component} from 'react';
import {View,Text,TouchableOpacity,AsyncStorage,ScrollView,Modal} from 'react-native';
import {  moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import * as colors from '../assets/colors'
import AppInput from '../common/AppInput';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import AppText from '../common/AppText';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import {BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import LoadingOverlay from '../components/LoadingOverlay';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import strings from '../assets/strings';
import ImagePicker from 'react-native-image-crop-picker';
import {getUser} from '../actions/AuthActions';




const MyButton =  withPreventDoubleClick(Button);



class ChangeProfilePhoto extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#707D67',
    };

    state= {
        loading:false,
        showModal:false,
        img:null,
        updateLoading:false,
    }

  
    componentDidMount(){    
        this.disableDrawer(); 
    }

    componentDidUpdate(){
        this.disableDrawer()
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }


    renderImagesParts = () => {
        const {currentUser,isRTL,navigator} = this.props;
        
        return(
            <View style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1,alignSelf:'center',marginTop:moderateScale(10),width:responsiveWidth(90),elevation:2,backgroundColor:'white',borderRadius:moderateScale(3)}}>
                <TouchableOpacity 
                onPress={()=>{this.setState({showModal:true})}}
                style={{height:responsiveHeight(9),justifyContent:'center', borderBottomWidth:0.3,borderBottomColor:'#DBDBDB', width:responsiveWidth(90)}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(5)}} >{Strings.viewCurrentPhoto}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={()=>{
                    ImagePicker.openCamera({
                        width: 500,
                        height: 500,

                      }).then(image => {
                        this.setState({img:image.path});
                      });
                 }}
                 style={{height:responsiveHeight(9),justifyContent:'center',borderBottomWidth:0.3,borderBottomColor:'#DBDBDB',  width:responsiveWidth(90)}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(5)}} >{Strings.TakeNewPhoto}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={()=>{
                    ImagePicker.openPicker({
                        width: 300,
                        height: 400,
                        cropping: true
                      }).then(image => {
                        this.setState({img:image.path})
                      });
                 }}
                 style={{height:responsiveHeight(9),justifyContent:'center',  width:responsiveWidth(90)}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(5)}} >{Strings.chooseFromYourPhotoLibrary}</Text>
                </TouchableOpacity>
                

            </View>

        )

    }
    
    onUpdate = () => {
        const {currentUser} = this.props;
        var data = new FormData(); 
            data.append('username',currentUser.user.username);
            data.append('email',currentUser.user.email)
            data.append('phone',currentUser.user.phone[0])
            data.append('country',currentUser.user.country)
            data.append('img',{
                uri: this.state.img,
                type: 'multipart/form-data',
                name: 'productImages'
            })
    
       this.setState({updateLoading:true})
        axios.put(`${BASE_END_POINT}user/${this.props.currentUser.user.id}/updateInfo`, data, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
            console.log('update user profile done')      
            console.log(response.data)
            this.setState({updateLoading:false});           
            const user = {...this.props.currentUser,user:{...response.data.user}}
            console.log('current user')
            console.log(user)
            RNToasty.Success({title:Strings.imgUpdated})
            this.props.getUser(user)
            AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
            this.props.navigator.pop()
        }).catch(error => {
            console.log('error');
            console.log(error.response);
            console.log(error);
            this.setState({updateLoading:false});
             
            if (!error.response) {
            this.setState({noConnection:Strings.noConnection});
            } 
          });
    }

    render(){
        const { isRTL,navigator,currentUser } = this.props;
        const {showModal,img} = this.state
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader navigator={navigator} title={Strings.profileImage}/>
                <View style={{alignSelf:'center',marginTop:moderateScale(8),justifyContent:"center",alignItems:'center'}}>
                    <FastImage style={{height:150,width:150,borderRadius:75}} source={img?{uri:img}:currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.jpg')} />
                </View>
                {this.renderImagesParts()}
                <Modal
                onRequestClose={()=>{
                    this.setState({showModal:false})
                }}
                animationType="slide"
                transparent={false}
                visible={this.state.showModal}
                >
                    <TouchableOpacity
                    onPress={()=>{this.setState({showModal:false})}}
                    style={{alignSelf:'flex-end',margin:moderateScale(10)}}>
                    <Icon name='closecircle' type='AntDesign' />
                    </TouchableOpacity>
                    <FastImage style={{flex:1}} source={currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.jpg')} />
                </Modal>
                <TouchableOpacity
                style={{ width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(3)}}
                onPress={()=>{
                    if(img){
                        this.onUpdate();
                    }else{
                        RNToasty.Warn({title:Strings.noImgSelected})
                    }
                }}
                >
                    <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.save}</Text>
                </TouchableOpacity>
                {this.state.updateLoading&&<LoadingDialogOverlay title={Strings.wait}/>}
            </View>
        )
    }
}

const mapDispatchToProps = {
    getUser,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(ChangeProfilePhoto);


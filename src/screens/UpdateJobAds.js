import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,TextInput,Modal,ScrollView,TouchableOpacity,Text,FlatList,ActivityIndicator,ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import LottieView from 'lottie-react-native';
import Slider from 'react-native-slider'
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import DateTimePicker from "react-native-modal-datetime-picker";
import RangeSlider from 'rn-range-slider';
import MapView, {Marker} from 'react-native-maps';


class UpdateJobAds extends Component {

    state = {
        lat:this.props.data.location[0],
        lng:this.props.data.location[1],
        showDialog:false,
       countries:[],
       cities:[],
       categories:[],
       subCategories:[], 
       images:this.props.data.img, 
       selectedCategory:this.props.data.category.id,
       selectedSubCategory:this.props.data.subCategory.id,
       selectedGoal:0, 
       title:this.props.data.title,
       companyName: this.props.data.companyName,
       price:10,
       selectedCountry:this.props.data.country,
       selectedCity:this.props.data.city,
       selectedAddress:' ',
       details: this.props.data.description,
       email: this.props.data.email,
       phone: this.props.data.phone.substring(5),
       participants:this.props.data.participate?this.props.data.participate:[],
       updateLoading:false,
       load:false,
       imgFlag:false,
       index:0,
       participationName:'',
       participations:[],
       users:[],
       val:'',
       id:null,
       catLoad:false,
       subCatLoad:true,
       imgDialge:false,
       gmailDialog:false,
      participationsTypeDialog:false,
      gmailText:'',
      selectedJobType:this.props.data.jopType,
      showDate:false,
      date:this.props.data.date,
      jopRequire: this.props.data.jopRequirements,
      experience:this.props.data.experience,
      selectedJobPerioud: this.props.data.salarySystem,
      priceFrom:Number(this.props.data.priceFrom),
      priceTo:Number(this.props.data.priceTo),
      selectImgFlag:false,
      currency:this.props.data.country.currency,
      countryCode:this.props.data.country.countryCode,
      categoryModal:false,
      subCategoryModal:false,
      subCategoryText:this.props.isRTL?this.props.data.subCategory.arabicName:this.props.data.subCategory.name,
      personal:this.props.data.personal,
      color:this.props.data.personal?1:0,
    }

    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#707D67',
    };


        
    componentDidMount(){    
        this.disableDrawer();
        this.getSubCategories(this.props.data.category.id)         
        this.getCountries()
        this.getCities(this.props.data.country.id)
    }

    componentWillUnmount(){
       // this.props.removeItem()
      }

      componentDidUpdate(){
          this.disableDrawer()
      }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    getUsers(user) {   
        axios.put(`${BASE_END_POINT}searchUser`,JSON.stringify({search:user}),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
            .then(response => {
                this.setState({users:response.data})
                
                console.log('USer   ',response.data)
               
            }).catch(error => {
                console.log("user   ",error);
                console.log("users   ",error.response);         
            })   
    }

    getCategories() {
        this.setState({catLoad:true})   
        axios.get(`${BASE_END_POINT}categories`)
            .then(response => {
                this.setState({categories:response.data.data,selectedCategory:response.data.data[0].id,catLoad:false})
                console.log('Categories   ',response.data)
                this.getSubCategories(response.data.data[0].id)
            }).catch(error => {
                this.setState({catLoad:false})
                console.log("error   ",error);
                console.log("error   ",error.response);         
            })   
}

    getSubCategories(id) {  
        this.setState({subCatLoad:true}) 
        axios.get(`${BASE_END_POINT}categories/${id}/sub-categories`)
            .then(response => {
                if(response.data.data.length>0){
                this.setState({subCategories:response.data.data,subCatLoad:false,})
                }else{
                    this.setState({subCategories:response.data.data,subCatLoad:false,})
                }
                console.log('sub Categories   ',response.data)
            
            }).catch(error => {
                this.setState({subCatLoad:false}) 
                console.log("sub error   ",error);
                console.log("error   ",error.response);         
            })   
    }

    getCountries() {   
        axios.get(`${BASE_END_POINT}countries`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
            },
          })
            .then(response => {
                this.setState({countries:response.data.data})
                
                console.log('countries   ',response.data)
               // this.getCities(response.data.data[0].id)
            
            }).catch(error => {
                console.log("countries error   ",error);
                console.log("error   ",error.response);         
            })   
    }

    getCities(countryId,flag) {   
        axios.get(`${BASE_END_POINT}countries/${countryId}/cities`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
            },
          })
            .then(response => {
                console.log('Citirs   ',response.data.data)
                this.setState({cities:response.data.data})
                if(flag){
                    this.setState({selectedCity:response.data.data[0].id})
                }
            
            }).catch(error => {
                console.log("Citirs error   ",error);
                console.log("error   ",error.response);         
            })   
    }
    



    render(){
        const {currentUser,isRTL,navigator,data} = this.props;
        const {color,personal, subCategoryText, categoryModal,subCategoryModal,currency,countryCode,priceFrom,priceTo, selectedJobPerioud,experience,jopRequire,date,showDate, selectedJobType, companyName, imgDialge, catLoad,subCatLoad, index,imgFlag,selectedCity, images, countries,cities, categories,subCategories, selectedCategory,selectedSubCategory, selectedGoal, title,price,selectedCountry,selectedAddress,details,email,phone, participants} = this.state;
       
        return(
            <View style={{ flex:1,backgroundColor:'white' }}>
                <FlatAppHeader  navigator={navigator} title={Strings.updateAds} />
                <ScrollView>
                    {/* #CFCECE */}
               
                    <Text style={{marginHorizontal:moderateScale(10), marginTop:moderateScale(5), alignSelf:isRTL?'flex-end':'flex-start', color:'black',fontSize:responsiveFontSize(7)}}>{Strings.attachPhoto}</Text>
                    <View style={{flexWrap:'wrap', flexDirection:isRTL?'row-reverse':'row', marginVertical:moderateScale(5),width:responsiveWidth(96),alignSelf:'center'}}>
                                                                                   
                        <TouchableOpacity
                        onPress={()=>{this.setState({imgDialge:true})}} style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,marginTop:moderateScale(4), marginHorizontal:moderateScale(4), alignSelf:isRTL?'flex-end':'flex-start', width:responsiveWidth(27),height:responsiveHeight(18),justifyContent:'center',alignItems:'center',backgroundColor:'white',elevation:1,borderRadius:moderateScale(3)}}>
                            <Icon style={{fontSize:responsiveFontSize(8),color:'#CFCECE'}} name='plus' type='Entypo' />
                        </TouchableOpacity>
                        {images.length==0&&imgFlag&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.require}</Text>
                        }
                                   
                        {images.map((val,index)=>(
                        
                        <ImageBackground
                        source={{uri:val}}                      
                         style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,marginHorizontal:moderateScale(4),alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(2), width:responsiveWidth(25),height:responsiveHeight(18),justifyContent:'center',alignItems:'center',backgroundColor:'white',elevation:1,borderRadius:moderateScale(3)}}>
                            
                        </ImageBackground>
                        
                        ))
                    }
                    </View>
                  


                    <TouchableOpacity 
                     onPress={()=>this.setState({categoryModal:true})}
                    style={{alignSelf:'center', marginTop:moderateScale(7),flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(4)}} >{isRTL?data.category.arabicName:data.category.name}</Text>
                        <View style={{marginHorizontal:moderateScale(4)}}>
                            <Icon name='caretdown' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(4)}} />
                        </View>
                    </TouchableOpacity>
                    <Modal
                    onRequestClose={()=>this.setState({categoryModal:false})}
                     animationType="slide"
                     transparent={false}
                     visible={categoryModal}
                    >
                        <TouchableOpacity
                         onPress={()=>this.setState({categoryModal:false})}
                         style={{alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10),marginBottom:moderateScale(5), marginHorizontal:moderateScale(10)}}>
                            <Icon name='close' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(11)}} />
                        </TouchableOpacity>
                        <TouchableOpacity 
                         onPress={()=>this.setState({selectedCategory:data.id, categoryModal:false})}
                        style={{height:responsiveHeight(10),justifyContent:'center', width:responsiveWidth(100),borderBottomColor:'#d7dade',borderBottomWidth:0.5}}>
                        <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(6)}} >{isRTL?data.category.arabicName:data.category.name}</Text>
                        </TouchableOpacity>
                    </Modal>


                    {/*<View style={{alignSelf:'center', width:responsiveWidth(90), marginTop:moderateScale(5)}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.category}</Text>
                        <View style={{marginTop:moderateScale(2),justifyContent:'center',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{ width:responsiveWidth(85)}}>
                        {!catLoad? 
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'#CFCECE' }}
                        selectedValue={selectedCategory}
                        onValueChange={(val)=>{
                            console.log("CAT VAL  ",val)
                            this.setState({selectedCategory:val})
                            //this.getSubCategories(val)

                        }}
                        >
                            <Picker.Item label={isRTL?data.category.arabicName:data.category.name} value={data.category.id} />                 
                        
                        </Picker>
                        :
                        <ActivityIndicator size='small'  />
                        }
                       </View>
                    </View>
                       
                    </View>
                    */}

                    <TouchableOpacity 
                     onPress={()=>this.setState({subCategoryModal:true})}
                    style={{alignSelf:'center', marginTop:moderateScale(7),flexDirection:isRTL?'row-reverse':'row', justifyContent:'space-between',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(4)}} >{subCategoryText}</Text>
                        <View style={{marginHorizontal:moderateScale(4)}}>
                            <Icon name='caretdown' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(4)}} />
                        </View>
                    </TouchableOpacity>
                    <Modal
                     onRequestClose={()=>this.setState({subCategoryModal:false})}
                     animationType="slide"
                     transparent={false}
                     visible={subCategoryModal}
                    >
                        
                        <TouchableOpacity
                         onPress={()=>this.setState({subCategoryModal:false})}
                         style={{alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10),marginBottom:moderateScale(5), marginHorizontal:moderateScale(10)}}>
                            <Icon name='close' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(11)}} />
                        </TouchableOpacity>
                        
                        
                        {subCategories.map(data=>(
                             <TouchableOpacity 
                             onPress={()=>this.setState({subCategoryText:isRTL?data.arabicName:data.name, selectedSubCategory:data.id, subCategoryModal:false})}
                            style={{height:responsiveHeight(10),justifyContent:'center', width:responsiveWidth(100),borderBottomColor:'#d7dade',borderBottomWidth:0.5}}>
                            <Text style={{fontSize:responsiveFontSize(6),color:'black',marginHorizontal:moderateScale(6)}} >{isRTL?data.arabicName:data.name}</Text>
                            </TouchableOpacity>
                        ))}
                       
                    </Modal>



                   {/* <View style={{alignSelf:'center', width:responsiveWidth(90), marginTop:moderateScale(5)}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.subCategory}</Text>
                        <View style={{marginTop:moderateScale(2),justifyContent:'center',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{ width:responsiveWidth(85)}}>
                        {!subCatLoad?
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'#CFCECE' }}
                        selectedValue={selectedSubCategory}
                        onValueChange={(val)=>{
                            this.setState({selectedSubCategory:val})
                        }}
                        >
                           {subCategories.map(val=>(
                                <Picker.Item label={isRTL?val.arabicName:val.name} value={val.id} />
                           ))}
                        </Picker>
                        :
                        <ActivityIndicator size='small'  />
                        }
                       </View>
                    </View>
                       
                    </View>
                    */}
                    
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.jobTitle}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={title}
                        onChangeText={(val)=>{this.setState({title:val})}}
                        placeholder={Strings.enterTitle} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {title.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>

              
                    {personal==false&&
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.companyName}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={companyName}
                        onChangeText={(val)=>{this.setState({companyName:val})}}
                        placeholder={Strings.enterCompanyName} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {companyName.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>
                    }
                    
                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.date}</Text>
                        <TouchableOpacity
                        onPress={()=>{this.setState({showDate:true})}}
                         style={{marginTop:moderateScale(3), marginHorizontal:moderateScale(1),alignSelf:isRTL?'flex-end':'flex-start',borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'center',alignItems:'center',width:responsiveWidth(30),height:responsiveHeight(6)}}>
                            <Text style={{color:'black'}}>{date}</Text>
                        </TouchableOpacity>
                    </View>
                    <DateTimePicker
                    mode='date'
                    isVisible={this.state.showDate}
                    onConfirm={(val)=>{
                        const d = new Date(val).toISOString().substring(0,10);
                        console.log('Date   ',d)
                        
                        this.setState({date:d,showDate:false})
                    }}
                    onCancel={ ()=>{ this.setState({showDate:false}) } }
                    />
                    

                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.selectCountry}</Text>
                        <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        
                        
                        {countries.length>0&&
                        <FastImage style={{height:responsiveHeight(4),width:responsiveWidth(12), marginHorizontal:moderateScale(5)}} source={{uri:selectedCountry.img}}  />
                        }  
                        
                        <View style={{width:responsiveWidth(countries.length==0?88:68)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(countries.length==0?88:68), color:'black' }}
                        selectedValue={selectedCountry.id}
                        onValueChange={(val,index)=>{
                            console.log("count VAL  ",val)
                            this.setState({ selectedCountry:countries[index]})
                            this.getCities(val,true)
                        }}
                        >
                            {countries.map((val,index)=>{
                               
                                return(
                                <Picker.Item label={isRTL?val.arabicName:val.countryName} value={val.id} />
                                )
                            })}
                        </Picker>
                        </View>

                        </View>

                    </View>

                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.selectedCity}</Text>
                        <View style={{borderWidth:1, marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                        <View style={{width:responsiveWidth(85)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'black' }}
                        selectedValue={selectedCity}
                        onValueChange={(val,index)=>{
                            console.log("count VAL  ",val)
                            this.setState({selectedCity:val})
                        }}
                        >
                            {cities.map((val,)=>{     
                                return(
                                <Picker.Item label={isRTL?val.arabicName:val.cityName} value={val.id} />
                                )
                            })}
                        </Picker>
                        </View>
                        </View>

                    </View>
       

                    <View style={{ marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7)}}> {Strings.salary}</Text>
                    <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center', marginTop:moderateScale(1),width:responsiveWidth(90)}}>
                    
                    <RangeSlider
                        style={{width: responsiveWidth(90),height:20}}
                        gravity={'top'}
                        labelStyle='none'
                        min={500}
                        max={10000}
                        step={100}
                        selectionColor="#679C8A"
                        blankColor="#ccc"
                        initialLowValue={priceFrom}
                        initialHighValue={priceTo}
                        onValueChanged={(low, high, fromUser) => {
                            console.log("low  ",low,"      hight    ",high)
                            this.setState({priceFrom:low,priceTo:high})
                    }}/>
                    
                    
                    <View style={{borderWidth:1,marginHorizontal:moderateScale(3), width:responsiveWidth(26),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'black'}} >
                    <Picker
                        note
                        mode="dropdown"
                        style={{ color:'#CFCECE' }}
                        selectedValue={selectedJobPerioud}
                        onValueChange={(val)=>{
                            this.setState({selectedJobPerioud:val})
                        }}
                        >
                            <Picker.Item label={Strings.daily} value='day' />
                            <Picker.Item label={Strings.weekly} value='week' />
                            <Picker.Item label={Strings.monthly} value='month' />
                            <Picker.Item label={Strings.yearly} value='year' />
                    </Picker>
                    </View> 

                    </View>
                    
                    </View>



                    <View style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(90),alignSelf:'center'}} >
                        <Text style={{color:'black',}}>{Strings.from}</Text>
                         <View style={{marginHorizontal:moderateScale(3), flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(25),height:responsiveHeight(7.5)}}>
                            <Text style={{color:'black'}}>{priceFrom}</Text>
                            <Text style={{color:'black'}}>{selectedCountry.currency}</Text>
                        </View>
                        <Text  style={{color:'black',marginHorizontal:moderateScale(3)}}>{Strings.to}</Text>
                         <View style={{flexDirection:isRTL?'row-reverse':'row', borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED', backgroundColor:'white', elevation:0.2, justifyContent:'space-around',alignItems:'center',width:responsiveWidth(25),height:responsiveHeight(7.5)}}>
                            <Text style={{color:'black'}}>{priceTo}</Text>
                            <Text style={{color:'black'}}>{selectedCountry.currency}</Text>
                        </View>

                    </View>



                    <View style={{ marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7)}}> {Strings.salarySystem}</Text>
                    <View style={{marginTop:moderateScale(2), borderWidth:1,marginHorizontal:moderateScale(3), width:responsiveWidth(85),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'#CFCECE'}} >
                    <View style={{width:responsiveWidth(85)}} >
                    <Picker
                        note
                        mode="dropdown"
                        style={{width:responsiveWidth(85), marginHorizontal:moderateScale(2), color:'black' }}

                       selectedValue={selectedJobPerioud}
                        onValueChange={(val)=>{
                            this.setState({selectedJobPerioud:val})
                        }}
                        >
                            <Picker.Item label={Strings.daily} value='day' />
                            <Picker.Item label={Strings.weekly} value='week' />
                            <Picker.Item label={Strings.monthly} value='month' />
                            <Picker.Item label={Strings.yearly} value='year' />
                    </Picker>
                    </View>
                    </View> 
                    
                    </View>

                  
                    
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.jobDescription}</Text>
                    <View style={{justifyContent:'flex-start', alignSelf:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                        multiline={true}
                        value={details}
                        onChangeText={(val)=>{this.setState({details:val})}}
                        placeholder={Strings.enterDescription} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                        {details.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>

                    
                    {data.personal==false&&
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.jopRequire}</Text>
                    <View style={{justifyContent:'flex-start', alignSelf:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                        multiline={true}
                        value={jopRequire}
                        onChangeText={(val)=>{this.setState({jopRequire:val})}}
                        placeholder={Strings.enterRequirements} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                        {jopRequire.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>
                    }

                    <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                        <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.jobType}</Text>
                        <View style={{borderWidth:1, marginTop:moderateScale(2), width:responsiveWidth(90),height:responsiveHeight(8),borderRadius:moderateScale(4),borderWidth:0.5,borderColor:'black'}} >
                        <View style={{width:responsiveWidth(85)}}>
                        <Picker
                        note
                        mode="dropdown"
                        style={{ color:'#CFCECE' }}
                        selectedValue={selectedJobType}
                        onValueChange={(val)=>{
                            this.setState({selectedJobType:val})
                        }}
                        >
                            <Picker.Item label={Strings.fullTime} value='Full Time' />
                            <Picker.Item label={Strings.partTime} value='Part Time' />
                            <Picker.Item label={Strings.remotely} value='remotely' />
                        </Picker>
                        </View>
                        </View>

                    </View>


                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.experience}</Text>
                    <View style={{justifyContent:'flex-start', alignSelf:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                        multiline={true}
                        value={experience}
                        onChangeText={(val)=>{this.setState({experience:val})}}
                        placeholder={Strings.enterYourExperience} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                        {experience.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                        }
                    </View>

                    
                    <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                    <View style={{justifyContent:'center',backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={email}
                         keyboardType='email-address'
                        onChangeText={(val)=>{this.setState({email:val})}}
                        placeholder={Strings.enterYourEmail} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {email.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                    </View>

                   
                    
                    <View style={{marginVertical:moderateScale(7),alignSelf:'center',height:responsiveHeight(40),width:responsiveWidth(90),marginTop:moderateScale(5)}}>
                        <MapView
                        style={{height:responsiveHeight(40),width:responsiveWidth(90),}}
                        region={{
                            latitude: this.state.lat,
                            longitude: this.state.lng,
                            latitudeDelta: 0.8,
                            longitudeDelta: 0.8,
                        }}
                        onPress={(coordinate)=>{
                            this.setState({
                                lat:coordinate.nativeEvent.coordinate.latitude,
                                lng:coordinate.nativeEvent.coordinate.longitude,
                            })
                        }}
                        showsCompass
                        showsIndoors
                        showsUserLocation
                        showsTraffic
                        zoomControlEnabled
                        >
                            <Marker
                            coordinate={{
                            latitude: this.state.lat,
                            longitude: this.state.lng,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                            }}
                            />
                        </MapView>
                    </View>

                    <TouchableOpacity
                        
                        style={{marginBottom:moderateScale(20),  width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(2)}}
                        onPress={()=>{
                           
                        //const {images,  selectedGoal,selectedCountry,selectedAddress, participants} = this.state;
                        if(!title.replace(/\s/g, '').length){
                            this.setState({title:''})
                        }

                        if(personal==false){
                            if(!companyName.replace(/\s/g, '').length){
                                this.setState({companyName:''})
                            }
                        }

                        if(!details.replace(/\s/g, '').length){
                            this.setState({details:''})
                        }

                        if(personal==false){
                            if(!jopRequire.replace(/\s/g, '').length){
                                this.setState({jopRequire:''})
                            }
                        }
                        if(!experience.replace(/\s/g, '').length){
                            this.setState({experience:''})
                        }if(!email.replace(/\s/g, '').length){
                            this.setState({email:''})
                        }          
                        if(images.length==0){
                            this.setState({images:[],imgFlag:true})
                        }

                        if(personal==false){

                            if( images.length>0&& email.replace(/\s/g, '').length&&experience.replace(/\s/g, '').length&&details.replace(/\s/g, '').length&&jopRequire.replace(/\s/g, '').length&&companyName.replace(/\s/g, '').length&&title.replace(/\s/g, '').length){

                                var data = new FormData(); 
                                data.append("description",details)
                                if(this.state.selectImgFlag){
                                images.filter(img=>{                               
                                    data.append('img',{
                                        uri: img,
                                        type: 'multipart/form-data',
                                        name: 'productImages'
                                        })                             
                                })
                                }
    
                                data.append("address",'address')
                                data.append("email",email)
                                data.append("phone",selectedCountry.countryCode+this.props.currentUser.user.phone.length>0?this.props.currentUser.user.phone[0]:"")
                                //data.append("price",Math.floor(price*1000) )
                                data.append("category",selectedCategory)
                                data.append("subCategory",selectedSubCategory)
                                data.append("country",selectedCountry.id)
                                data.append("title",title)
                                data.append("lat",this.state.lat)
                                data.append("long",this.state.lng)
                                data.append("priceFrom",priceFrom)
                                data.append("priceTo",priceTo)
                                data.append("jopRequirements",jopRequire)
                                data.append("jopType",selectedJobType)
                                data.append("experience",experience)
                                data.append("salarySystem",selectedJobPerioud)
                                data.append("date",date)
                                data.append("companyName",companyName)
                                data.append("personal",personal)
                                data.append("city",selectedCity)
                               
                                //data.append("participate[]",9)
                                const parts = this.state.participants;
                                if(parts.length>0){
                                    
                                for(var i=0 ; i<parts.length; i++){
                                            data.append("participate[]",parts[i])
                                        }
                                    }
                                
                                
                                this.setState({load:true});
                                axios.put(`${BASE_END_POINT}ads/${this.props.data.id}`, data, {
                                    headers: {
                                      'Content-Type': 'application/json',
                                      'Authorization': `Bearer ${this.props.currentUser.token}`
                                    },
                                  }).then(response=>{
                                      console.log('add Ads')
                                      this.setState({load:false});
                                      RNToasty.Success({title:Strings.adsUpdated})
                                      navigator.resetTo({
                                        screen:'Home',
                                        animated: true,
                                        animationType:'slide-horizontal'
                                    })
                                  }).catch(error=>{
                                    console.log('Error  ',error)
                                    console.log('Error  ',error.reponse)
                                    this.setState({load:false});
                                     if(error.response.status == 403){
                                        RNToasty.Error({title:Strings.accountDeleted})
                                    }
                                  })
                            }

                        }else{
                            if( images.length>0&& email.replace(/\s/g, '').length&&experience.replace(/\s/g, '').length&&details.replace(/\s/g, '').length&&title.replace(/\s/g, '').length){

                                var data = new FormData(); 
                                data.append("description",details)
                                if(this.state.selectImgFlag){
                                images.filter(img=>{                               
                                    data.append('img',{
                                        uri: img,
                                        type: 'multipart/form-data',
                                        name: 'productImages'
                                        })                             
                                })
                                }
    
                                data.append("address",'address')
                                data.append("email",email)
                                data.append("phone",selectedCountry.countryCode+this.props.currentUser.user.phone[0])
                                //data.append("price",Math.floor(price*1000) )
                                data.append("category",selectedCategory)
                                data.append("subCategory",selectedSubCategory)
                                data.append("country",selectedCountry.id)
                                data.append("title",title)
                                data.append("lat",this.state.lat)
                                data.append("long",this.state.lng)
                                data.append("priceFrom",priceFrom)
                                data.append("priceTo",priceTo)
                                data.append("jopRequirements",'')
                                data.append("jopType",selectedJobType)
                                data.append("experience",experience)
                                data.append("salarySystem",selectedJobPerioud)
                                data.append("date",date)
                                data.append("companyName",'')
                                data.append("personal",personal)
                                data.append("city",selectedCity)
                               
                                //data.append("participate[]",9)
                                const parts = this.state.participants;
                                if(parts.length>0){
                                    
                                for(var i=0 ; i<parts.length; i++){
                                            data.append("participate[]",parts[i])
                                        }
                                    }
                                
                                
                                this.setState({load:true});
                                axios.put(`${BASE_END_POINT}ads/${this.props.data.id}`, data, {
                                    headers: {
                                      'Content-Type': 'application/json',
                                      'Authorization': `Bearer ${this.props.currentUser.token}`
                                    },
                                  }).then(response=>{
                                      console.log('add Ads')
                                      this.setState({load:false});
                                      RNToasty.Success({title:Strings.adsUpdated})
                                      navigator.resetTo({
                                        screen:'Home',
                                        animated: true,
                                        animationType:'slide-horizontal'
                                    })
                                  }).catch(error=>{
                                    console.log('Error  ',error)
                                    console.log('Error  ',error.reponse)
                                    this.setState({load:false});
                                     if(error.response.status == 403){
                                        RNToasty.Error({title:Strings.accountDeleted})
                                    }
                                  })
                            }
                        }

                        
                        
                        
                        
                        }}
                        >
                        <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.save}</Text>
                    </TouchableOpacity>


                </ScrollView>

                <Dialog
                width={responsiveWidth(90)}          
                visible={this.state.imgDialge}
                onTouchOutside={() => {
                this.setState({ imgDialge: false });
                }}
                >   
                <View style={{width:responsiveWidth(90),marginVertical:moderateScale(10)}}>
                
                <TouchableOpacity
                 onPress={()=>{
                     this.setState({imgDialge:false})
                     ImagePicker.openCamera({
                        width: 500,
                        height: 500,

                      }).then(image => {
                        this.setState({selectImgFlag:true, imgFlag:false, images:[image.path] });
                      });
                }}
                 style={{width:responsiveWidth(90),borderBottomColor:'#679C8A',borderBottomWidth:1 ,height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(5)}}>
                    <Icon type='Entypo' name='camera' style={{fontSize:responsiveFontSize(8), color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}}>{Strings.camera}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>{
                    this.setState({imgDialge:false})
                    ImagePicker.openPicker({
                                
                        multiple: true,
                        waitAnimationEnd: false,
                        includeExif: true,
                        forceJpg: true,

                      }).then(images => {
                        this.setState({selectImgFlag:true, images: images.map(i =>i.path) });
                      });
                }}
                 style={{width:responsiveWidth(90),height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(7)}}>
                    <Icon type='MaterialIcons' name='photo-library' style={{fontSize:responsiveFontSize(8),color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}} >{Strings.gallery}</Text>
                </TouchableOpacity>

                
                </View>        
                </Dialog>

                
                <Dialog
                width={responsiveWidth(90)}          
                visible={this.state.participationsTypeDialog}
                onTouchOutside={() => {
                this.setState({ participationsTypeDialog: false });
                }}
                >   
                <View style={{width:responsiveWidth(90),marginVertical:moderateScale(10)}}>
                
                <TouchableOpacity
                 onPress={()=>{
                     this.setState({participationsTypeDialog:false,showDialog:true})
                    
                }}
                 style={{width:responsiveWidth(90),borderBottomColor:'#679C8A',borderBottomWidth:1 ,height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(5)}}>
                    <FastImage resizeMode='center' style={{width:responsiveWidth(7),height:responsiveHeight(6)}} source={require('../assets/imgs/appLogo.png')}/>
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}}>{Strings.inApp}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>{
                    this.setState({participationsTypeDialog:false,gmailDialog:true})
                }}
                 style={{width:responsiveWidth(90),height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(7)}}>
                    <Icon type='MaterialCommunityIcons' name='gmail' style={{fontSize:responsiveFontSize(8),color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}} >{Strings.gmail}</Text>
                </TouchableOpacity>

                
                </View>        
                </Dialog>

            <Dialog
            width={responsiveWidth(90)}
           
            visible={this.state.showDialog}
            onTouchOutside={() => {
            this.setState({ showDialog: false });
            }}
            >    
            <View style={{width:responsiveWidth(90)}}>
               <Text style={{alignSelf:'center', marginTop:moderateScale(15),fontSize:responsiveFontSize(7.5),color:'black'}} >{Strings.selectParticipants}</Text>
               <View style={{marginTop:moderateScale(15), borderRadius:moderateScale(4), width:responsiveWidth(80),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',borderWidth:1,borderColor:'#C6C6C6'}}>
                <TextInput 
                onChangeText={(val)=>{
                    console.log("ooo   ",val)
                    this.setState({val:val})
                    this.getUsers(val)
                }}
                value={this.state.val}
                 style={{width:responsiveWidth(70)}} />
                <Icon name='search' type='EvilIcons' style={{color:'#C6C6C6',fontSize:responsiveFontSize(3)}} />
               </View>

               <ScrollView style={{maxHeight:responsiveHeight(30), borderRadius:moderateScale(4), marginTop:moderateScale(0.5),backgroundColor:'white',elevation:2,width:responsiveWidth(80),alignSelf:'center'}} >
                {this.state.users.map(val=>(
                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({val:val.email,id:val.id,users:[]})
                    }}
                     style={{ borderBottomColor:'gray',borderBottomWidth:0.3, width:responsiveWidth(80),height:responsiveHeight(8),justifyContent:'center'}} >
                        <Text style={{marginHorizontal:moderateScale(5)}}>{val.email}</Text>
                    </TouchableOpacity>
                ))}
               </ScrollView>

                <TouchableOpacity
                onPress={()=>{
                   if(this.state.id){
                    this.setState({showDialog:false, participants:[...this.state.participants,this.state.id],id:null,val:'',users:[]})
                   }
                }}
                 style={{marginHorizontal:moderateScale(10),marginBottom:moderateScale(20), alignSelf:isRTL?'flex-start':'flex-end', marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',width:responsiveWidth(30),height:responsiveHeight(8),borderRadius:moderateScale(4),backgroundColor:'#679C8A'}}>
                    <Text style={{color:'white',fontSize:responsiveFontSize(7)}}>{Strings.done}</Text>
                </TouchableOpacity>
            </View>        
            </Dialog>
     
            <Dialog
            width={responsiveWidth(90)}
           
            visible={this.state.gmailDialog}
            onTouchOutside={() => {
            this.setState({ gmailDialog: false });
            }}
            >    
            <View style={{width:responsiveWidth(90)}}>
               <Text style={{alignSelf:'center', marginTop:moderateScale(15),fontSize:responsiveFontSize(7.5),color:'black'}} >{Strings.shareToGmail}</Text>
               <View style={{marginTop:moderateScale(15), borderRadius:moderateScale(4), width:responsiveWidth(80),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-around',alignItems:'center',borderWidth:1,borderColor:'#C6C6C6'}}>
                <TextInput 
                onChangeText={(val)=>{
                    this.setState({gmailText:val})
                }}
                value={this.state.gmailText}
                 style={{width:responsiveWidth(70)}} />
                <Icon name='search' type='EvilIcons' style={{color:'#C6C6C6',fontSize:responsiveFontSize(3)}} />
               </View>

    

                <TouchableOpacity
                onPress={()=>{
                   if(this.state.gmailText.replace(/\s/g, '').length&&
                   this.state.gmailText.replace(/\s/g, '').includes('@')&&
                   this.state.gmailText.replace(/\s/g, '').includes('.com'))
                   {
                       const data = {
                        email:this.state.gmailText,
                        message:"https//www.google.com"
                       }
                       axios.post(`${BASE_END_POINT}ads/share`,JSON.stringify(data),{
                        headers: {
                          'Content-Type': 'application/json',
                          'Authorization': `Bearer ${this.props.currentUser.token}`
                        },
                      })
                       .then(response=>{
                            RNToasty.Success({title:Strings.gmaiShare})
                            this.setState({gmailText:'',gmailDialog:false})
                       })
                       .catch(error=>{
                           console.log('error  ',error.response)
                       })
                   }else{
                    RNToasty.Error({title:Strings.enterCorrectEmail})
                   }
                }}
                 style={{alignSelf:'center', marginHorizontal:moderateScale(10),marginBottom:moderateScale(20),  marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',width:responsiveWidth(30),height:responsiveHeight(8),borderRadius:moderateScale(4),backgroundColor:'#679C8A'}}>
                    <Text style={{color:'white',fontSize:responsiveFontSize(7)}}>{Strings.done}</Text>
                </TouchableOpacity>
            </View>        
            </Dialog>
              
            {this.state.load && <LoadingDialogOverlay title={Strings.wait}/>}
               
            </View>
            
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})



export default connect(mapToStateProps,mapDispatchToProps)(UpdateJobAds);


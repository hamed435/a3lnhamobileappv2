import React,{Component} from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
     View,TouchableOpacity,NetInfo,ScrollView,FlatList,Text,Platform,AppState,
     Image,ImageBackground,RefreshControl,TextInput,BackHandler ,Alert,Modal,TouchableWithoutFeedback
} from 'react-native';
import {  moderateScale, responsiveFontSize, responsiveWidth, responsiveHeight } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import {Badge,Icon } from 'native-base';
//import Icon from 'react-native-vector-icons/FontAwesome5';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import {getUnreadNotificationsNumers,showDeleteNotificationDialog} from '../actions/NotificationAction'; 
import {userLocation} from '../actions/OrderAction';
import {selectMenu,removeItem} from '../actions/MenuActions';
import {getUnseenMessages} from '../actions/ChatAction'
import Swiper from "react-native-swiper";
import NotificationCard from '../components/NotificationCard'
import CategoryCard from '../components/CategoryCard' 
import HomeAdsCard from '../components/HomeAdsCard' 
import ListFooter from '../components/ListFooter';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import * as Animatable from 'react-native-animatable';

export let homeNavigator = null


let that;
const CAT_IDEX = [0,1,2,6,7,8,12,13,14,18,19,20,24,25,26,30,31,32,36,37,38,42,43,44,48,49,50]
class Home extends Component {
     that = this;
     swiper=null;
     backPressed=0;
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
        
    };

    page=1; 
    page2=1;   
    page3=1;
    list=[]
    state = { 
        noConnection:null,
        currentPage:1,
        showCircleMenu:false,
        hideHomeButton: false,
        errorText:null,
        
        categories: new DataProvider(),
        catRefresh:false,
        catLoading:true,
        catFooter:false,
        catPages:null,

        notifications: new DataProvider(),
        notiRefresh:false,
        notiLoading:true,
        notiFooter:false,
        notiPages:null,

        //ads: new DataProvider(),
        ads:[],
        adsRfresh:false,
        adsLoading:true,
        adsFooter:false,
        adsPages:null,
        adsSearch: new DataProvider(),
        searchResults:[],
        searchText:'',
        showSearchDialog:false,
        localSearchResultsL:[]

    };

    constructor(props) {
        super(props);  
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.catRenderLayoutProvider = new LayoutProvider(
            () => 1,
            (type, dim) => {
              dim.width = responsiveWidth(33);
              dim.height = responsiveHeight(26);
            },
          );

          this.notiRenderLayoutProvider = new LayoutProvider(
            () => 1,
            (type, dim) => {
              dim.width = responsiveWidth(100);
              dim.height = responsiveHeight(20.5);
            },
          );

          this.adsRenderLayoutProvider = new LayoutProvider(
            () => 2,
            (type, dim) => {
              dim.width = responsiveWidth(50);
              dim.height = responsiveHeight(35);
            },
          );

        console.log('Color   ',this.props.color)
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                //this.getAds(1,false)
                this.getCategories(1,false)
                if(this.props.currentUser){
                    //this.getNotifications(1,false)
                    this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
                }
                
              //this.props.getUnseenMessages(this.props.currentUser.token)
              
            }else{
                this.setState({noConnection:'Strings.noConnection'})
            }
          });
    }

    onNavigatorEvent(event) {
        switch (event.id) {
          case 'willAppear':
              //Alert.alert('will')
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
            break;
          case 'willDisappear':
              //Alert.alert('Dis')
            this.backPressed = this.backPressed+1;
            this.backHandler.remove();
            break;
          default:
            break;
        }
    }

    handleBackPress = () => {   
        
        if(this.state.showCircleMenu){
            this.setState({showCircleMenu:false})
            return true;
        }else{
            if(this.state.showSearchDialog){
                this.setState({showSearchDialog:false})
                return true;
            }else{
                
                Alert.alert(
                    '',
                    `${strings.areYouSureExis}`,
                    [
                      {
                        text: strings.cancel,
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      {text: strings.ok, onPress: () =>{
                        BackHandler.exitApp()
                        //this.props.navigator.pop()
                    
                      }
                    },
                    ],
                    {cancelable: true},
                  );
                  return true;
            }
        }
        
    }
      
    
    

    getLocalSearchResults=async ()=>{
        const data = await AsyncStorage.getItem('@search')
        console.log('search res  ',data)
        if(data){
            this.setState({localSearchResultsL:JSON.parse(data)})
            console.log('search res2  ',data)
        }
        
    }

    componentDidMount(){
        //Alert.alert("hi")
        
        this.enableDrawer();
        homeNavigator=this.props.navigator;
        
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null}) 
                    //this.getAds(1,true) 
                    this.getCategories(1,true) 
                    if(this.props.currentUser){
                        //this.getNotifications(1,true)
                        this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
                    }
                                   
                     
                     
                }
            }
          );
            
         // AsyncStorage.removeItem('@search')
          this.getLocalSearchResults()

 
        }
 

    renderCatRow = (type, data, row) => {
        return (
       <View style={{marginTop:moderateScale(1), justifyContent:'center',alignItems:'center'}}>
          <CategoryCard navigator={this.props.navigator} color={CAT_IDEX.includes(row)?'#679C8A':'#C7BE75'} row={row} data={data} />
       </View> 
        );
    }
    renderCatFooter = () => {
        return (
          this.state.catFooter ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
    }

    renderAdsRow = (type, data, row) => {
        return (
       <View style={{marginTop:moderateScale(1), justifyContent:'center',alignItems:'center'}}>
          <HomeAdsCard onPress={()=>{
               this.props.navigator.push({
                screen: data.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                animated:true,
                animationType:'slide-horizontal',
                passProps:{data:data}
              })
          }} navigator={this.props.navigator} data={data} />
       </View> 
        );
    }

    renderAdsFooter = () => {
        return (
          this.state.adsFooter ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
    }


    renderNotiRow = (type, data, row) => {
        return (
       <View style={{marginTop:moderateScale(0), justifyContent:'center',alignItems:'center'}}>
          <NotificationCard
          data={data}
          navigator={this.props.navigator}
           />
       </View> 
        );
    }

    renderNotiFooter = () => {
        return (
            //this.state.notiLoading
            this.state.notiFooter ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
    }

    getCategories(page, refresh) {
        //this.setState({loading:true})
        let uri = `${BASE_END_POINT}categories?page=${page}&limit=20`
        if (refresh) {
            this.setState({catLoading: false, catRefresh: true})
        } else {
            this.setState({catRefresh:false,catFooter:true})
        }
        axios.get(uri)
            .then(response => {
                this.setState({
                    catLoading:false,
                    catRefresh:false,
                    catPages:response.data.pageCount,
                    errorText:null,
                    catFooter:false,
                    categories: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.catRefresh ? [...response.data.data] : [...this.state.categories.getAllData(), ...response.data.data]),
                })
                console.log('Categories   ',response.data)
                
            }).catch(error => {
                this.setState({catLoading:false,catRefresh:false})
                console.log("error   ",error);
                console.log("error   ",error.response);
                if (!error.response) {
                    this.setState({errorText:Strings.noConnection})
                }
            })
    
}


    getAds(page, refresh) {
     console.log("CURRENT COUNTRY   ",this.props.currentCountry.id)
    let uri = `${BASE_END_POINT}ads?onlyMe=false&active=true&country=${this.props.currentCountry.id}&page=${page}&limit=20`
    if (refresh) {
        this.setState({adsLoading: false, adsRfresh: true})
    } else {
        this.setState({adsRfresh:false,adsFooter:true})
    }
    axios.get(uri)
        .then(response => {
            console.log('ADS   ',response.data)
            this.setState({
                ads:refresh?response.data.data:[...this.state.ads,...response.data.data],
                adsLoading:false,
                adsRfresh:false,
                adsPages:response.data.pageCount,
                errorText:null,
                adsFooter:false,
                //ads: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.adsRfresh ? [...response.data.data] : [...this.state.ads.getAllData(), ...response.data.data]),
            })
            console.log('ADS   ',response.data)
            
        }).catch(error => {
            //Alert.alert("error  ",error.response)
            this.setState({adsLoading:false,adsRfresh:false})
            console.log("error   ",error);
            console.log("error ads  ",error.response);
            if (!error.response) {
                this.setState({errorText:Strings.noConnection})
            }
        })

}



    getNotifications(page, refresh) {
        //this.setState({loading:true})
        let uri = `${BASE_END_POINT}notif?page=${page}&limit=20`
        if (refresh) {
            this.setState({notiLoading: false, notiRefresh: true})
        } else {
            this.setState({notiRefresh:false,notiFooter:true})
        }
        axios.get(uri, {
            headers: {
            'Content-Type': 'application/json',
            //token
            'Authorization': `Bearer ${this.props.currentUser.token}`  
            },
        })
            .then(response => {
                this.setState({
                    //currentPage:0,
                    notiLoading:false,
                    notiRefresh:false,
                    notiPages:response.data.pageCount,
                    errorText:null,
                    notiFooter:false,
                   // notifications: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.notiRefresh ? [1,2,3,4,5,6,7,8,9,10,1] : [...this.state.notifications.getAllData(),1,2,3,4,5,6,7,8,9,10,1]),

                    notifications: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.notiRefresh ? [...response.data.data] : [...this.state.notifications.getAllData(), ...response.data.data]),
                })
                console.log('Notififcations   ',response.data)
                
            }).catch(error => {
                this.setState({notiLoading:false,notiRefresh:false})
                console.log("error   ",error);
                console.log("error   ",error.response);
                if (!error.response) {
                    this.setState({errorText:Strings.noConnection})
                }
            })

    }
  
  
    
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

componentDidUpdate(){
        this.enableDrawer()
    }    

    
    
    

    componentWillUnmount(){
      this.props.removeItem()
    }

    getSearchResults(val) {
        //this.setState({loading:true})
        let uri = `${BASE_END_POINT}ads/search/ads?search=${val}&country=${this.props.currentCountry.id}&onlyMe=false&active=true`
        axios.get(uri)
            .then(response => {
                this.setState({showSearchDialog:true, searchResults:response.data.data})
                console.log('search   ',response.data.data)
                /*if(response.data.data.length==0){
                    RNToasty.Info({title:Strings.notResults})
                    this.setState({showSearchDialog:false,})
                }*/
            }).catch(error => {              
                console.log("error   ",error);
                console.log("error   ",error.response);            
            }) 
    }

    header = () => {
        const {isRTL,currentUser,navigator} = this.props
        return(
            <ImageBackground 
            imageStyle={{ borderBottomLeftRadius:moderateScale(10),borderBottomRightRadius:moderateScale(10)}}
            source={require('../assets/imgs/header.png')}
            style={{borderBottomLeftRadius:moderateScale(10),borderBottomRightRadius:moderateScale(10),height:responsiveHeight(11),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                <TouchableOpacity
                onPress={()=>{
                    navigator.toggleDrawer({
                    side: this.props.isRTL ? 'right' : 'left',
                    })
                }}
                style={{ marginHorizontal:moderateScale(5)}}
                >
                    <Icon style={{color:'white',fontSize:responsiveFontSize(12)}} type='MaterialCommunityIcons' name='sort-variant' />
                </TouchableOpacity>
                <View style={{justifyContent:'space-between',alignItems:'flex-end', flexDirection:isRTL?'row-reverse':'row',borderBottomColor:'white',borderBottomWidth:0.5, width:responsiveWidth(77),marginHorizontal:moderateScale(0)}}>
                    <TextInput 
                    
                    onFocus={()=>{
                        this.setState({showSearchDialog:true})
                        
                    }}
                    value={this.state.searchText}
                    onChangeText={(searcValue)=>{
                        this.setState({searchText:searcValue})
                        if(searcValue.replace(/\s/g, '').length){ 
                                    
                        this.getSearchResults(searcValue)
                        }else{
                            this.setState({searchResults:[]})  
                        }
                        
                        /*
                        if(this.state.currentPage==1){
                        console.log('searcValue   ',searcValue)
                        this.list = this.state.ads._data.filter(val=>{
                            console.log('inFilter   ')
                            if(
                                val.description.toLowerCase().includes(searcValue.toLowerCase()) || 
                                val.title.toLowerCase().includes(searcValue.toLowerCase()) ||
                                val.category.name.toLowerCase().includes(searcValue.toLowerCase()) ||
                                val.category.arabicName.toLowerCase().includes(searcValue.toLowerCase()) ||
                                val.subCategory.arabicName.toLowerCase().includes(searcValue.toLowerCase()) ||
                                val.subCategory.arabicName.toLowerCase().includes(searcValue.toLowerCase()) 
                              ){
                                console.log('catch   ')
                                return val;
                              }
                        })
                        this.setState({ 
                            searchText:searcValue,
                            currentPage:1,            
                            adsSearch: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.list)
                        })
                    }
                    */
                    }}
                    //onBlur={()=>this.setState({showSearchDialog:false})}
                    value={this.state.searchText}
                    placeholderTextColor='white'
                    placeholder={strings.search}
                    underlineColorAndroid='transparent'
                     style={{padding:0, color:'white',width:responsiveWidth(67)}} 
                     />
                     {this.state.showSearchDialog?
                     <TouchableOpacity onPress={()=>{
                         this.setState({showSearchDialog:false,searchText:''})
                     }} >
                         <Icon name='close' type='AntDesign' style={{marginBottom:moderateScale(3), color:'white',fontSize:responsiveFontSize(8)}} />
                     </TouchableOpacity>
                     :
                     <Icon name='search' type='EvilIcons' style={{marginBottom:moderateScale(3), color:'white',fontSize:responsiveFontSize(8)}} />
                     }
                     
                </View>
            </ImageBackground>
        )
    }

    circleMenu = () => {
        const {navigator,currentUser} = this.props
        const { hideHomeButton } = this.state;
        return(
            <Animatable.View duration={1000} animation='slideInUp' >
               {!this.state.showCircleMenu&&
                currentUser?
                <TouchableOpacity
                onPress={()=>{

                    this.setState({showSearchDialog:false, showCircleMenu:true,hideHomeButton:true})
                }}
                    style={{marginBottom:moderateScale(-9),marginRight:moderateScale(4), backgroundColor:'#679C8A', width:50,height:50,borderRadius:25,justifyContent:'center',alignItems:'center'}}>
                    {/*<Icon name='plus' type='Entypo' style={{color:'white'}} />*/}
                    <Image style={{width:25,height:25}} source={require('../assets/imgs/appLogo2.png')} />
               </TouchableOpacity>
               :
               !hideHomeButton&&
               <TouchableOpacity
               onPress={()=>{
                   navigator.push({
                       screen:'Login',
                       animated: true,
                       animationType:'slide-horizontal'
                   })
               }}
                style={{marginBottom:moderateScale(-8),marginRight:moderateScale(4), backgroundColor:'#679C8A', width:50,height:50,borderRadius:25,justifyContent:'center',alignItems:'center'}}>
                   <Image style={{width:25,height:25}} source={require('../assets/imgs/appLogo2.png')} />
               </TouchableOpacity>
               }
               {this.state.showCircleMenu &&
               <Animatable.View duration={800} animation='slideInUp'  style={{justifyContent:'space-around',alignItems:'flex-end', flexDirection:'row', elevation:5, width:responsiveWidth(97),alignSelf:'center',height:responsiveHeight(30),borderTopRightRadius:moderateScale(90),borderTopLeftRadius:moderateScale(90),backgroundColor:'white', shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.25, shadowRadius:25}}>
                    
                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({showCircleMenu:false})
                        this.props.selectMenu('ADSTYPES')
                        this.props.navigator.push({
                            screen: 'Complaints',
                            animated: true,
                            animationType:'slide-horizontal'
                        })
                    }} 
                     style={{marginBottom:moderateScale(10),marginLeft:moderateScale(10)}}>
                        <Icon name='shield-off' type='Feather' style={{fontSize:responsiveFontSize(10), color:'#679C8A'}} />
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({showCircleMenu:false})
                        this.props.selectMenu('ADSTYPES')
                        this.props.navigator.push({
                            screen: 'AdsTypes',
                            animated: true,
                            animationType:'slide-horizontal'
                        })
                    }} 
                     style={{marginBottom:moderateScale(25),flexDirection:'row',}}>
                        {/*<Icon name='edit' type='AntDesign' style={{fontSize:responsiveFontSize(11), color:'#679C8A'}} /> */}

                        <View style={{flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
                            <Icon name='ad' type='FontAwesome5' style={{fontSize:responsiveFontSize(11), color:'#679C8A'}} />
                            <Text style={{paddingRight:moderateScale(2),marginLeft:moderateScale(-1), fontSize:responsiveFontSize(6),fontWeight:'bold',color:'white',backgroundColor:'#679C8A',}} >+</Text>
                        </View> 
                        
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({showCircleMenu:false})
                        this.props.selectMenu('MYADS')
                        this.props.navigator.push({
                            screen: 'Adds',
                            animated: true,
                            animationType:'slide-horizontal'
                        })
                    }}
                     style={{marginBottom:moderateScale(40),}}>
                         
                         {/*<Text style={{color:'white',backgroundColor:'#679C8A',padding:3,borderRadius:4}} >My Ads</Text>*/}
                        <Icon name='ad' type='FontAwesome5' style={{fontSize:responsiveFontSize(11), color:'#679C8A'}} /> 
                    </TouchableOpacity>
                    
                    <TouchableOpacity 
                    onPress={()=>{
                        this.setState({showCircleMenu:false,hideHomeButton:true})
                    }}
                    style={{marginBottom:moderateScale(55), backgroundColor:'#679C8A', width:40,height:40,borderRadius:20,justifyContent:'center',alignItems:'center',}}>
                        <Icon name='close' type='AntDesign' style={{color:'white'}} />
                    </TouchableOpacity>



                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({showCircleMenu:false})
                        this.props.selectMenu('MYFAV')
                        this.props.navigator.push({
                            screen: 'MyFavoriutes',
                            animated: true,
                            animationType:'slide-horizontal'
                        })
                    }}
                     style={{marginBottom:moderateScale(40),}}>
                        <Icon name='heart' type='AntDesign' style={{fontSize:responsiveFontSize(11),color:'#679C8A'}} />
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                     onPress={()=>{
                        this.setState({showCircleMenu:false})
                        this.props.selectMenu('MYFOLLOWERS')
                        this.props.navigator.push({
                            screen: 'MyFollowers',
                            animated: true,
                            animationType:'slide-horizontal'
                        })
                    }}
                     style={{marginBottom:moderateScale(22),}}>
                        <Icon name='eye' type='Entypo' style={{fontSize:responsiveFontSize(11),color:'#679C8A'}} />
                    </TouchableOpacity>

                    <TouchableOpacity
                     onPress={()=>{
                        this.setState({showCircleMenu:false})
                        this.props.selectMenu('MYFOLLOWERS')
                        this.props.navigator.push({
                            screen: 'Suggesting',
                            animated: true,
                            animationType:'slide-horizontal'
                        })
                    }}
                     style={{marginBottom:moderateScale(6),marginRight:moderateScale(13)}}>
                        <Icon name='direction' type='Entypo' style={{fontSize:responsiveFontSize(11),color:'#679C8A'}} />
                    </TouchableOpacity>

               </Animatable.View>
               }
           </Animatable.View>
        )
    }

    bottomNavigation = () => {
        const {isRTL,currentUser,navigator} = this.props
        const {currentPage} = this.state;
        return(
            <View style={{justifyContent:'center',alignItems:'center', width:responsiveWidth(100),position:'absolute',bottom:0,right:0,left:0}}>    
            {this.circleMenu()}
            {!this.state.showCircleMenu&&    
            <ImageBackground 
            resizeMode='cover'
            source={require('../assets/imgs/bottom.png')}
            style={{flexDirection:isRTL?'row-reverse':'row', height:responsiveHeight(8),justifyContent:'space-between',alignItems:'center', width:responsiveWidth(108)}}>
            
            {currentUser?
            <TouchableOpacity
            onPress={()=>{
                if(this.state.currentPage!=0){
                    this.setState({notiLoading:true,showSearchDialog:false,currentPage:0})
                    this.page2=1
                    this.getNotifications(1,true)
                }
            }}
             style={{marginHorizontal:moderateScale(20)}}>
                 {this.props.unReaded>0&&
                  <Badge danger style={{backgroundColor:'red', elevation:2, height:20, justifyContent:'center',alignItems:'center'}}>
                        <Text style={{alignSelf:'center', fontSize:10, color:'white'}}>{this.props.unReaded}</Text> 
                    </Badge>
                      }
                <Icon name='bell-o' type='FontAwesome' style={{marginTop:moderateScale(-2), color:currentPage==0?'yellow':'white',fontSize:responsiveFontSize(8)}} />         
                </TouchableOpacity>
            :
            <TouchableOpacity
            onPress={()=>{
                navigator.push({
                    screen:"Login",
                    animated: true,
                    animationType:'slide-horizontal'
                })
            }}
             style={{marginHorizontal:moderateScale(20)}}>
                <Icon name='user-alt' type='FontAwesome5' style={{color:currentPage==0?'yellow':'white',fontSize:responsiveFontSize(8)}} />
            </TouchableOpacity>
            }

            <TouchableOpacity
            onPress={()=>{
                if(this.state.currentPage!=2){
                    console.log("ADS PAGE ")
                    this.setState({ads:new DataProvider(),adsLoading:true, currentPage:2,showSearchDialog:false,})
                    this.page3=1
                    this.getAds(1,false)
                }
                
            }}
             style={{marginHorizontal:moderateScale(20)}}>
                <Icon name='view-dashboard' type='MaterialCommunityIcons' style={{color:currentPage==2?'yellow':'white',fontSize:responsiveFontSize(8)}} />
            </TouchableOpacity>
           

            </ImageBackground>
            }
            </View>
        )
    }

    
    categoriesPage = () => {
        return(
            this.state.catLoading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.state.categories._data?
                <View style={{marginBottom:moderateScale(25),flex:1}} >
                
                <Text style={{fontFamily:'RobotoBold',marginHorizontal:moderateScale(6), color:'black',fontSize:responsiveFontSize(7),marginBottom:moderateScale(4),marginTop:moderateScale(7)}} >{strings.categories}</Text>

                {/*<Text style={{fontFamily:'RobotoBold', color:'black',fontSize:responsiveFontSize(8),marginVertical:moderateScale(4),alignSelf:'center'}} >{strings.categories}</Text> */}
                <RecyclerListView  
                 extendedState={this.state}     
                layoutProvider={this.catRenderLayoutProvider}
                dataProvider={this.state.categories}
                rowRenderer={this.renderCatRow}
                renderFooter={this.renderCatFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.state.catPages){
                        this.page++;
                        this.getCategories(this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.catRefresh}
                    onRefresh={() => {
                      this.page = 1
                      this.getCategories(this.page, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}    
                />
                </View>
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    {/*<Icon name='exclefile1' type='AntDesign'  style={{color:'black', fontSize:responsiveFontSize(6),marginBottom:moderateScale(2)}} />*/}
                    <Text style={{fontFamily:'Roboto-Regular',color:'black'}}>{strings.noDataAtRecent} </Text>
                </View>
            
        )
    }

    adsPage = () => {
        console.log('adsPage')
        return(
               this.state.adsLoading?
               <View style={{ flex:1}}> 
               <LottieView
               style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
               source={require('../assets/animations/smartGarbageLoading.json')}
               autoPlay
               loop
               />
                <LottieView
               style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
               source={require('../assets/animations/smartGarbageLoading.json')}
               autoPlay
               loop
               />
                <LottieView
               style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
               source={require('../assets/animations/smartGarbageLoading.json')}
               autoPlay
               loop
               />
           </View>
                :
                this.state.ads.length>0?
                <View style={{marginTop:moderateScale(0), marginBottom:moderateScale(30),flex:1}} >
                <Text style={{fontFamily:'RobotoBold',marginHorizontal:moderateScale(6), color:'black',fontSize:responsiveFontSize(7),marginBottom:moderateScale(4),marginTop:moderateScale(7)}} >{strings.currentAds}</Text>
                <FlatList
                numColumns={2}
                data={this.state.ads}
                renderItem={({item})=>(
                    <HomeAdsCard onPress={()=>{
                        this.props.navigator.push({
                         screen: item.category.type=='JOPS'?'JobAdsDescription':'AdsDescription',
                         animated:true,
                         animationType:'slide-horizontal',
                         passProps:{data:item}
                       })
                   }} navigator={this.props.navigator} data={item} />
                )}
                onEndReached={() => {
                   
                    if(this.page3 <= this.state.adsPages){
                        this.page3++;
                        this.getAds(this.page3, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.catRefresh}
                    onRefresh={() => {
                      this.page3 = 1
                      this.getAds(this.page3, true);
                    }
                    } 
                />}
                  onEndReachedThreshold={.5}    
                 />
                </View>
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Text style={{fontFamily:'Roboto-Regular',color:'black'}}>{strings.noDataAtRecent} </Text>
                </View>
            
        )
    }


    notificationsPage = () => {
        console.log("anwer noti   ",this.state.notifications)
        return(
            this.state.notiLoading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                this.state.notifications._data.length>0?
                <View style={{marginBottom:moderateScale(25),flex:1}} >
                <RecyclerListView
                extendedState={this.state}             
                layoutProvider={this.notiRenderLayoutProvider}
                dataProvider={this.state.notifications}
                rowRenderer={this.renderNotiRow}
                renderFooter={this.renderNotiFooter}
                onEndReached={() => {
                   
                    if(this.page2 <= this.state.notiPages){
                        this.page2++;
                        this.getNotifications(this.page2, false);
                    }
                    //this.getNotifications(this.page2, false);
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.catRefresh}
                    onRefresh={() => {
                      this.page2 = 1
                      this.getNotifications(this.page2, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}    
                />
                </View>
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    
                    <Text style={{fontFamily:'Roboto-Regular',color:'black'}}>{strings.noDataAtRecent} </Text>
                </View>
            
        )
    }

    deleteNotification = (notID) => {
        console.log(notID)
      axios.delete(`${BASE_END_POINT}notif/${notID}/delete`,{
          headers: {
            'Content-Type': 'application/json',
            //this.props.currentUser.token
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
      }).then(Response=>{
          console.log("notification is deleted")
          console.log(Response)
          //this.setState({delete:true})
          
          this.page2 = 1
        this.getNotifications(1, true);
        //this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
      }).catch(error=>{
          console.log(error.response)
      })
  }

  searchDialog = () =>{
      const {isRTL,navigator} = this.props
      return(
          
        <TouchableOpacity
        onPress={()=>{this.setState({showSearchDialog:false})}}
         style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1, position:'absolute',zIndex:3000,elevation:4,top:responsiveHeight(10),width:responsiveWidth(100),height:responsiveHeight(80),backgroundColor:'rgba(0,0,0,0.1)'}}>
        <ScrollView  style={{alignSelf:'center',borderRadius:moderateScale(0), backgroundColor:'white', width:responsiveWidth(80),maxHeight:responsiveHeight(35)}} >
          
             <Text style={{marginVertical:moderateScale(5), fontSize:responsiveFontSize(6), marginHorizontal:moderateScale(4), alignSelf:isRTL?'flex-end':'flex-start',color:'gray'}} >{Strings.recentResults}</Text>
            {
            this.state.searchResults.length>0?
            this.state.searchResults.map(val=>(
            <TouchableOpacity
            
            onPress={()=>{
                navigator.push({
                    screen:'HomeSearchResults',
                    animated:true,
                    animationType:'slide-horizontal',
                    passProps:{data:this.state.searchResults}
                })
                this.setState({showSearchDialog:false})
                if(!this.state.localSearchResultsL.includes(val.title)){
                    const r = [...this.state.localSearchResultsL,val.title]
                    AsyncStorage.setItem('@search',JSON.stringify(r))
                }
                
                //console.log('rrr   ',r)
                
            }}
             style={{marginVertical:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(77),alignSelf:'center'}}>
                <View  style={{alignSelf:isRTL?'flex-end':'flex-start'}}>
                <Icon name='search' type='EvilIcons' style={{color:'black',fontSize:responsiveFontSize(10)}} />
                </View>
                <Text style={{fontSize:responsiveFontSize(7), marginHorizontal:moderateScale(4), alignSelf:isRTL?'flex-end':'flex-start',color:'black'}} >{val.title}</Text>
            </TouchableOpacity>
            ))
            :
            <View style={{width:responsiveWidth(80),marginTop:moderateScale(4), justifyContent:'center',alignItems:'center'}} >
                <Text style={{fontFamily:'Roboto-Regular',color:'gray'}}>{strings.noDataAtRecent} </Text>
            </View>
            }

            <View style={{marginVertical:moderateScale(5),marginBottom:moderateScale(10), borderWidth:0.3,borderColor:'#d7dade'}} />
            <Text style={{marginVertical:moderateScale(5), fontSize:responsiveFontSize(6), marginHorizontal:moderateScale(4), alignSelf:isRTL?'flex-end':'flex-start',color:'gray'}} >{Strings.oldResult}</Text>
            {this.state.localSearchResultsL.length>0?
                this.state.localSearchResultsL.map(val=>(
                <TouchableOpacity
                onPress={()=>{
                    this.setState({searchText:val})
                    this.getSearchResults(val)
                }}
                 style={{marginVertical:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(77),alignSelf:'center'}}>
                    <View style={{alignSelf:isRTL?'flex-end':'flex-start'}}>
                    <Icon name='staro' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(10)}} />
                    </View>
                    <Text style={{fontSize:responsiveFontSize(7), marginHorizontal:moderateScale(4), alignSelf:isRTL?'flex-end':'flex-start',color:'black'}} >{val}</Text>
                </TouchableOpacity>
            ))
            :
            <View style={{width:responsiveWidth(80),marginVertical:moderateScale(4), justifyContent:'center',alignItems:'center'}} >
                <Text style={{fontFamily:'Roboto-Regular',color:'gray'}}>{strings.noDataAtRecent} </Text>
            </View>
            }
           
        </ScrollView>
        </TouchableOpacity>
      )
  }
    
    render(){
        const {currentUser,unSeen,isRTL} = this.props;
        const {showSearchDialog, currentPage} = this.state;
        console.log('rerender   ',this.list)
        return(
            <ImageBackground source={require('../assets/imgs/b.png')} style={{ flex:1,backgroundColor:'white'}}>
               {this.header()}

               {showSearchDialog&&this.searchDialog()}
               
               {currentUser&&currentPage==0&&
               <View style={{flex:1}} >
                {this.notificationsPage()}
                <Dialog
               width={responsiveWidth(70)}
              
               visible={this.props.showDeleteDialog}
               onTouchOutside={() => {
               this.props.showDeleteNotificationDialog(0,false)
               }}
               >    
               <View style={{width:responsiveWidth(70)}}>
                    <View style={{alignSelf:'center',marginTop:moderateScale(10)}}>
                    <Icon name='delete' type='MaterialCommunityIcons' style={{color:'red',fontSize:responsiveFontSize(10)}} />
                    </View>
                    <Text style={{textAlign:'center',width:responsiveWidth(60), fontSize:responsiveFontSize(6), alignSelf:'center',color:'black',marginTop:moderateScale(3)}} >{strings.areYouShure}</Text>
                    <View style={{alignSelf:'center',width:responsiveWidth(60),justifyContent:'space-between',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',marginVertical:moderateScale(10)}} >
                       <TouchableOpacity 
                       onPress={()=>{
                        this.props.showDeleteNotificationDialog(0,false)
                    }}
                       >
                       <Text style={{fontSize:responsiveFontSize(7), color:'gray'}}>{strings.cancel}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        onPress={()=>{
                            this.deleteNotification(this.props.id)
                            this.props.showDeleteNotificationDialog(0,false)
                        }}
                        >
                        <Text style={{fontSize:responsiveFontSize(7), color:'red'}}>{strings.remove}</Text>
                        </TouchableOpacity>
                       
                    </View>
               </View>        
               </Dialog>
                </View>           
                }

               {currentPage==1&&this.categoriesPage()}
               {currentPage==2&&this.adsPage()}
               
               {this.bottomNavigation()}
              
            </ImageBackground>
        )
    }
}


const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,  
    unReaded: state.noti.unReaded, 
    barColor: state.lang.color,
    unSeen: state.chat.unSeen,
    showDeleteDialog: state.noti.showDeleteDialog,
    id: state.noti.id,
    currentCountry:state.auth.currentCountry,

})

const mapDispatchToProps = {
    selectMenu,
    userLocation,
    getUnreadNotificationsNumers,
    removeItem,
    getUnseenMessages,
    showDeleteNotificationDialog
    
}

export default connect(mapToStateProps,mapDispatchToProps)(Home);
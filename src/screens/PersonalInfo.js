import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,TextInput,Modal,ScrollView,TouchableOpacity,Text,FlatList
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import FlatAppHeader from '../common/FlatAppHeader'
import LottieView from 'lottie-react-native';




class PersonalInfo extends Component {

    state = {
       name: this.props.currentUser.user.username,
       email: this.props.currentUser.user.email,
       phone:this.props.currentUser.user.phone[0]?this.props.currentUser.user.phone[0]:'',
       password: ' ',
       updateLoading:false,

    }

    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#707D67',
    };


        
    componentDidMount(){    
        this.disableDrawer();
    }

    componentDidUpdate(){
        this.disableDrawer()
    }

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    
    onUpdate = (name,email,phone,country) => {
        var data = new FormData(); 
            data.append('username',name);
            data.append('email',email)
            data.append('phone',phone)
            data.append('country',country)
            //data.append('countryId',5)
    
       this.setState({updateLoading:true})
        axios.put(`${BASE_END_POINT}user/${this.props.currentUser.user.id}/updateInfo`, data, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
            console.log('update user profile done')      
            console.log(response.data)
            this.setState({updateLoading:false});           
            const user = {...this.props.currentUser,user:{...response.data.user}}
            console.log('current user')
            console.log(user)
            RNToasty.Success({title:Strings.updateProfileSuccess})
            this.props.getUser(user)
            AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
            this.props.navigator.pop()
        }).catch(error => {
            console.log('error');
            console.log(error.response);
            console.log(error);
            this.setState({updateLoading:false});
             
            if (!error.response) {
            this.setState({noConnection:Strings.noConnection});
            } 
          });
    }

   
  

    render(){
        const {currentUser,isRTL,navigator} = this.props;
        const {name, email, phone,password} = this.state;
       
        return(
            <View style={{ flex:1,backgroundColor:'white' }}>
                <FlatAppHeader navigator={navigator} title={Strings.personalInformation} />
                <ScrollView >
                <View style={{alignSelf:'center',marginTop:moderateScale(8),justifyContent:"center",alignItems:'center'}}>
                    <FastImage style={{height:150,width:150,borderRadius:75}} source={currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.jpg')} />
                </View>

                <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.name}</Text>
                    <View style={{justifyContent:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput 
                        value={name}
                        onChangeText={(val)=>{this.setState({name:val})}}
                        placeholder={Strings.enterYourName} underlineColorAndroid='transparent' style={{textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {name.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

                <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                    <View style={{justifyContent:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={email}
                        keyboardType='email-address'
                        onChangeText={(val)=>{this.setState({email:val})}}
                        placeholder={Strings.enterYourEmail} underlineColorAndroid='transparent' style={{textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {email.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

                
                {/*<View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                <Text style={{color:'black',fontSize:responsiveFontSize(2.5),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.password}</Text>
                <View style={{backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                    <TextInput
                    secureTextEntry
                    onChangeText={(val)=>{this.setState({password:val})}}
                    placeholder={Strings.password} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                </View>
                {password.length==0&&
                <Text style={{color:'red',fontSize:responsiveFontSize(2.5),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
                </View>
            */}

                <View style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.phone}</Text>
                    <View style={{justifyContent:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput
                        value={phone}
                        keyboardType='phone-pad'
                        onChangeText={(val)=>{this.setState({phone:val})}}
                        placeholder={Strings.enterYourPhone} underlineColorAndroid='transparent' style={{textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {phone.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>

              <TouchableOpacity
                style={{ width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginVertical:moderateScale(12),borderRadius:moderateScale(3)}}
                onPress={()=>{
                if(!phone.replace(/\s/g, '').length){
                    this.setState({phone:''})
                }
                if(!name.replace(/\s/g, '').length){
                    this.setState({name:''})
                }
                if(!email.replace(/\s/g, '').length){
                    this.setState({email:''})
                }
                

                if(phone.replace(/\s/g, '').length&&name.replace(/\s/g, '').length&&email.replace(/\s/g, '').length){
                this.onUpdate(name,email,phone,currentUser.user.country);
                }
     
        }}
        >
          <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.save}</Text>
        </TouchableOpacity>


                </ScrollView>
              
            {this.state.updateLoading && <LoadingDialogOverlay title={Strings.waitUpdateProfile}/>}
               
            </View>
            
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(PersonalInfo);


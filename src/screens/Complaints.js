import React,{Component} from 'react';
import {View,ScrollView,Text,ActivityIndicator,TouchableOpacity,TextInput} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';
import {Icon} from 'native-base'
import {removeItem} from '../actions/MenuActions';
import FlatAppHeader from '../common/FlatAppHeader';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'


class Complaints extends Component {

    state={
        load:false,
        details:' ',
        problemType:' ',
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#A1C7C1',
    };
   
     componentWillUnmount(){
        this.props.removeItem()
    }
   

    componentDidMount(){
        this.enableDrawer() 
          
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

  

   

    render(){
        const {navigator,isRTL,data} = this.props;
        const {details,problemType} = this.state
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <FlatAppHeader menu navigator={navigator} title={Strings.complaints} />
               

                <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.problemType}</Text>
                    <View style={{justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2,shadowOffset: { height: 1,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput 
                        onChangeText={(val)=>{this.setState({problemType:val})}}
                        placeholder={Strings.problemType} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(0),width:responsiveWidth(86), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                    </View>
                    {problemType.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>



                <View style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.complaints}</Text>
                    <View style={{justifyContent:'flex-start', alignSelf:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(6), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(30)}}>
                        <TextInput
                        multiline={true}
                        
                        onChangeText={(val)=>{this.setState({details:val})}}
                        placeholder={Strings.enterYourDetails} underlineColorAndroid='transparent' style={{textAlignVertical:'top', height:responsiveHeight(30), marginHorizontal:moderateScale(2), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {details.length==0&&
                        <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </View>


                    <TouchableOpacity
                        style={{marginBottom:moderateScale(20),  width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(2)}}
                        onPress={()=>{
                            if(!details.replace(/\s/g, '').length){
                                this.setState({details:''})
                            }
                            if(!problemType.replace(/\s/g, '').length){
                                this.setState({problemType:''})
                            }

                            if(details.replace(/\s/g, '').length&&problemType.replace(/\s/g, '').length){
                                var complaint ={
                                    problemType:problemType,
                                    description:details,
                                }
                                if(data){
                                    complaint['ads']=data.id
                                }
                                this.setState({load:true})
                                axios.post(`${BASE_END_POINT}problem`,JSON.stringify(complaint), {
                                    headers: {
                                      'Content-Type': 'application/json',
                                      'Authorization': `Bearer ${this.props.currentUser.token}`
                                    },
                                  }).then(response=>{
                                      console.log('add Ads')
                                    this.setState({load:false});
                                    RNToasty.Success({title:Strings.yourComplaintsSend})
                                    navigator.resetTo({
                                        screen:'Home',
                                        animated: true,
                                        animationType:'slide-horizontal'
                                    })
                                  }).catch(error=>{
                                    console.log('Error  ',error)
                                    console.log('Error  ',error.reponse)
                                    this.setState({load:false});
                                    if(error.response.status == 505){
                                        RNToasty.Error({title:Strings.youAreBlockedToReport})
                                    }
                                  })
                            }
                        }}
                        >
                        <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.send}</Text>
                    </TouchableOpacity>

                    {this.state.load && <LoadingDialogOverlay title={Strings.wait}/>}
            </View>   
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,  
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(Complaints);

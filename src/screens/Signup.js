import React, { Component } from 'react';
import {
  View,TouchableOpacity,Text,ScrollView,Image,TextInput,Modal,ActivityIndicator,ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage  from '@react-native-community/async-storage'
import { Button,Item, Picker, Label,Icon} from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import Strings from  '../assets/strings';
import { Field, reduxForm } from "redux-form"
import Checkbox from 'react-native-custom-checkbox';
import { signup,getUser } from '../actions/AuthActions';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {removeItem} from '../actions/MenuActions';
import { RNToasty } from 'react-native-toasty';
import FlatAppHeader from  '../common/FlatAppHeader'
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import FastImage from 'react-native-fast-image';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import CodeInput from 'react-native-confirmation-code-input';
//import countries from '../assets/countries';
import Swiper from "react-native-swiper";
const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);
import CountDown from 'react-native-countdown-component';




class Signup extends Component {
  swiper=null;
  static navigatorStyle = {
    navBarHidden: true,
    statusBarColor: '#707D67',
  };

  state={
    name:' ',
    nameError: Strings.require,
    email: ' ',
    emailError:Strings.require,
    password: ' ',
    passwordError:Strings.require,
    confirmPassword: ' ',
    phone: ' ',
    phoneError:Strings.require,
    country: ' ',
    
    openModal: false,
    agreeTerms:false,
    countries:[],
    countryLoad:true,
    selectedCountry:'',
    countryImage:'',
    countryText:'',

    hidePassword:true,
    confirmHidePassword: true,

    code:null,
    currentPage:this.props.pageNumber?1:0,

    signupLoading:false,
    verivectionLoading:false,
    user:this.props.pageNumber?this.props.currentUser:null,
    
  }

  signupProcess = (user) => {
    this.setState({signupLoading:true})
    axios.post(`${BASE_END_POINT}signup`, JSON.stringify(user), {
      headers: {
        'Content-Type': 'application/json',
        //'Content-Type': 'multipart/form-data',
      },
    }).then(response => {
      RNToasty.Success({title:Strings.addUserSuccessfuly})
      //AsyncStorage.setItem('@QsathaUser', JSON.stringify(response.data));  
      console.log(response.data);
      console.log('done'); 
      this.setState({user:response.data, currentPage:1,signupLoading:false})
      //this.swiper.scrollBy(2); 
      //dispatch({ type: SIGNUP_SUCCESS,payload:response.data});  
     /* navigator.resetTo({
        screen:'Home'
      })  */          
    })
      .catch(error => {
        console.log(error.response);
        this.setState({signupLoading:false})
        if (!error) {       
          RNToasty.Error({title:Strings.errorExist})
        }else if (error.response.status == 422) {         
          RNToasty.Error({title:Strings.thisEmailAlreadyExist})
        }else{
          RNToasty.Error({title:Strings.errorExist})
        }
      });
  }

  
  sendCode(code) {
    console.log("email   ",this.state.user.user.email)
    let body = { 
        email: this.state.user.user.email.toLowerCase(),
        verifycode: code
     }
     this.setState({ verivectionLoading: true, })
    axios.post(`${BASE_END_POINT}confirm-code`, JSON.stringify(body), {
        headers: { 'Content-Type': 'application/json', },
    }).then(response => {
      AsyncStorage.setItem('@QsathaUser', JSON.stringify(this.state.user));   
      this.setState({ verivectionLoading: false })
      this.props.getUser(this.state.user)
      this.props.navigator.resetTo({
        screen:'Home',
        animated:true,
        animationType:'slide-horizontal'
      })
    }).catch(error => {
        this.setState({ verivectionLoading: false })
        if (error.request.status === 400) {              
            RNToasty.Error({title:Strings.correctCode})
        }
        
        console.log('code error')
        console.log(error)
        console.log(error.response)
    });
}
  _onFulfill(code) {
      this.sendCode(code);
  }

  componentDidMount() {
    this.disableDrawer();
    this.getCountries()
  }

  getCountries() {   
    axios.get(`${BASE_END_POINT}countries`, {
        headers: {
          'Content-Type': 'application/json',
          //'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYm9vZHJDYXIiLCJpYXQiOjE1NjgzNDQyMDg0MzEsImV4cCI6MTU2ODM3NTc0NDQzMX0.mOo6770mjitsTKK4JzrbSB2OB5cR7dtyfB8LcwpP7V0`
        },
      })
        .then(response => {
          //this.props.currentCountry
          this.setState({countryLoad:false, countries:response.data.data,selectedCountry:this.props.currentCountry.id,countryImage:this.props.currentCountry.img,countryText:this.props.isRTL? this.props.currentCountry.arabicName : this.props.currentCountry.countryName})
            //this.setState({countryLoad:false, countries:response.data.data,selectedCountry:response.data.data[0].id,countryImage:response.data.data[0].img,countryText:response.data.data[0].countryName})
            console.log('countries   ',response.data)
            //this.getCities(response.data.data[0].id)
        
        }).catch(error => {
            console.log("countries error   ",error);
            console.log("error   ",error.response);         
        })   
}


  disableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

   validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


  signupPage = ()=>{
    const  { isRTL,navigator,userToken } = this.props;
    const  {hidePassword,confirmHidePassword, nameError,phoneError,emailError,passwordError, name,email,password,confirmPassword,phone,selectedCountry,countries,countryImage,countryText,countryLoad,agreeTerms} = this.state

    return(
    <ScrollView>
        <View style={{marginTop:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.name}</Text>
          <View style={{justifyContent:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
              <TextInput 
              onChangeText={(val)=>{this.setState({name:val})}}
              placeholder={Strings.enterYourName} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
          </View>
          {name.length==0&&
          <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {nameError}</Text>
          }
          </View>

        <View style={{marginTop:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
          <View style={{justifyContent:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
              <TextInput
              keyboardType='email-address'
              onChangeText={(val)=>{this.setState({email:val})}}
               placeholder={Strings.enterYourEmail} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
          </View>
          {email.length==0&&
          <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {emailError}</Text>
          }
        </View>

        <View style={{marginTop:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.phone}</Text>
          <View style={{justifyContent:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
              <TextInput
              keyboardType='phone-pad'
               onChangeText={(val)=>{this.setState({phone:val})}}
               placeholder={Strings.enterYourPhone} underlineColorAndroid='transparent' style={{ marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
          </View>
          {phone.length==0&&
          <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {phoneError}</Text>
          }
        </View>


        <View
         
         style={{ marginTop:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <Text style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center', color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.country}</Text>
          <TouchableOpacity
          onPress={()=>{this.setState({openModal:true})}} 
          style={{backgroundColor:'white',alignItems:'center', flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}
          >
            {countryLoad?
            <View style={{backgroundColor:'white',alignItems:'center',justifyContent:'center',elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                <ActivityIndicator />
            </View>
            :
            <View style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row', backgroundColor:'white', }}>
             <View style={{borderRightWidth:!isRTL?0.3:0,borderLeftWidth:isRTL?0.3:0,justifyContent:'center',alignItems:'center',height:responsiveHeight(7.5)}} >
              <FastImage resizeMode='contain' source={{uri:countryImage}} style={{marginHorizontal:moderateScale(5),width:responsiveWidth(10),height:responsiveHeight(5)}} />
              </View>
              <Text style={{color:'black', marginHorizontal:moderateScale(5)}}>{countryText}</Text>
            </View>
            }
         
          <Icon name='chevron-small-down' type='Entypo'  style={{marginHorizontal:moderateScale(5), color:'black',fontSize:responsiveFontSize(7)}} />
          </TouchableOpacity>
        </View>
        
        <Modal
         onRequestClose={()=>this.setState({openModal:false})}
        animationType="slide"
        transparent={false}
         visible={this.state.openModal}
         >
            <TouchableOpacity
            onPress={()=>{this.setState({openModal:false})}}
             style={{alignSelf:'flex-end',margin:moderateScale(10)}}>
              <Icon name='closecircle' type='AntDesign' />
            </TouchableOpacity>
            {countries.map((val,index)=>
               <TouchableOpacity
                onPress={()=>this.setState({selectedCountry:val.id,countryText:this.props.isRTL?val.arabicName:val.countryName,countryImage:val.img, openModal:false})}
                style={{marginBottom:moderateScale(5), borderBottomWidth:0.4,borderBottomColor:'gray', flexDirection:'row',width:responsiveWidth(100),alignItems:'center',marginTop:moderateScale(5)}}>
                 <FastImage resizeMode='contain' source={{uri:val.img}} style={{marginHorizontal:moderateScale(5),width:responsiveWidth(10),height:responsiveHeight(5)}} />
                  <Text style={{color:'black',}}>{isRTL?val.arabicName:val.countryName}</Text>
              </TouchableOpacity>
              )}
        </Modal>
        
        <View style={{marginTop:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.password}</Text>
                    <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
                        <TextInput         
                        secureTextEntry={hidePassword}
                        onChangeText={(val)=>{this.setState({password:val})}}
                        placeholder={Strings.enterYourPassword} underlineColorAndroid='transparent' style={{width:responsiveWidth(81), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                        <TouchableOpacity 
                        onPress={()=>{this.setState({hidePassword:!hidePassword})}}
                        >
                            <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={hidePassword?'eye-off':'eye'} type='Feather' />
                        </TouchableOpacity>
                    </View>
                    {password.length==0&&
                      <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {passwordError}</Text>
                    }
          </View>


        <View style={{marginTop:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
           <Text style={{color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.confirmPassword}</Text>
           <View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderWidth:1,borderColor:'#EDEDED',height:responsiveHeight(8)}}>
            <TextInput         
            secureTextEntry={confirmHidePassword}
            onChangeText={(val)=>{this.setState({confirmPassword:val})}}
            placeholder={Strings.enterYourConfirmPassword} underlineColorAndroid='transparent' style={{width:responsiveWidth(81), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
            <TouchableOpacity 
              onPress={()=>{this.setState({confirmHidePassword:!confirmHidePassword})}}
            >
              <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={confirmHidePassword?'eye-off':'eye'} type='Feather' />
              </TouchableOpacity>
              </View>
              {confirmPassword.length==0&&
              <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
              }
          </View>
        

        <TouchableOpacity style={{alignItems:'center',marginTop:moderateScale(7), marginHorizontal:moderateScale(7),flexDirection:isRTL?'row-reverse':'row',alignSelf:isRTL?'flex-end':'flex-start'}}>
        <Checkbox
            checked={this.state.agreeTerms}
            style={{backgroundColor: "white",color: 'black',borderRadius: 5}}
            onChange={(name, checked) => {
              this.setState({ agreeTerms: checked });
            }}
          />
          <Text style={{fontFamily:'Roboto-Regular', fontSize:responsiveFontSize(6), color:'black',marginHorizontal:moderateScale(4)}}>{Strings.agreeToOurPrivacy}</Text>
        </TouchableOpacity>

        <TouchableOpacity
        disabled={!agreeTerms}
        style={{ width:responsiveWidth(90),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(12),borderRadius:moderateScale(2)}}
        onPress={()=>{
          if(name.replace(/\s/g, '').length<3){
            this.setState({name:'',nameError:Strings.nameLength})
          }
          if(phone.replace(/\s/g, '').length<11){
            this.setState({phone:'',phoneError:Strings.phoneLength})
          }
          if(password.replace(/\s/g, '').length<8){
            this.setState({password:'',passwordError:Strings.passwordLength})
          }

          /*if(!email.replace(/\s/g, '').includes('@')|| !email.replace(/\s/g, '').includes('.com')){
            this.setState({email:'',emailError:Strings.validEmail})
          }*/

          if(!phone.replace(/\s/g, '').length){
            this.setState({phone:'',phoneError:Strings.require})
          }
          if(!name.replace(/\s/g, '').length){
            this.setState({name:'',nameError:Strings.require})
          }
          if(!email.replace(/\s/g, '').length){
            this.setState({email:'',emailError:Strings.require})
          }
          if(!password.replace(/\s/g, '').length){
            this.setState({password:'',passwordError:Strings.require})
          }
          if(!confirmPassword.replace(/\s/g, '').length){
            this.setState({confirmPassword:''})
          }

          if(password!=confirmPassword){
            RNToasty.Error({title:Strings.errorConfirmPassword})
          }

          if( password.replace(/\s/g, '').length>=8&& name.replace(/\s/g, '').length>=3&&phone.replace(/\s/g, '').length>=11&& password==confirmPassword&&phone.replace(/\s/g, '').length&&name.replace(/\s/g, '').length&&email.replace(/\s/g, '').length&&password.replace(/\s/g, '').length&&confirmPassword.replace(/\s/g, '').length){
           
            const user = {
              token:userToken,
              username:name,
              email:email.toLocaleLowerCase(),
              phone:phone,
              country: countryText,
              countryId: selectedCountry,
              type: 'CLIENT',
              password: password,
            }

            this.signupProcess(user)
           
          }

          
        }}
        >
          
          <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.registeration}</Text>
        </TouchableOpacity>

        <View style={{marginVertical:moderateScale(6),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center',}}>
            <Text style={{color:'black',fontSize:responsiveFontSize(6)}}>{Strings.alreadHaveAccount}</Text>
            <TouchableOpacity
            onPress={()=>{
              navigator.push({
                screen:"Login",
                animated: true,
                animationType:'slide-horizontal'
              })
            }}
             style={{marginVertical:moderateScale(2)}}>
              <Text style={{color:'black',fontSize:responsiveFontSize(6)}}>  {Strings.login10}</Text>
            </TouchableOpacity>
        </View>
       
        {this.state.signupLoading && (
          <LoadingDialogOverlay title={Strings.checkSignup} />
        )}

      </ScrollView>
    )
  }

  verifyCodePage = () => {
    const  { isRTL,navigator,userToken } = this.props;
    const  {hidePassword,confirmHidePassword, nameError,phoneError,emailError,passwordError, name,email,password,confirmPassword,phone,selectedCountry,countries,countryImage,countryText,countryLoad,agreeTerms} = this.state
    return(
      <View style={{flex:1}} >

        <View style={{marginTop:moderateScale(10),alignSelf:'center'}}>
            <Text style={{fontSize:responsiveFontSize(6.5),color:'#C1C1C1'}}>{Strings.enterPin}</Text>
        </View>
     
        <View style={{justifyContent:'center',alignItems:'center', alignSelf:'center',marginTop:moderateScale(20),height:responsiveHeight(15)}}>
              
              <CodeInput
              ref="codeInputRef1"
              secureTextEntry
              //compareWithCode='1234'
              codeLength={4}
              className={'border-b'}
              space={5}
              size={45}
              inputPosition='center'
              activeColor='black'//'rgba(49, 180, 4, 1)'
              inactiveColor='#C1C1C1'
              onFulfill={(code) => {
                  console.log("code is ", code)
                 // this.setState({code:code})
                  this._onFulfill(code)
              }}
              />
          </View>

          <View style={{marginTop:moderateScale(2),width:responsiveWidth(90),alignSelf:'center',justifyContent:'space-between',flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <TouchableOpacity
                    onPress={()=>{
                        const body = {
                            email:this.state.user.user.email.toLowerCase()
                        }
                        this.setState({verivectionLoading: true})
                        axios.post(`${BASE_END_POINT}sendCode`, JSON.stringify(body), {
                         headers: { 'Content-Type':  'application/json', },
                        })
                        .then(response => {
                            console.log('phone done')
                            console.log(response.data)        
                            this.setState({verivectionLoading: false})
                            RNToasty.Success({title:Strings.weResendTheCodeToYourEmail})
                        })
                        .catch(error => {
                        this.setState({verivectionLoading:false});
                        
                        console.log('phone error')
                        console.log(error)
                        console.log(error.response)
                        });
                                
                      
                    }}
                    >
                        <Text style={{color:'#A3A3A3'}}>{Strings.resenCode}</Text>
                    </TouchableOpacity>
                    <CountDown
                    until={60 * 2 + 30}
                    size={responsiveFontSize(7)}
                    onFinish={() => {
                        const body = {
                            email:this.state.user.user.email.toLowerCase()
                        }
                        this.setState({verivectionLoading: true})
                        axios.post(`${BASE_END_POINT}sendCode`, JSON.stringify(body), {
                         headers: { 'Content-Type':  'application/json', },
                        })
                        .then(response => {
                            console.log('phone done')
                            console.log(response.data)        
                            this.setState({verivectionLoading: false})
                            RNToasty.Success({title:Strings.weResendTheCodeToYourEmail})
                            
                        })
                        .catch(error => {
                        this.setState({verivectionLoading:false});
                        
                        console.log('phone error')
                        console.log(error)
                        console.log(error.response)
                        });
                                
                    }}
                    digitStyle={{backgroundColor: 'transparent'}}
                    digitTxtStyle={{color: '#A3A3A3'}}
                    timeToShow={['M', 'S']}
                    timeLabels={{m: '', s: ''}}
                />
                </View>
         
        {this.state.verivectionLoading && (
          <LoadingDialogOverlay title={Strings.wait} />
        )}
      </View>
    )
  }

  render() {
    const  { isRTL,navigator,userToken } = this.props;
    return (
      <View style={{ flex: 1,backgroundColor:'white' }}>
        <FlatAppHeader title={Strings.registeration} navigator={navigator} />
         
        {this.state.currentPage==0&&
         this.signupPage()
        }

        {this.state.currentPage==1&&
        this.verifyCodePage()     
        }

         {/*<View style={{flex:1}}>
         <Swiper
          style={{flex:1}}
          loop={false}
          autoplay={false}
          scrollEnabled={false}
          showsButtons={false}
          showsPagination={false}
          ref={(s) => this.swiper = s}
          index={this.state.currentPage}                            
          >

          {this.signupPage()}   
          {this.verifyCodePage()}           
         
                            
          </Swiper>
    </View>*/}
       
      </View>
    );
  }
}

const mapDispatchToProps = {
    signup,
    removeItem,
    getUser
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    barColor: state.lang.color,
    userToken: state.auth.userToken, 
    currentCountry:state.auth.currentCountry,
    currentUser: state.auth.currentUser, 
})


export default connect(mapToStateProps,mapDispatchToProps)(Signup);


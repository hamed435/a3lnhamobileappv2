import React,{Component} from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {View,Image,Text,TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import {Button,Icon,Radio} from 'native-base';
import Strings from '../assets/strings';
import AppText from '../common/AppText'
import AppHeader from '../common/AppHeader';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {  moderateScale, responsiveFontSize, responsiveWidth,responsiveHeight } from "../utils/responsiveDimensions";
import {removeItem} from '../actions/MenuActions';

import Checkbox from 'react-native-custom-checkbox';
import FlatAppHeader from  '../common/FlatAppHeader'

class SelectLanguage extends Component {

    
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#707D67',
    };

    state={
        showModal: false,
        lang: this.props.isRTL
    }
  
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }

    componentDidUpdate(){
        this.enableDrawer()
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

      settingsItem = (item) =>{
        const {isRTL,barColor} = this.props
        return(
            <View style={{width:responsiveWidth(90),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',marginTop:moderateScale(10)}}>
                <Text style={{color:'white',fontSize:responsiveFontSize(3)}} >{item}</Text>
                <Text style={{color:'white',fontSize:responsiveFontSize(3)}} >{item}</Text>
            </View>
        )
      }
     
    render(){
        const {isRTL,barColor,navigator} = this.props
        const {showModal,lang} = this.state;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
              <FlatAppHeader menu navigator={navigator} title={Strings.language} />            

                <TouchableOpacity
                activeOpacity={1}
                 onPress={()=>{this.setState({showModal:!showModal})}}
                 style={{backgroundColor:'white', marginTop:moderateScale(10), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90),alignSelf:'center',borderRadius:moderateScale(2),borderColor:'white',elevation:1,shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(4)}} >{lang?'اللغة العربية':'English'}</Text>
                    <View style={{marginHorizontal:moderateScale(4)}}>
                        <Icon name={showModal?'chevron-small-up':'chevron-small-down'} type='Entypo'  style={{color:'black',fontSize:responsiveFontSize(2.4)}} />
                    </View>
                </TouchableOpacity>
                {showModal&&
                <View style={{height:responsiveHeight(22),backgroundColor:'white', alignItems:'center', width:responsiveWidth(90),alignSelf:'center',borderRadius:moderateScale(2),elevation:1,shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1}}>
                <TouchableOpacity
                 onPress={()=>{ this.setState({lang:false,showModal:false}) }}
                 style={{borderBottomWidth:0.3,borderBottomColor:'#DBDBDB',backgroundColor:'white', marginTop:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90)}}>
                    <Text style={{color:'black',fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(4)}} >English</Text>
                    {!lang&&
                    <View style={{marginHorizontal:moderateScale(4)}}>
                    <Checkbox            
                        checked={lang?false:true}
                        style={{marginHorizontal:moderateScale(4), borderColor:'#679C8A', backgroundColor: "#679C8A",color: 'white',borderRadius: 0}}
                        /*onChange={(name, checked) => {
                        this.setState({ agreeTerms: checked });
                        }}*/
                    />
                    </View>
                    }
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={()=>{ this.setState({lang:true,showModal:false}) }}
                style={{backgroundColor:'white', marginVertical:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90)}}>
                    <Text style={{color:"black",fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(4)}} >اللغة العربية</Text>
                    {lang&&
                     <View style={{marginHorizontal:moderateScale(4)}}>
                     <Checkbox               
                         checked={lang?true:false}
                         style={{marginHorizontal:moderateScale(4), borderColor:'#679C8A', backgroundColor: "#679C8A",color: 'white',borderRadius: 0}}
                         /*onChange={(name, checked) => {
                         this.setState({ agreeTerms: checked });
                         }}*/
                     />
                     </View>
                    }
                </TouchableOpacity>
                </View>
                }
            
                <TouchableOpacity
                    style={{width:responsiveWidth(80),alignSelf:'center',height:responsiveHeight(8),alignItems:'center',justifyContent:'center',backgroundColor:'#679C8A',marginTop:moderateScale(15),borderRadius:moderateScale(2)}}
                    onPress={()=>{
                    if(lang){
                        this.props.changeLanguage(true);
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang','ar')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.navigator.pop()
                    }else{
                        this.props.changeLanguage(false);
                        Strings.setLanguage('en');
                        AsyncStorage.setItem('@lang','en')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.navigator.pop()
                    }
                    }}
                    >
                    <Text style={{color:'white',fontSize:responsiveFontSize(8)}}>{Strings.done}</Text>
                </TouchableOpacity>
           
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
   
})

const mapDispatchToProps = {
    changeLanguage,
    changeColor,
    removeItem
}



export default connect(mapToStateProps,mapDispatchToProps)(SelectLanguage);
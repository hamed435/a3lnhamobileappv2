

const countries = [
    
    {
        name: 'Saudi',
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Flag_of_Saudi_Arabia.svg/2000px-Flag_of_Saudi_Arabia.svg.png'
    },
    {
        name: 'Kuwait',
        img: 'https://images-na.ssl-images-amazon.com/images/I/81cmCn55G2L._SL1500_.jpg'
    },
    {
        name: 'UAE',
        img: 'https://images-na.ssl-images-amazon.com/images/I/413YBG3WHML._SX425_.jpg'
    },
    {
        name: 'Oman',
        img: 'http://www.printableflags.net/wp-content/uploads/2017/04/oman-flag-1c65d11a09e212b04992581bdecf7b5c-OwMfpr.jpg'
    }
    ,{
        name: 'Bahrain',
        img: 'http://www.all-flags-world.com/country-flag/Bahrain/flag-bahrain-XL.jpg'
    }
    
] 

export default countries;
import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Text,Image} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppText from '../common/AppText';
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers,showDeleteNotificationDialog} from '../actions/NotificationAction'
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import AsyncStorage  from '@react-native-community/async-storage'
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import {getUser} from '../actions/AuthActions';
import { RNToasty } from 'react-native-toasty';


class NotificationCard extends Component {
    
    state = {
        ground: false,
        fadeAnim: new Animated.Value(0),
        delete:false,
        on: 'ads' in this.props.data?this.props.data.ads.notif:false,
    }

    _menu = null;
 
    setMenuRef = ref => {
      this._menu = ref;
    };
   
    hideMenu = () => {
      this._menu.hide();
    };
   
    showMenu = () => {
      this._menu.show();
    };
    
    componentDidMount(){
        //moment.locale(this.props.isRTL ? 'ar' : 'en');
    }

    readNotification = (notID) => {
          console.log(notID)
        axios.put(`${BASE_END_POINT}notif/${notID}/read`,{}, {
            headers: {
              'Content-Type': 'application/json',
              //this.props.currentUser.token
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
        }).then(Response=>{
            console.log("notification is red")
            console.log(Response)
            this.setState({ground:true})
            this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
        }).catch(error=>{
            console.log(error.response)
        })
    }

    deleteNotification = (notID) => {
        console.log(notID)
      axios.delete(`${BASE_END_POINT}notif/${notID}/delete`,{
          headers: {
            'Content-Type': 'application/json',
            //this.props.currentUser.token
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
      }).then(Response=>{
          console.log("notification is deleted")
          console.log(Response)
          this.setState({delete:true})
          //this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
      }).catch(error=>{
          console.log(error.response)
      })
    }

    turnOnNotification = (notID) => {
        console.log(notID)
      axios.put(`${BASE_END_POINT}ads/${notID}/enableNotif`,{}, {
          headers: {
            'Content-Type': 'application/json',
            //this.props.currentUser.token
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
      }).then(Response=>{
          console.log("notification is on")
          console.log(Response)
          this.setState({on:true})
          //this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
      }).catch(error=>{
          console.log(error.response)
      })
  }

  turnOfNotification = (notID) => {
    console.log(notID)
    axios.put(`${BASE_END_POINT}ads/${notID}/disableNotif`,{}, {
        headers: {
            'Content-Type': 'application/json',
            //this.props.currentUser.token
            'Authorization': `Bearer ${this.props.currentUser.token}`
        },
    }).then(Response=>{
        console.log("notification is of")
        console.log(Response)
        this.setState({on:false})
        //this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
    }).catch(error=>{
        console.log(error.response)
    })
}


follow = () => {
    //this.setState({disableFollowButton:true})
    axios.post(`${BASE_END_POINT}follow/${this.props.data.resource.id}/follow`, {}, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('Follow Done');
       this.setState({isFollow:true,})
      RNToasty.Success({title:Strings.youFollowHimNow})
      const user = {...this.props.currentUser,user:{...response.data.myUser}}
      console.log('current user')
      console.log(user)
      this.props.getUser(user)
      AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
    }).catch(error => {
       // this.setState({disableFollowButton:false})
        if(error.status==403){
            //RNToasty.Info({title:Strings.youFollowHimNow})
        }
        console.log('Follow error  ',error.status);
        console.log('Follow error  ',error.respone);
        console.log('Follow error  ',error);
        
      });
}

unFollow = () => {
    console.log('id   ',this.props.data.resource.id);
    //this.setState({disableFollowButton:true})
    axios.put(`${BASE_END_POINT}follow/${this.props.data.resource.id}/unfollow`, {}, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('un Follow Done');
       this.setState({isFollow:false,})
       const user = {...this.props.currentUser,user:{...response.data.myuser}}
       console.log('current user')
       console.log(user)
       this.props.getUser(user)
       AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
      RNToasty.Success({title:Strings.YouUnFollwHim})
    }).catch(error => {
        if(error.status==403){
            //RNToasty.Info({title:Strings.youFollowHimNow})
        }
        //this.setState({disableFollowButton:false})
        console.log('unFollow error  ',error.status);
        console.log('unFollow error  ',error.respone);
        console.log('unFollow error  ',error);
        
      });
}


Block = () => {
    //this.setState({disableFollowButton:true})
    axios.put(`${BASE_END_POINT}${this.props.data.resource.id}/blockPeople`, {}, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('Block Done ',response.data);
       this.setState({isFollow:true,})
      RNToasty.Success({title:Strings.userBlocke})
      const user = {...this.props.currentUser,user:{...response.data.user}}
      console.log('current user')
      console.log(user)
      this.props.getUser(user)
      AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
    }).catch(error => {
       // this.setState({disableFollowButton:false})
        if(error.status==403){
            //RNToasty.Info({title:Strings.youFollowHimNow})
        }
        console.log('block error  ',error.status);
        console.log('block error  ',error.respone);
        console.log('block error  ',error);
        
      });
}

Unblock = () => {
    //this.setState({disableFollowButton:true})
    axios.put(`${BASE_END_POINT}${this.props.data.resource.id}/unblockPeople`, {}, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('unBlock Done');
       this.setState({isFollow:true,})
      RNToasty.Success({title:Strings.userUnblock})
      const user = {...this.props.currentUser,user:{...response.data.user}}
      console.log('current user')
      console.log(user)
      this.props.getUser(user)
      AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
    }).catch(error => {
       // this.setState({disableFollowButton:false})
        if(error.status==403){
            //RNToasty.Info({title:Strings.youFollowHimNow})
        }
        console.log('block error  ',error.status);
        console.log('block error  ',error.respone);
        console.log('block error  ',error);
        
      });
}


    render(){
        const {data,isRTL,navigator,currentUser} = this.props;
        console.log("noti data",data)
        //const dateToFormat = () => <Moment date={date} />,
        //backgroundColor:data.read||this.state.ground? 'white' : '#D4E2DD',
        return( 
            <TouchableOpacity
            onPress={()=>{
                if(!data.read){
                    this.readNotification(data.id)
                } 

                if(data.description.includes('like')||data.description.includes('share')||data.description.includes('comment')||data.description.includes('accepted')||data.description.includes('refused')||data.description.includes('add')){
                    if(data.ads.type=='JOPS'){
                        navigator.push({
                            screen: 'JobAdsDescription',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data.ads}
                        })
                    }else{
                        navigator.push({
                            screen: 'AdsDescription',
                            animated:true,
                            animationType:'slide-horizontal',
                            passProps:{data:data.ads}
                        })
                    }
                }else if(data.description.includes('follow')){
                    navigator.push({
                        screen: 'FriendProfile',
                        animated:true,
                        animationType:'slide-horizontal',
                        passProps:{data:data.resource}
                    })
                }else if(data.description.includes('new message')){
                    navigator.push({
                        screen: 'DirectChat',
                        animated:true,
                        animationType:'slide-horizontal',
                        passProps:{data:data.resource}
                    })
                }   

            }}
            activeOpacity={1}
            style={{backgroundColor:data.read||this.state.ground? 'white' : '#f6f3f3', height:responsiveHeight(20),width:responsiveWidth(95),alignSelf:'center'}}
            >
                <Text style={{marginHorizontal:moderateScale(3), alignSelf:isRTL?'flex-end':'flex-start',colors:'black'}}>{data.createdAt.substring(0,10)}</Text>
                <View style={{marginTop:moderateScale(3), alignItems:'center', flexDirection:isRTL?'row-reverse':'row'}}>
                    <Image style={{width:70,height:70,borderRadius:35}} source={data.resource.img?{uri:data.resource.img}:require('../assets/imgs/profileicon.jpg')} />
                    <View style={{marginHorizontal:moderateScale(3)}}>
                        <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between',width:responsiveWidth(70)}}>
                            <Text style={{color:'black'}}>{data.resource.username}</Text>
                            
                            <Menu
                                ref={this.setMenuRef}
                                button={<Icon  onPress={this.showMenu} name='ellipsis1' type='AntDesign' style={{color:'#646464'}} />}
                                >
                                {'ads' in this.props.data ?
                                this.state.on?
                                 <MenuItem onPress={()=>{
                                    this.turnOfNotification(data.ads.id)
                                    this.hideMenu();
                                }}> {Strings.turnOfPostNotification}</MenuItem>
                                 :
                                 <MenuItem onPress={()=>{
                                    this.turnOnNotification(data.ads.id)
                                    this.hideMenu();
                                }}>{Strings.turnOnPostNotification}</MenuItem>
                                :
                                null
                                }

                                <MenuItem onPress={()=>{
                                    this.props.showDeleteNotificationDialog(data.id,true)
                                    //this.deleteNotification(data.id);
                                    this.hideMenu();
                                }}>{Strings.deleteThisNotification}</MenuItem>    
                               {/*
                                <MenuItem onPress={this.hideMenu}>{Strings.follow}</MenuItem>
                                <MenuItem onPress={this.hideMenu}>{Strings.block}</MenuItem>        
                               */}
                               { currentUser.user.id!=data.resource.id?
                                currentUser.user.following.includes(data.resource.id)?
                                <MenuItem onPress={()=>{
                                   this.unFollow()
                                    this.hideMenu();
                                }}>{Strings.unFollow}</MenuItem>  
                                :
                                <MenuItem onPress={()=>{
                                    this.follow()
                                    this.hideMenu();
                                }}>{Strings.follow}</MenuItem> 
                                :
                                null 
                               }
                                
                                { currentUser.user.id!=data.resource.id?
                                currentUser.user.blocked.includes(data.resource.id)?
                                <MenuItem onPress={()=>{
                                   this.Unblock()
                                    this.hideMenu();
                                }}>{Strings.unblock}</MenuItem>  
                                :
                                <MenuItem onPress={()=>{
                                    this.Block()
                                    this.hideMenu();
                                }}>{Strings.block}</MenuItem> 
                                :
                                null 
                               }
                               </Menu>

                               
                            
                        </View>
                        <View style={{marginTop:moderateScale(4), flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between',width:responsiveWidth(70)}}>
                            <Text style={{color:'black'}} >{isRTL? data.arabicDescription.length>20?data.arabicDescription.substring(0,20):data.arabicDescription  : data.description.length>20?data.description.substring(0,20):data.description}</Text>
                            <Text style={{color:'gray'}}>{data.createdAt.substring(14,19)}</Text>
                        </View>
                    </View>

                </View>
                <View style={{alignSelf:'center',marginTop:moderateScale(6), width:responsiveWidth(70),borderBottomWidth:0.5,borderBottomColor:'black'}} />
                          
            </TouchableOpacity>       
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    showDeleteNotificationDialog,
    getUser
}


export default connect(mapStateToProps,mapDispatchToProps)(NotificationCard);

import React,{Component} from 'react';
import {View,ImageBackground,Text,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';
const MyButton =  withPreventDoubleClick(Button);
import Stars from 'react-native-stars';
import {getAds,showDeleteAdsDialog} from '../actions/AdsAction'
import * as Animatable from 'react-native-animatable';


 class AdsCard extends Component {
     state={
       delete:false,
       rate:3,
     }


    componentDidMount(){
      if(this.props.data.ratePrecent==0){
        this.setState({rate:0.5})
      }else if(this.props.data.ratePrecent>0&&this.props.data.ratePrecent<=20){
        this.setState({rate:1})
      }else if(this.props.data.ratePrecent>20&&this.props.data.ratePrecent<=40){
        this.setState({rate:2})
      }else if(this.props.data.ratePrecent>40&&this.props.data.ratePrecent<=60){
        this.setState({rate:3})
      }else if(this.props.data.ratePrecent>60&&this.props.data.ratePrecent<=80){
        this.setState({rate:4})
      }else if(this.props.data.ratePrecent>80){
        this.setState({rate:5})
      }
    }

     deleteAds = (id) => {
       // console.log("my product  "+this.props.data.product.id)
        axios.delete(`${BASE_END_POINT}ads/${id}`, {
            headers: {
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
        .then(response=>{
            console.log('ads deleted')
            console.log(response.data);       
            this.props.getAds(1,true,this.props.currentUser.user.id)
                          
        }).catch(error=>{
            console.log(error);
            console.log(error.response);
        })     
          
     }

    render(){
        const {data,isRTL,navigator,remove} = this.props;
        
        return(
          <Animatable.View duration={1500} animation='zoomIn' style={{marginVertical:moderateScale(5)}} >
           <TouchableOpacity
           onPress={()=>{
            if(data.category.type=='JOPS'){
                navigator.push({
                    screen:'JobAdsDescription',
                    animated:true,
                    animationType:'slide-horizontal',
                    passProps:{data:data}
                })
            }
            else {
                navigator.push({
                    screen:'AdsDescription',
                    animated:true,
                    animationType:'slide-horizontal',
                    passProps:{data:data}
                })
            }
          }}
           activeOpacity={1} 
           style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1,elevation:2,backgroundColor:'white' ,borderRadius:moderateScale(3), flexDirection:isRTL?'row-reverse':'row', width:responsiveWidth(90),minHeight:responsiveHeight(19),alignSelf:'center'}}>
               <ImageBackground resizeMode='stretch' 
               style={{flex:1, alignItems:'center', borderTopLeftRadius:!isRTL?moderateScale(2):0,borderBottomLeftRadius:!isRTL?moderateScale(2):0,borderTopRightRadius:isRTL?moderateScale(2):0,borderBottomRightRadius:isRTL?moderateScale(2):0,width:responsiveWidth(27),}}
                source={{uri:data.img[0]}}
                >
                  {data.soled&&
                  <View style={{position:'absolute',backgroundColor:'#679C8A',justifyContent:'center',alignItems:'center',paddingHorizontal:moderateScale(5),paddingVertical:moderateScale(3.5), alignSelf:isRTL?'flex-end':'flex-start',marginTop:moderateScale(5)}}>
                      <Text style={{color:'white',fontFamily:'Roboto-Regular',}} >{strings.buyed}</Text>
                  </View>
                  }
                </ImageBackground>
               
               
               <View style={{ width:responsiveWidth(60),marginHorizontal:moderateScale(2), minHeight:responsiveHeight(19)}}>
                    <View style={{marginHorizontal:moderateScale(2),marginTop:moderateScale(3), flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',}}>
                        <Text style={{width:responsiveWidth(50),textAlign:isRTL?'right':'left', fontFamily:'Roboto-Regular',color:'#B7B7B7',fontSize:responsiveFontSize(6)}}>{data.title}</Text>
                        <TouchableOpacity
                        onPress={()=>{
                          if(data.category.type=='MOTOR'){
                            navigator.push({
                              screen:'UpdateCarAds',
                              animated:true,
                              animationType:'slide-horizontal',
                              passProps:{data:data}
                          })
                          }else if(data.category.type=='JOPS'){
                              navigator.push({
                                  screen:'UpdateJobAds',
                                  animated:true,
                                  animationType:'slide-horizontal',
                                  passProps:{data:data}
                              })
                          }else if(data.category.type=='REAL-STATE'){
                            navigator.push({
                                screen:'UpdateBuildingAds',
                                animated:true,
                                animationType:'slide-horizontal',
                                passProps:{data:data}
                            })
                        }
                          else {
                              navigator.push({
                                  screen:'UpdateAds',
                                  animated:true,
                                  animationType:'slide-horizontal',
                                  passProps:{data:data}
                              })
                          }

                       
                        }}
                        >
                        <Icon name='edit' type='MaterialIcons' style={{fontSize:responsiveFontSize(10), color:'#036C90'}} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginHorizontal:moderateScale(2),marginTop:moderateScale(3),justifyContent:'center'}}>
                        <Text style={{fontFamily:'Roboto-Regular',alignSelf:isRTL?'flex-end':'flex-start', color:'#B7B7B7',fontSize:responsiveFontSize(6)}}>{data.country.currency} {data.category.type=='JOPS'?data.priceFrom+"-"+data.priceTo:data.price}</Text>
                    </View>

                    <View style={{marginHorizontal:0, justifyContent:'center', marginTop:moderateScale(3),}}>
                    <View  style={{fontFamily:'Roboto-Regular',alignSelf:isRTL?'flex-end':'flex-start',width:responsiveWidth(25),justifyContent:'center'}}>
                    <Stars
                       disabled
                        default={this.state.rate}
                        count={5}
                        half={true}

                        fullStar={<Icon name='star' type='MaterialCommunityIcons' style={[styles.myStarStyle]}/>}
                        emptyStar={<Icon name={'star-outline'} type='MaterialCommunityIcons' style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
                        halfStar={<Icon name={'star-half'} type='MaterialIcons' style={[styles.myStarStyle]}/>}
                    />
                    </View>

                    </View>
                    
                    <View style={{marginHorizontal:0, flex:1,marginVertical:moderateScale(3), flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center'}}>
                        <View style={{marginHorizontal:moderateScale(1), alignItems:'center', flexDirection:isRTL?'row-reverse':'row'}}>
                        <Icon name='location-pin' type='Entypo' style={{fontSize:responsiveFontSize(6),color:'#036C90'}} />
                        <Text style={{fontFamily:'Roboto-Regular',color:'#B7B7B7',fontSize:responsiveFontSize(6)}}>{data.country.countryName}</Text>
                        </View>
                        <TouchableOpacity
                        onPress={()=>{
                          this.props.showDeleteAdsDialog(data.id,true)
                          //this.deleteAds(data.id)
                        }}
                        >
                            <Icon name='delete' type='MaterialCommunityIcons' style={{fontSize:responsiveFontSize(10),padding:0, color:'red'}} />
                        </TouchableOpacity>
                    </View>
               </View>
               
           </TouchableOpacity>
           </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    myStarStyle: {
      color: '#FFC416',
      backgroundColor: 'transparent',
      //textShadowColor: 'black',
      //textShadowOffset: {width: 1, height: 1},
      //textShadowRadius: 2,
      fontSize:responsiveFontSize(7),
      margin:0,
      padding:0,
    },
    myEmptyStarStyle: {
      color: 'gray',
      fontSize:responsiveFontSize(7),
      margin:0,
      padding:0,
    }
  });

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
  getAds,
  showDeleteAdsDialog
}

export default connect(mapStateToProps,mapDispatchToProps)(AdsCard);

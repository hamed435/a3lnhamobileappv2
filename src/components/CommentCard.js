import React,{Component} from 'react';
import {View,ImageBackground,Text,Alert,TouchableOpacity,StyleSheet} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import {Button,Icon,Thumbnail} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
const MyButton =  withPreventDoubleClick(Button);
import Stars from 'react-native-stars';
import * as Animatable from 'react-native-animatable';


 class CommentCard extends Component {

 

    render(){
        const {data,onPress,isRTL,navigator,currentUser} = this.props; 
        return(
          <Animatable.View duration={1500} animation='zoomIn' >
            <TouchableOpacity
            onPress={()=>{
              if(currentUser.user.id==data.user.id){
                navigator.push({
                  screen:'Profile',
                  animated:true,
                  animationType:'slide-horizontal',
                  //passProps:{data:data.user}
              })
              }else{
                navigator.push({
                  screen:'FriendProfile',
                  animated:true,
                  animationType:'slide-horizontal',
                  passProps:{data:data.user}
              })
              }
             
            }}
            activeOpacity={1}
            style={{alignSelf:'center', width:responsiveWidth(90),borderBottomWidth:0.4,borderBottomColor:'#646464'}}
            >

            <View style={{marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row'}}>
                <Thumbnail small source={data.user.img?{uri:data.user.img}:require('../assets/imgs/profileicon.jpg')} />
               <View style={{marginHorizontal:moderateScale(5)}}>
               <Text style={{fontFamily:'Roboto-Regular',color:'#646464'}}>{data.user.username}</Text>
               <View style={{marginHorizontal:moderateScale(5), width:responsiveWidth(15)}}>
               <Stars  
               disabled                    
                       default={Number(data.rate)}
                       count={5}
                       half={true}
                       fullStar={<Icon name='star' type='MaterialCommunityIcons' style={[styles.myStarStyle]}/>}
                       emptyStar={<Icon name={'star-outline'} type='MaterialCommunityIcons' style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
                       halfStar={<Icon name={'star-half'} type='MaterialIcons' style={[styles.myStarStyle]}/>}
                   />
                </View>
               </View>
            </View>
            <View style={{borderRadius:moderateScale(3), backgroundColor:'#f2f2f0', margin:moderateScale(5)}}>
            <Text style={{fontFamily:'Roboto-Regular',margin:moderateScale(4),marginVertical:moderateScale(6),  color:'#646464'}}>{data.comment}</Text>
            </View>
            


            </TouchableOpacity>
          </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    myStarStyle: {
      color: '#FFC416',
      backgroundColor: 'transparent',
      textShadowColor: 'black',
      textShadowOffset: {width: 1, height: 1},
      textShadowRadius: 2,
      fontSize:responsiveFontSize(6),
      margin:0,
      padding:0,
    },
    myEmptyStarStyle: {
      color: 'white',
      fontSize:responsiveFontSize(6),
      margin:0,
      padding:0,
    }
  });

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

export default connect(mapStateToProps)(CommentCard);

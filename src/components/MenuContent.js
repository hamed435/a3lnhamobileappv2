import React, { Component } from 'react';
import {
  View,
  Text, Alert,
  Image,
  ActivityIndicator,
  Platform,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Share,Linking
} from 'react-native';
import { connect } from 'react-redux';
import { Button, Content, Icon, Thumbnail } from 'native-base';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/MenuActions';
import logout from "../actions/LogoutActions";
import { AppText, MenuItem } from '../common';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import { rootNavigator } from '../screens/Login';
import Strings from '../assets/strings';
import { selectMenu } from '../actions/MenuActions';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';

import { homeNavigator } from '../screens/SplashScreen'

// Mahmoud Hamed

class MenuContent extends Component {

  state = {
    v: false,
    load: false,
    showLogoutDialog: false,
  }
  componentDidMount() {
    console.log('homeNavigator : ', homeNavigator);
    console.log('HomeNav', this.props.navigator);
  }

  render() {
    const { item, currentUser, color, isRTL } = this.props;
    if (this.props.isRTL) {
      Strings.setLanguage('ar')
    } else {
      Strings.setLanguage('en')
    }
    console.log("menu Content item =>   " + item);
    return (
      <ScrollView style={{ flex: 1, backgroundColor: '#679C8A', width: responsiveWidth(82), }}>

        <TouchableOpacity
          onPress={() => {
            if (this.props.currentUser) {
              if (item[item.length - 1] == "PROFILE") {
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              } else {
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('PROFILE');
                /*this.props.navigator.push({
                  screen:'Profile',
                  animated:true,
                })*/
                this.props.iosRootNavigator.push({
                  screen: 'Profile',
                  animated: true,
                  animationType:'slide-horizontal'
                })
              }
            } else {
              this.props.iosRootNavigator.push({
                screen: 'Login',
                animated: true,
                animationType:'slide-horizontal'
              })
            }
          }}
          style={{ marginHorizontal: moderateScale(7), marginTop: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
          <View style={{ marginTop: hp(5), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
            <Thumbnail
              style={{ borderColor: colors.buttonColor }} large
              source={currentUser && currentUser.user.img ? { uri: currentUser.user.img } : require('../assets/imgs/profileicon.jpg')} />
          </View>

          {currentUser &&
            <View style={{ marginHorizontal: moderateScale(5), marginTop: hp(5) }}>
              <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: 'white', fontSize: responsiveFontSize(8) }}>{currentUser.user.username}</Text>
              <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', marginTop: moderateScale(2), color: 'white', fontSize: responsiveFontSize(7) }}>{isRTL ? currentUser.user.countryId.arabicName : currentUser.user.countryId.countryName}</Text>
            </View>
          }
        </TouchableOpacity>


        <View
          showsVerticalScrollIndicator={false}
          style={{ marginTop: hp(4) }}>
          <View style={{ marginBottom: hp(3) }}>
            <MenuItem
              onPress={() => {
                console.log("main 1 =>   " + item);
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('HOME');
                this.props.iosRootNavigator.resetTo({
                  screen: 'Home',
                  animated: true,
                  animationType:'slide-horizontal'
                })
                /* if(item[item.length-1] == "HOME"){
                   this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                 }else{
                   this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                   this.props.selectMenu('HOME');
                   this.props.navigator.push({
                     screen:'Home',
                     animated: true
                   })
                 }*/
              }}
              focused={item[item.length - 1] == 'HOME'} type='FontAwesome' iconName='home' text={Strings.home}
            />

            {currentUser &&
              <MenuItem
                onPress={() => {
                  if (this.props.currentUser) {
                    if (item[item.length - 1] == "PROFILE") {
                      this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    } else {
                      this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                      this.props.selectMenu('PROFILE');
                      this.props.iosRootNavigator.push({
                        screen: 'Profile',
                        animated: true,
                        animationType:'slide-horizontal'
                      })
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser })
                  }

                }}
                focused={item[item.length - 1] == 'PROFILE'}
                iconName='user-alt' type='FontAwesome5' text={Strings.profile} />
            }

            {currentUser &&
              <MenuItem
                onPress={() => {
                  console.log("main 1 =>   " + item);
                  if (item[item.length - 1] == "MYADS") {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  } else {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    this.props.selectMenu('MYADS');
                    this.props.iosRootNavigator.push({
                      screen: 'Adds',
                      animated: true,
                      animationType:'slide-horizontal'
                    })
                  }
                }}
                focused={item[item.length - 1] == 'MYADS'} type='FontAwesome5' iconName='ad' text={Strings.adds}
              />

            }

            {currentUser &&
              <MenuItem
                onPress={() => {
                  console.log("main 1 =>   " + item);
                  if (item[item.length - 1] == "MYFAV") {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  } else {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    this.props.selectMenu('MYFAV');
                    this.props.iosRootNavigator.push({
                      screen: 'MyFavoriutes',
                      animated: true,
                      animationType:'slide-horizontal'
                    })
                  }
                }}
                focused={item[item.length - 1] == 'MYFAV'} type='AntDesign' iconName='heart' text={Strings.myFav}
              />
            }

            {currentUser &&
              <MenuItem
                onPress={() => {
                  console.log("main 1 =>   " + item);
                  if (item[item.length - 1] == "MYFOLLOWERS") {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  } else {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    this.props.selectMenu('MYFOLLOWERS');
                    this.props.iosRootNavigator.push({
                      screen: 'MyFollowers',
                      animated: true,
                      animationType:'slide-horizontal'
                    })
                  }
                }}
                focused={item[item.length - 1] == 'MYFOLLOWERS'} type='MaterialCommunityIcons' iconName='eye' text={Strings.myFollowers}
              />
            }




           
              <MenuItem
                onPress={() => {
                  console.log("main 1 =>   " + item);
                  if (item[item.length - 1] == "SELECTCOUNTRY") {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  } else {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    this.props.selectMenu('SELECTCOUNTRY');
                    this.props.iosRootNavigator.push({
                      screen: 'SelectCountry',
                      animated: true,
                      animationType:'slide-horizontal'
                    })
                  }
                }}
                focused={item[item.length - 1] == 'SELECTCOUNTRY'} type='MaterialIcons' iconName='location-city' text={Strings.selectCountry}
              />
            


            <MenuItem
              onPress={() => {
                if (item[item.length - 1] == "CHANGELANGUAGE") {
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                } else {
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('CHANGELANGUAGE');
                  this.props.iosRootNavigator.push({
                    screen: 'SelectLanguage',
                    animated: true,
                    animationType:'slide-horizontal'
                  })
                }

              }}
              focused={item[item.length - 1] == 'CHANGELANGUAGE'}
              iconName='web' type='MaterialCommunityIcons' text={Strings.changeLanguage}
            />



            {currentUser &&
              // this.props.logout(this.props.userToken,this.props.currentUser.token,this.props.navigator)
              <TouchableOpacity
                onPress={() => {
                  Share.share({
                    message: `  "رابط تحميل التطبيق" \n \n \n "https://play.google.com/store/apps/details?id=com.a3lnhamobileapp" `,

                  }, {
                    // Android only:
                    dialogTitle: 'Share',
                    // iOS only:
                    excludedActivityTypes: [
                      'com.apple.UIKit.activity.PostToTwitter'
                    ]
                  })
                }}
                style={{ marginTop: moderateScale(5), marginBottom: moderateScale(5), marginHorizontal: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <Icon style={{ fontSize: responsiveFontSize(8), color: 'white' }} name='share' type='Entypo' />
                <Text style={{ marginHorizontal: moderateScale(7), fontSize: responsiveFontSize(8), fontWeight: '500', marginHorizontal: moderateScale(10), color: 'white' }}>{Strings.shareOurApp}</Text>
              </TouchableOpacity>
            }



            {/*currentUser&&
             <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item[item.length-1] == "CHAT"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('CHAT');
                  this.props.navigator.push({
                    screen:'ChatPeople',
                    animated: true
                  })
                }
              }}
             focused={item[item.length-1] == 'CHAT'} iconName='comment-alt' text={Strings.chat}
             />
            */}

            {currentUser &&
              <MenuItem
                onPress={() => {
                  console.log("main 1 =>   " + item);
                  if (item[item.length - 1] == "SETTINGS") {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  } else {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    this.props.selectMenu('SETTINGS');
                    this.props.iosRootNavigator.push({
                      screen: 'Settings',
                      animated: true,
                      animationType:'slide-horizontal'
                    })
                  }
                }}
                focused={item[item.length - 1] == 'SETTINGS'} type='Ionicons' iconName='md-settings' text={Strings.settings}
              />
            }

              {currentUser&&
              <TouchableOpacity
                onPress={() => {
                  if (item[item.length - 1] == "ADSTYPES") {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  } else {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    this.props.selectMenu('ADSTYPES');
                    this.props.iosRootNavigator.push({
                      screen: 'AdsTypes',
                      animated: true,
                      animationType:'slide-horizontal'
                    })
                  }

                }}
                style={{ marginTop: moderateScale(5), marginBottom: moderateScale(5), marginHorizontal: moderateScale(7), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                 
                <View style={{flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
                    <Icon name='ad' type='FontAwesome5' style={{fontSize:responsiveFontSize(10), color:'white'}} />
                    <Text style={{paddingRight:moderateScale(2),marginLeft:moderateScale(-1), fontSize:responsiveFontSize(5.5),fontWeight:'bold',color:'#679C8A',backgroundColor:'white'}} >+</Text>
                </View>
                <Text style={{ marginHorizontal: moderateScale(7), fontSize: responsiveFontSize(7), fontWeight: '500', marginHorizontal: moderateScale(10), color: 'white' }}>{Strings.addAds}</Text>
              </TouchableOpacity>
              }
              
           

            {currentUser &&
              <MenuItem
                onPress={() => {
                  if (item[item.length - 1] == "FRIEND") {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  } else {
                    this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                    this.props.selectMenu('FRIEND');
                    this.props.iosRootNavigator.push({
                      screen: 'ChatPeople',
                      animated: true,
                      animationType:'slide-horizontal'
                    })
                  }

                }}
                focused={item[item.length - 1] == 'FRIEND'}
                iconName='chat' type='Entypo' text={Strings.chat}
              />

            }



            <MenuItem
              onPress={() => {
                if (item[item.length - 1] == "USAGES") {
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                } else {

                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('USAGES');
                  this.props.iosRootNavigator.push({
                    screen: 'Usages',
                    animated: true,
                    animationType:'slide-horizontal'
                  })
                }

              }}
              focused={item[item.length - 1] == 'USAGES'}
              iconName='open-book' type='Entypo' text={Strings.usages}
            />

            {currentUser &&
            <MenuItem
              onPress={() => {
                if (item[item.length - 1] == "COMPLAINTS") {
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                } else {

                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('COMPLAINTS');
                  this.props.iosRootNavigator.push({
                    screen: 'Complaints',
                    animated: true,
                    animationType:'slide-horizontal'
                  })
                }

              }}
              focused={item[item.length - 1] == 'COMPLAINTS'}
              iconName='shield-off' type='Feather' text={Strings.complaints}
            />
            }

            <MenuItem
              onPress={() => {
                if (item[item.length - 1] == "SUGGESTING") {
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                } else {

                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('SUGGESTING');
                  this.props.iosRootNavigator.push({
                    screen: 'Suggesting',
                    animated: true,
                    animationType:'slide-horizontal'
                  })
                }

              }}
              focused={item[item.length - 1] == 'SUGGESTING'}
              iconName='direction' type='Entypo' text={Strings.suggestion}
            />

              <TouchableOpacity
                onPress={() => {
                 Linking.openURL('https://a3lnha.com/')
                }}
                style={{ marginTop: moderateScale(5), marginBottom: moderateScale(5), marginHorizontal: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <Icon style={{ fontSize: responsiveFontSize(8), color: 'white' }} name='webpack' type='MaterialCommunityIcons' />
                <Text style={{ marginHorizontal: moderateScale(7), fontSize: responsiveFontSize(8), fontWeight: '500', marginHorizontal: moderateScale(10), color: 'white' }}>{Strings.goToWebsite}</Text>
              </TouchableOpacity>
            



            {currentUser &&
              // this.props.logout(this.props.userToken,this.props.currentUser.token,this.props.navigator)
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showLogoutDialog: true })
                  // this.props.logout('token',this.props.currentUser.token,this.props.navigator)
                }}
                style={{ marginTop: moderateScale(16), marginBottom: moderateScale(5), marginHorizontal: moderateScale(7), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <Icon style={{ color: 'white' }} name='logout' type='MaterialCommunityIcons' />
                <Text style={{ fontSize: responsiveFontSize(8), fontWeight: '500', marginHorizontal: moderateScale(5), color: 'white' }}>{Strings.logout}</Text>
              </TouchableOpacity>
            }

          </View>
        </View>

        <Dialog
          width={responsiveWidth(70)}

          visible={this.state.showLogoutDialog}
          onTouchOutside={() => {
            this.setState({ showLogoutDialog: false })
            //this.props.showDeleteNotificationDialog(0,false)
          }}
        >
          <View style={{ width: responsiveWidth(70) }}>
            <View style={{ alignSelf: 'center', marginTop: moderateScale(10) }}>
              <Icon name='logout' type='AntDesign' style={{ color: 'red', fontSize: responsiveFontSize(8) }} />
            </View>
            <Text style={{ textAlign: 'center', width: responsiveWidth(60), fontSize: responsiveFontSize(6), alignSelf: 'center', color: 'black', marginTop: moderateScale(3) }} >{Strings.areYouShure3}</Text>
            <View style={{ alignSelf: 'center', width: responsiveWidth(60), justifyContent: 'space-between', alignItems: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', marginVertical: moderateScale(10) }} >
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showLogoutDialog: false })
                  //this.props.showDeleteNotificationDialog(0,false)
                }}
              >
                <Text style={{ fontSize: responsiveFontSize(7), color: 'gray' }}>{Strings.cancel}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.logout('token', this.props.currentUser.token, this.props.iosRootNavigator)
                  this.setState({ showLogoutDialog: false })
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  //this.deleteNotification(this.props.id)
                  //this.props.showDeleteNotificationDialog(0,false)
                }}
              >
                <Text style={{ fontSize: responsiveFontSize(7), color: 'red' }}>{Strings.logout}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </Dialog>

      </ScrollView>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: colors.darkPrimaryColor,
    width: responsiveWidth(82),


  },

  linksContainer: {
    flex: 1,
    marginBottom: moderateScale(15),
  },
  linksContainerScroll: {
    flex: 1,
    marginTop: moderateScale(50)
  },

};

const mapStateToProps = state => ({
  item: state.menu.item,
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  color: state.lang.color,
  iosRootNavigator: state.navigation.iosRootNavigator,

});

const mapDispatchToProps = {
  selectMenu,
  logout,
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuContent);

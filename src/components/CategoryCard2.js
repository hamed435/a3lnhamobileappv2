import React,{Component} from 'react';
import {View,TouchableOpacity,Text,Image} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {AddProductToBacket} from '../actions/OrderAction';
import * as Animatable from 'react-native-animatable';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class CategoryCard2 extends Component {
    

    render(){
        const {data,isRTL,navigator,row,color} = this.props;
        console.log('data')
        console.log(data)
        return(
        <Animatable.View duration={1500} animation='zoomIn' >
          <TouchableOpacity
          onPress={()=>{
            if(data.type=='MOTOR'){             
                navigator.push({
                    screen: 'AddCarAds',
                    animated: true,
                    animationType:'slide-horizontal',
                    passProps:{ data:data }
                })
            }else if(data.type=='JOPS'){
                navigator.push({
                    screen: 'AddJobAds',
                    animated: true,
                    animationType:'slide-horizontal',
                    passProps:{ data:data }
                })
            }
            else if(data.type=='REAL-STATE'){
                navigator.push({
                    screen: 'AddBuildingAds',
                    animated: true,
                    animationType:'slide-horizontal',
                    passProps:{ data:data }
                })
            }else {
                navigator.push({
                    screen: 'AddAds',
                    animated: true,
                    animationType:'slide-horizontal',
                    passProps:{ data:data }
                })
            }
          }}
          activeOpacity={1}
          style={{marginTop:moderateScale(3), marginHorizontal:moderateScale(3), width:responsiveWidth(25),height:responsiveHeight(24),alignItems:'center'}}
          >
              <View
              style={{borderRadius:moderateScale(6), backgroundColor:color, width:responsiveWidth(22),height:responsiveHeight(13),justifyContent:'center',alignItems:'center'}}
              >
                <Image  source={{uri:data.img}} style={{width:responsiveWidth(13),height:responsiveHeight(8)}} resizeMode='contain' />
              </View>

              <Text style={{fontFamily:'Roboto-Regular',textAlign:'center', marginTop:moderateScale(5), color:'#3E6D7A',fontSize:responsiveFontSize(6.5)}}>{isRTL?data.arabicName:data.name}</Text>

          </TouchableOpacity>
          </Animatable.View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
});

const mapDispatchToProps = {
    AddProductToBacket,
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryCard2);

import React,{Component} from 'react';
import {View,ImageBackground,Text,Alert,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import {Button,Icon,Thumbnail} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
const MyButton =  withPreventDoubleClick(Button);


 class MyFollowersCard extends Component {

    _menu = null;
 
    setMenuRef = ref => {
      this._menu = ref;
    };
   
    hideMenu = () => {
      this._menu.hide();
    };
   
    showMenu = () => {
      this._menu.show();
    };


    render(){
        const {data,onPress,isRTL,navigator} = this.props; 
        return(
            <TouchableOpacity
            onPress={()=>{
              navigator.push({
                screen: 'FriendProfile',
                animated: true,
                animationType:'slide-horizontal',
                passProps:{ data:data.follower }
              })
            }}
            activeOpacity={1}
            style={{alignSelf:'center', width:responsiveWidth(90),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center',height:responsiveHeight(11),borderBottomWidth:0.4,borderBottomColor:'#646464'}}
            >

            <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                <Thumbnail small source={data.follower.img?{uri:data.follower.img}:require('../assets/imgs/profileicon.jpg')} />
                <Text style={{fontFamily:'Roboto-Regular',marginHorizontal:moderateScale(5), color:'#646464'}}>{data.follower.username}</Text>
            </View>

          
            <Menu
                ref={this.setMenuRef}
                button={<Icon  onPress={this.showMenu} name='ellipsis1' type='AntDesign' style={{color:'#646464'}} />}
                >
                {/*<MenuItem onPress={this.hideMenu}>{strings.unFollow}</MenuItem>*/}
                <MenuItem onPress={()=>{
                  this.hideMenu()
                   navigator.push({
                    screen: 'FriendProfile',
                    animated: true,
                    animationType:'slide-horizontal',
                    passProps:{ data:data.follower }
                  })
                }}>{strings.visitProfile}</MenuItem>
                
               
             </Menu>

            </TouchableOpacity>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

export default connect(mapStateToProps)(MyFollowersCard);

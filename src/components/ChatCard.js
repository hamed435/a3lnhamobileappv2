import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import {Icon} from 'native-base'
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppText from '../common/AppText';
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import FastImage from 'react-native-fast-image'
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
//import { SwipeItem, SwipeButtonsContainer } from 'react-native-swipe-item';
import Swipeout from 'react-native-swipeout';
import {openImage} from '../actions/ChatAction';
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);




class ChatCard extends Component {
     swipeoutBtns = [
        {
          text: 'Button'
        }
      ]
    state = {
        ground: 0,
    }
    
    /*componentDidMount(){
        moment.locale(this.props.isRTL ? 'ar' : 'en');
    }*/

    

   

    render(){
        const {chatRow,currentUser} = this.props;
        //const dateToFormat = () => <Moment date={date} />,
        return(
       
        <Swipeout 
        close
        //right={ chatRow.user._id!=currentUser.user.id&&[{text: moment(chatRow.createdAt).fromNow(),backgroundColor:'white',color:'bkack'}] }
        //left={  chatRow.user._id==currentUser.user.id&&[{text: moment(chatRow.createdAt).fromNow(),backgroundColor:'white',color:'bkack'}] }    
        style={{backgroundColor:'white'}}
         >

            <View style={{alignSelf:'center', borderRadius:moderateScale(2), marginVertical:moderateScale(5),justifyContent:'center',alignItems:'center',padding:moderateScale(3),backgroundColor:'#f9f3f3'}}>
                <Text style={{color:'black'}}>{ moment(chatRow.createdAt).fromNow()}</Text>
             </View>

            <View style={{flexDirection:'row', margin:moderateScale(2),justifyContent:'center',alignSelf:chatRow.user._id==currentUser.user.id?'flex-end':'flex-start'}}>
               {chatRow.user._id!=currentUser.user.id&& <Thumbnail small source={chatRow.user.img?{uri:chatRow.user.img}:require('../assets/imgs/profileicon.jpg')} />}
                
                {chatRow.text.length>0&&
                <View style={{ justifyContent:'center', alignSelf:chatRow.user._id==currentUser.user.id?'flex-end':'flex-start'}}>
                    <TouchableOpacity 
                    style={{maxWidth:responsiveWidth(70), flexDirection:chatRow.user._id==currentUser.user.id?'row-reverse':'row', minHeight:responsiveHeight(7),alignItems:'center', borderRadius:moderateScale(5),alignSelf:chatRow.user._id==currentUser.user.id?'flex-end':'flex-start',marginHorizontal:moderateScale(1),backgroundColor:chatRow.user._id==currentUser.user.id?'#679C8A':'#f6f1f1'}}>
                        
                       
                        {/*<View style={{marginHorizontal:moderateScale(2.5),marginVertical:moderateScale(2)}}>
                        <Icon type='AntDesign' name='checkcircleo' style={{color:chatRow.seen?'green':'gray',fontSize:10}} />
                        </View>
                        */}

                        <View style={{maxWidth:responsiveWidth(60),margin:moderateScale(3),}}>
                         <Text style={{fontFamily:'Roboto-Regular', color:chatRow.user._id==currentUser.user.id?'white':'black' }}>{chatRow.text}</Text>         
                        </View>
                    </TouchableOpacity>       
                </View>
                }

               
            </View>

            {chatRow.image.length>10&&
                <View style={{ alignItems:'flex-end', alignSelf:chatRow.user._id==currentUser.user.id?'flex-end':'flex-start'}}>
                    <TouchableOpacity 
                    onPress={()=>{
                        this.props.openImage(true,chatRow.image)
                    }}
                    style={{ maxWidth:responsiveWidth(70), flexDirection:chatRow.user._id==currentUser.user.id?'row-reverse':'row', minHeight:responsiveHeight(7),alignItems:'center', borderRadius:moderateScale(5),alignSelf:chatRow.user._id==currentUser.user.id?'flex-end':'flex-start',marginHorizontal:moderateScale(1) }}>

                        <View style={{elevation:20, maxWidth:responsiveWidth(60),margin:moderateScale(3)}}>
                        <FastImage
                        source={{uri:chatRow.image}}
                         style={{borderRadius:moderateScale(3), width:responsiveWidth(50),height:responsiveHeight(28) }}
                        resizeMode='stretch'
                        />         
                        </View>
                    </TouchableOpacity>       
                </View> 
                }
        </Swipeout >
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    openImage
}


export default connect(mapStateToProps,mapDispatchToProps)(ChatCard);

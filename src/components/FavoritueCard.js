import React,{Component} from 'react';
import {View,ImageBackground,Text,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUser} from '../actions/AuthActions';
const MyButton =  withPreventDoubleClick(Button);
import Stars from 'react-native-stars';
import AsyncStorage  from '@react-native-community/async-storage'
import * as Animatable from 'react-native-animatable';


 class FavoritueCard extends Component {
    
  state={
  
    rate:3,
    favDone:true,

  }

  fav = (adsId) => {
        
    axios.post(`${BASE_END_POINT}favourite/${this.props.data.id}/ads`, {}, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('Fav Done');
       this.setState({favDone:true })
       //this.props.addFavLToist(adsId)
       const user = {...this.props.currentUser,user:{...response.data.user}}
       console.log('current user')
       console.log(user)
       this.props.getUser(user)
       AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
      //RNToasty.Success({title:Strings.youFavouriteThisAds})
    }).catch(error => {
        if(error.status==403){
            //RNToasty.Info({title:Strings.youFollowHimNow})
        }
        console.log('Fav error  ',error.status);
        console.log('Fav error  ',error.respone);
        console.log('Fav error  ',error);
        
      });
}

  removeFav = (adsId) => {
        
    axios.delete(`${BASE_END_POINT}favourite/${this.props.data.id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('un Fav Done');
       this.setState({favDone:false })
       //this.props.removeFavLToist(adsId)
       const user = {...this.props.currentUser,user:{...response.data.user}}
       this.props.getUser(user)
       AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
       /*this.props.navigator.resetTo({
         screen:'Home',
         animated: true
       })*/
      //RNToasty.Success({title:Strings.youFavouriteThisAds})
    }).catch(error => {
        if(error.status==403){
            //RNToasty.Info({title:Strings.youFollowHimNow})
        }
        console.log('Fav error  ',error.status);
        console.log('Fav error  ',error.respone);
        console.log('Fav error  ',error);
        
      });
}

 componentDidMount(){
   if(this.props.data.ratePrecent==0){
     this.setState({rate:0.5})
   }else if(this.props.data.ratePrecent>0&&this.props.data.ratePrecent<=20){
     this.setState({rate:1})
   }else if(this.props.data.ratePrecent>20&&this.props.data.ratePrecent<=40){
     this.setState({rate:2})
   }else if(this.props.data.ratePrecent>40&&this.props.data.ratePrecent<=60){
     this.setState({rate:3})
   }else if(this.props.data.ratePrecent>60&&this.props.data.ratePrecent<=80){
     this.setState({rate:4})
   }else if(this.props.data.ratePrecent>80){
     this.setState({rate:5})
   }
 }

    
    render(){
        const {data,isRTL,navigator,currentUser} = this.props;
        
        return(
          <Animatable.View duration={1500} animation='zoomIn' >
          <TouchableOpacity
          onPress={()=>{
            if(data.category.type=='JOPS'){
                navigator.push({
                    screen:'JobAdsDescription',
                    animated:true,
                    animationType:'slide-horizontal',
                    passProps:{data:data}
                })
            }
            else {
                navigator.push({
                    screen:'AdsDescription',
                    animated:true,
                    animationType:'slide-horizontal',
                    passProps:{data:data}
                })
            }
          }}
           style={{shadowOffset:{height:2,width:0}, shadowColor:'black', shadowOpacity:0.1, marginHorizontal:moderateScale(4),marginVertical:moderateScale(5), backgroundColor:'white',elevation:2, width:responsiveWidth(45),minHeight:responsiveHeight(36),borderRadius:moderateScale(2.5)}}>
            <ImageBackground imageStyle={{borderTopLeftRadius:moderateScale(2),borderTopRightRadius:moderateScale(2)}}  source={{uri:data.img[0]}}
             style={{width:responsiveWidth(45),height:responsiveHeight(18),borderRadius:moderateScale(2.5)}}>
              <TouchableOpacity 
              onPress={()=>{
                //RNToasty.Info({title:'ddd'})
                if(currentUser){
                    if(this.state.favDone){
                        this.removeFav(data.id)
                    }else{
                        this.fav(data.id)
                    }
                    

                }else{
                    navigator.push({
                        screen: 'Login',
                        animated: true,
                        animationType:'slide-horizontal'
                    })
                }
             }}
               style={{alignSelf:isRTL?'flex-start':'flex-end',margin:moderateScale(5)}}>
                <Icon name='heart' type='AntDesign' style={{color:this.state.favDone?'red':'gray'}} />
              </TouchableOpacity>
            
              {data.soled&&
              <View style={{position:'absolute',backgroundColor:'#679C8A',justifyContent:'center',alignItems:'center',paddingHorizontal:moderateScale(5),paddingVertical:moderateScale(3.5), alignSelf:isRTL?'flex-end':'flex-start',marginTop:moderateScale(5)}}>
                  <Text style={{color:'white',fontFamily:'Roboto-Regular',}} >{strings.buyed}</Text>
              </View>
              }
              
            </ImageBackground>
            <View style={{ width:responsiveWidth(43),marginBottom:moderateScale(7),}}>
                    <View style={{alignSelf:'center',marginTop:moderateScale(3), marginHorizontal:moderateScale(3),width:responsiveWidth(41),justifyContent:'center',}}>
                        <Text style={{width:responsiveWidth(41),textAlign:isRTL?'right':'left', fontFamily:'Roboto-Regular', color:'black',fontSize:responsiveFontSize(6)}}>{data.title}</Text>
                    </View>
                    <View style={{ marginHorizontal:moderateScale(3),marginTop:moderateScale(2),width:responsiveWidth(41),alignSelf:'center', justifyContent:'center'}}>
                        <Text style={{fontFamily:'Roboto-Regular', alignSelf:isRTL?'flex-end':'flex-start', color:'#B7B7B7',fontSize:responsiveFontSize(6)}}>{data.country.currency} {data.category.type=='JOPS'?data.priceFrom+"-"+data.priceTo:data.price}</Text>
                    </View>
                    <View style={{justifyContent:'center',width:responsiveWidth(41),alignSelf:'center',}}>
                    <View  style={{marginHorizontal:moderateScale(-3),alignSelf:isRTL?'flex-end':'flex-start',marginTop:moderateScale(2), width:responsiveWidth(25),justifyContent:'center'}}>
                    <Stars
                       disabled
                        default={this.state.rate}
                        count={5}
                        half={true}

                        fullStar={<Icon name='star' type='MaterialCommunityIcons' style={[styles.myStarStyle]}/>}
                        emptyStar={<Icon name={'star-outline'} type='MaterialCommunityIcons' style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
                        halfStar={<Icon name={'star-half'} type='MaterialIcons' style={[styles.myStarStyle]}/>}
                    />
                    </View>

                    </View>
                    
                    <View style={{padding:0,margin:0,width:responsiveWidth(41),alignSelf:'center',marginTop:moderateScale(2),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                      
                        <Icon name='location-pin' type='Entypo' style={{fontSize:responsiveFontSize(7), padding:0,margin:0, color:'#036C90'}} />
                        <Text style={{fontFamily:'Roboto-Regular',color:'#B7B7B7',fontSize:responsiveFontSize(6)}}>{data.country.countryName.length>15?data.country.countryName.substring(0,15):data.country.countryName}</Text>
                       
                    </View>
               </View>
          </TouchableOpacity> 
          </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    myStarStyle: {
      color: '#FFC416',
      backgroundColor: 'transparent',
      //textShadowColor: 'black',
      //textShadowOffset: {width: 1, height: 1},
      //textShadowRadius: 2,
      fontSize:responsiveFontSize(6),
      margin:0,
      padding:0,
    },
    myEmptyStarStyle: {
      color: 'gray',
      fontSize:responsiveFontSize(6),
      margin:0,
      padding:0,
    }
  });

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});
const mapDispatchToProps = {
  getUser,
}


export default connect(mapStateToProps,mapDispatchToProps)(FavoritueCard);

import React,{Component} from 'react';
import {View,ImageBackground,Text,Alert,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import {Button,Icon,Thumbnail} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
const MyButton =  withPreventDoubleClick(Button);


 class SelectCategorCard extends Component {


    render(){
        const {data,onPress,isRTL,navigator} = this.props; 
        return(
            <TouchableOpacity
            onPress={()=>{
                if(data.type=='MOTOR'){

                }else if(data.type=='JOPS'){
                    navigator.push({
                        screen: 'AddJobAds',
                        animated: true,
                        animationType:'slide-horizontal',
                        passProps:{ data:data }
                    })
                }
                else if(data.type=='REAL-STATE'){
                    navigator.push({
                        screen: 'AddBuildingAds',
                        animated: true,
                        animationType:'slide-horizontal',
                        passProps:{ data:data }
                    })
                }else {
                    navigator.push({
                        screen: 'AddAds',
                        animated: true,
                        animationType:'slide-horizontal',
                        passProps:{ data:data }
                    })
                }
              /*navigator.push({
                screen: 'FriendProfile',
                animated: true,
                passProps:{ data:data.follower }
              })*/
            }}
            activeOpacity={1}
            style={{alignSelf:'center', width:responsiveWidth(90),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center',height:responsiveHeight(11),borderBottomWidth:0.4,borderBottomColor:'#646464'}}
            >

            <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                <View style={{width:40,height:40,borderRadius:20,backgroundColor:'gray',justifyContent:'center',alignItems:'center'}}>
                <FastImage resizeMode='center' style={{width:20,height:20}} source={{uri:data.img}} />
                </View>
                <Text style={{marginHorizontal:moderateScale(5), color:'#646464'}}>{isRTL?data.arabicName:data.name}</Text>
            </View>

          
            

            </TouchableOpacity>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

export default connect(mapStateToProps)(SelectCategorCard);

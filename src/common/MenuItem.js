//
import React, { Component } from "react";
import { Text, Platform, View, TouchableOpacity } from "react-native";

import AppText from "./AppText";
import { connect } from "react-redux";
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
//import Icon from 'react-native-vector-icons/FontAwesome5';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Icon} from 'native-base';

const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

class MenuItem extends Component {
//color={focused? '#ff6f61' : 'white'}
    renderNormal() {
        const {type, text, focused, onPress, userType, isRTL,iconName,iconSize } = this.props;

        return (
            <MyTouchableOpacity style={{backgroundColor:"transparent",height:hp(8),justifyContent:'center'}} onPress={onPress}>
                <View style={{alignSelf:isRTL?'flex-end':'flex-start',  width:wp(60), marginHorizontal:moderateScale(7),flexDirection: isRTL ? "row-reverse" : 'row',alignItems:'center'}} >
                   <View style={{width:wp(8),justifyContent:'center',alignItems:'center'}} >
                   <Icon style={{color:'white',fontSize:responsiveFontSize(8)}} name={iconName} type={type}    />
                   </View>
                   <AppText marginHorizontal={wp(5)} fontWeight='600' text={text} textAlign="center" fontSize={wp(5)} color='white' />
                </View>
            </MyTouchableOpacity>
        )
    }
    render() {
        return this.renderNormal();
    }
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(MenuItem);

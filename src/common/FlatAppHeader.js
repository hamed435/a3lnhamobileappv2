import React, { Component } from 'react';
import {
  View,
  Image,
  StatusBar,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  Platform,Text,
  StyleSheet,
} from 'react-native';
import { Icon, Button,Badge } from 'native-base';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import AppTitle from './AppTitle';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import allStrings from '../assets/strings';
import * as allColors from '../assets/colors';
import { RNToasty } from 'react-native-toasty';


class FlatAppHeader extends Component {

  

  render() {
    const {color, title, isRTL, navigator,menu,add,onPress, update, updateAction } = this.props;
    return (
      <ImageBackground
      source={require('../assets/imgs/header.png')}
       style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,elevation:1, height:responsiveHeight(10),width:responsiveWidth(100), backgroundColor:'#707D67',flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>
          {menu?
          <TouchableOpacity
          onPress={()=>{
            navigator.toggleDrawer({
              side: this.props.isRTL ? 'right' : 'left',
            })
          }}
           style={{ width:responsiveWidth(10), marginHorizontal:moderateScale(5)}}>
              <Icon style={{fontSize:responsiveFontSize(10), color:'white'}} type='MaterialCommunityIcons' name='sort-variant' />
          </TouchableOpacity>
          :
          <TouchableOpacity
          onPress={()=>{
            navigator.pop()
          }}
           style={{  width:responsiveWidth(10), marginHorizontal:moderateScale(5)}}>
              <Icon style={{fontSize:responsiveFontSize(9), color:'white'}} type='AntDesign' name={isRTL?'arrowright':'arrowleft'} />
          </TouchableOpacity>
          }
          <View style={{width:responsiveWidth(60),justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontFamily:'Roboto-Regular',color:'white',fontSize:responsiveFontSize(8)}}>{title}</Text>
          </View>

          <View style={{ flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
          {add&&this.props.currentUser?
           <TouchableOpacity
           onPress={()=>{
            onPress()
           }}
            style={{ marginHorizontal:moderateScale(5)}}>
               <Icon style={{fontSize:responsiveFontSize(10), color:'white'}} type='MaterialIcons' name='add' />
           </TouchableOpacity>
           :
          <View style={{width:responsiveWidth(10)}} />
          }
           {update&&this.props.currentUser?
           <TouchableOpacity
           onPress={()=>{
            updateAction()
           }}
            style={{ marginHorizontal:moderateScale(5)}}>
               <Icon style={{fontSize:responsiveFontSize(10), color:'white'}} name='edit' type='MaterialIcons' />
           </TouchableOpacity>
           :
          null
          }
          </View>
      </ImageBackground>
    );
  }
}


const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  color: state.lang.color,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps)(FlatAppHeader);

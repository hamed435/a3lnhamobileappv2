package com.a3lnhamobileapp;

import android.app.Application;

import com.facebook.react.ReactApplication;

import com.airbnb.android.react.maps.MapsPackage;

//
import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.reactnativenavigation.NavigationApplication;
/*require to facebbok*/
import com.reactnativenavigation.controllers.ActivityCallbacks;
import android.content.Intent;
/* -- */
import com.reactnativenavigation.bridge.NavigationReactPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.ashideas.rnrangeslider.RangeSliderPackage;
//import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.horcrux.svg.SvgPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import ui.toasty.RNToastyPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage; 
//import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage; 
//
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;



public class MainApplication extends NavigationApplication {

  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();
  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }

  protected List<ReactPackage> getPackages() {

    // Add additional packages you require here
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList( // <== this
            new MainReactPackage(),
            new MapsPackage(),
            //new RNFirebaseCrashlyticsPackage(),
            new RNFirebasePackage(),
            new RNFirebaseNotificationsPackage(),
            new RNFirebaseMessagingPackage(),
            new RNGoogleSigninPackage(),
            new FBSDKPackage(mCallbackManager),
            new NavigationReactPackage(),
            new RangeSliderPackage(),
            // new RNGoogleSigninPackage(),
            new AsyncStoragePackage(),
            new PickerPackage(),
            new FastImageViewPackage(),
            new LottiePackage(),
            new SvgPackage(),
            new ReactNativeLocalizationPackage(),
            new VectorIconsPackage(),
            new RNToastyPackage()


    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }

  @Override
  public String getJSMainModuleName() {
    return "index";
  }

  @Override
  public void onCreate() {
    super.onCreate();
    setActivityCallbacks(new ActivityCallbacks() 
    {   @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) 
        {  mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    });
    FacebookSdk.sdkInitialize(getApplicationContext());
    AppEventsLogger.activateApp(this);

  }

}

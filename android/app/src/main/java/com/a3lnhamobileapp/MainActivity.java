package com.a3lnhamobileapp;


import android.content.Intent;
import com.facebook.react.ReactActivity;
import com.reactnativenavigation.controllers.SplashActivity;
import com.facebook.CallbackManager;


public class MainActivity extends SplashActivity {

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

   /* @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        
    }
    */
}
